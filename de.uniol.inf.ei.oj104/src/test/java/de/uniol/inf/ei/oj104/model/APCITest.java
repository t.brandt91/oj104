/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.controlfield.ControlFunctionType;
import de.uniol.inf.ei.oj104.model.controlfield.InformationTransfer;
import de.uniol.inf.ei.oj104.model.controlfield.NumberedSupervisoryFunction;
import de.uniol.inf.ei.oj104.model.controlfield.UnnumberedControlFunction;

/**
 * Class that contains test for {@link APCI}.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class APCITest {

	/**
	 * Test of creation, converting to, and converting from bytes of an {@link APCI}
	 * in I-format with random send and receive sequence numbers.
	 */
	@Test
	public void testInformationTransfer() {
		int length = (int) (Math.random() * 250 + 4);
		InformationTransfer controlField = new InformationTransfer((short) (Math.random() * 256),
				(short) (Math.random() * 256));
		byte[] objectBytes = null;
		try {
			objectBytes = controlField.toBytes();
		} catch (IEC608705104ProtocolException e) {
			fail(e.getMessage());
		}
		byte[] bytes = new byte[6];
		bytes[0] = 0x68;
		bytes[1] = (byte) (length & 0xff);
		for (int i = 0; i < 4; i++) {
			bytes[i + 2] = objectBytes[i];
		}
		APCI apci = new APCI();
		try {
			apci.fromBytes(bytes);
		} catch (IEC608705104ProtocolException e) {
			fail(e.getMessage());
		}
		assertEquals("Length of ASDU does not match! Expected=" + length + ", Actual=" + apci.getLength(), length,
				apci.getLength());
		assertEquals("Control field does not match! Expected=" + controlField + ", Actual=" + apci.getControlField(),
				controlField, apci.getControlField());

		byte[] bytes2 = null;
		try {
			bytes2 = apci.toBytes();
		} catch (IEC608705104ProtocolException e) {
			fail(e.getMessage());
		}
		for (int i = 0; i < 6; i++) {
			assertEquals("Byte representation of " + apci + " does not match!", bytes[i], bytes2[i]);
		}
	}

	/**
	 * Test of creation, converting to, and converting from bytes of an {@link APCI}
	 * in S-format with random receive sequence numbers.
	 */
	@Test
	public void testNumberedSupervisoryFunction() {
		int length = 4;
		NumberedSupervisoryFunction controlField = new NumberedSupervisoryFunction((short) (Math.random() * 256));
		byte[] objectBytes = null;
		try {
			objectBytes = controlField.toBytes();
		} catch (IEC608705104ProtocolException e) {
			fail(e.getMessage());
		}
		byte[] bytes = new byte[6];
		bytes[0] = 0x68;
		bytes[1] = (byte) (length & 0xff);
		for (int i = 0; i < 4; i++) {
			bytes[i + 2] = objectBytes[i];
		}
		APCI apci = new APCI();
		try {
			apci.fromBytes(bytes);
		} catch (IEC608705104ProtocolException e) {
			fail(e.getMessage());
		}
		assertEquals("Length of ASDU does not match! Expected=" + length + ", Actual=" + apci.getLength(), length,
				apci.getLength());
		assertEquals("Control field does not match! Expected=" + controlField + ", Actual=" + apci.getControlField(),
				controlField, apci.getControlField());

		byte[] bytes2 = null;
		try {
			bytes2 = apci.toBytes();
		} catch (IEC608705104ProtocolException e) {
			fail(e.getMessage());
		}
		for (int i = 0; i < 6; i++) {
			assertEquals("Byte representation of " + apci + " does not match!", bytes[i], bytes2[i]);
		}
	}

	/**
	 * Test of creation, converting to, and converting from bytes of an {@link APCI}
	 * in U-format of random type.
	 */
	@Test
	public void testUnnumberedControlFunction() {
		int length = 4;
		UnnumberedControlFunction controlField = new UnnumberedControlFunction(
				ControlFunctionType.values()[(int) (Math.random() * 3)], Math.random() < 0.5);
		byte[] objectBytes = null;
		try {
			objectBytes = controlField.toBytes();
		} catch (IEC608705104ProtocolException e) {
			fail(e.getMessage());
		}
		byte[] bytes = new byte[6];
		bytes[0] = 0x68;
		bytes[1] = (byte) (length & 0xff);
		for (int i = 0; i < 4; i++) {
			bytes[i + 2] = objectBytes[i];
		}
		APCI apci = new APCI();
		try {
			apci.fromBytes(bytes);
		} catch (IEC608705104ProtocolException e) {
			fail(e.getMessage());
		}
		assertEquals("Length of ASDU does not match! Expected=" + length + ", Actual=" + apci.getLength(), length,
				apci.getLength());
		assertEquals("Control field does not match! Expected=" + controlField + ", Actual=" + apci.getControlField(),
				controlField, apci.getControlField());

		byte[] bytes2 = null;
		try {
			bytes2 = apci.toBytes();
		} catch (IEC608705104ProtocolException e) {
			fail(e.getMessage());
		}
		for (int i = 0; i < 6; i++) {
			assertEquals("Byte representation of " + apci + " does not match!", bytes[i], bytes2[i]);
		}
	}

}