/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;

import de.uniol.inf.ei.oj104.model.CauseOfTransmission;

/**
 * Class that contains test for {@link CauseOfTransmissionFactory}.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class CauseOfTransmissionFactoryTest {

	/**
	 * Test of loading all information about the causes of transmission.
	 */
	@Test
	public void test() {
		Collection<CauseOfTransmission> causesFromGetAll = CauseOfTransmissionFactory.getAllCausesOfTransmission();
		Collection<CauseOfTransmission> causesFromGetSingle = new ArrayList<>();

		causesFromGetSingle.add(testSingleCause(0, "not used"));
		causesFromGetSingle.add(testSingleCause(1, "periodic, cyclic"));
		causesFromGetSingle.add(testSingleCause(2, "background scan"));
		causesFromGetSingle.add(testSingleCause(3, "spontaneous"));
		causesFromGetSingle.add(testSingleCause(4, "initialized"));
		causesFromGetSingle.add(testSingleCause(5, "request or requested"));
		causesFromGetSingle.add(testSingleCause(6, "activation"));
		causesFromGetSingle.add(testSingleCause(7, "activation confirmation"));
		causesFromGetSingle.add(testSingleCause(8, "deactivation"));
		causesFromGetSingle.add(testSingleCause(9, "deactivation confirmation"));
		causesFromGetSingle.add(testSingleCause(10, "activation termination"));
		causesFromGetSingle.add(testSingleCause(11, "return information caused by a remote command"));
		causesFromGetSingle.add(testSingleCause(12, "return information caused by a local command"));
		causesFromGetSingle.add(testSingleCause(13, "file transfer"));
		causesFromGetSingle.add(testSingleCause(14, "reserved for further compatible definitions"));
		causesFromGetSingle.add(testSingleCause(15, "reserved for further compatible definitions"));
		causesFromGetSingle.add(testSingleCause(16, "reserved for further compatible definitions"));
		causesFromGetSingle.add(testSingleCause(17, "reserved for further compatible definitions"));
		causesFromGetSingle.add(testSingleCause(18, "reserved for further compatible definitions"));
		causesFromGetSingle.add(testSingleCause(19, "reserved for further compatible definitions"));
		causesFromGetSingle.add(testSingleCause(20, "interrogated by station interrogation"));
		causesFromGetSingle.add(testSingleCause(21, "interrogated by group 1 interrogation"));
		causesFromGetSingle.add(testSingleCause(22, "interrogated by group 2 interrogation"));
		causesFromGetSingle.add(testSingleCause(23, "interrogated by group 3 interrogation"));
		causesFromGetSingle.add(testSingleCause(24, "interrogated by group 4 interrogation"));
		causesFromGetSingle.add(testSingleCause(25, "interrogated by group 5 interrogation"));
		causesFromGetSingle.add(testSingleCause(26, "interrogated by group 6 interrogation"));
		causesFromGetSingle.add(testSingleCause(27, "interrogated by group 7 interrogation"));
		causesFromGetSingle.add(testSingleCause(28, "interrogated by group 8 interrogation"));
		causesFromGetSingle.add(testSingleCause(29, "interrogated by group 9 interrogation"));
		causesFromGetSingle.add(testSingleCause(30, "interrogated by group 10 interrogation"));
		causesFromGetSingle.add(testSingleCause(31, "interrogated by group 11 interrogation"));
		causesFromGetSingle.add(testSingleCause(32, "interrogated by group 12 interrogation"));
		causesFromGetSingle.add(testSingleCause(33, "interrogated by group 13 interrogation"));
		causesFromGetSingle.add(testSingleCause(34, "interrogated by group 14 interrogation"));
		causesFromGetSingle.add(testSingleCause(35, "interrogated by group 15 interrogation"));
		causesFromGetSingle.add(testSingleCause(36, "interrogated by group 16 interrogation"));
		causesFromGetSingle.add(testSingleCause(37, "requested by general counter request"));
		causesFromGetSingle.add(testSingleCause(38, "requested by group 1 counter request"));
		causesFromGetSingle.add(testSingleCause(39, "requested by group 2 counter request"));
		causesFromGetSingle.add(testSingleCause(40, "requested by group 3 counter request"));
		causesFromGetSingle.add(testSingleCause(41, "requested by group 4 counter request"));
		causesFromGetSingle.add(testSingleCause(42, "reserved for further compatible definitions"));
		causesFromGetSingle.add(testSingleCause(43, "reserved for further compatible definitions"));
		causesFromGetSingle.add(testSingleCause(44, "unknown type identification"));
		causesFromGetSingle.add(testSingleCause(45, "unknown cause of transmission"));
		causesFromGetSingle.add(testSingleCause(46, "unknown common address of ASDU"));
		causesFromGetSingle.add(testSingleCause(47, "unknown information object address"));
		causesFromGetSingle.add(testSingleCause(48, "for special use (private range)"));
		causesFromGetSingle.add(testSingleCause(49, "for special use (private range)"));
		causesFromGetSingle.add(testSingleCause(50, "for special use (private range)"));
		causesFromGetSingle.add(testSingleCause(51, "for special use (private range)"));
		causesFromGetSingle.add(testSingleCause(52, "for special use (private range)"));
		causesFromGetSingle.add(testSingleCause(53, "for special use (private range)"));
		causesFromGetSingle.add(testSingleCause(54, "for special use (private range)"));
		causesFromGetSingle.add(testSingleCause(55, "for special use (private range)"));
		causesFromGetSingle.add(testSingleCause(56, "for special use (private range)"));
		causesFromGetSingle.add(testSingleCause(57, "for special use (private range)"));
		causesFromGetSingle.add(testSingleCause(58, "for special use (private range)"));
		causesFromGetSingle.add(testSingleCause(59, "for special use (private range)"));
		causesFromGetSingle.add(testSingleCause(60, "for special use (private range)"));
		causesFromGetSingle.add(testSingleCause(61, "for special use (private range)"));
		causesFromGetSingle.add(testSingleCause(62, "for special use (private range)"));
		causesFromGetSingle.add(testSingleCause(63, "for special use (private range)"));
		assertTrue(
				"Methods getAllCausesOfTransmission and getCauseOfTransmission do not return the same amount of Causes of Transmission",
				causesFromGetAll.size() == causesFromGetSingle.size());
		assertTrue(
				"Methods getAllCausesOfTransmission and getCauseOfTransmission do not return the same Causes of Transmission",
				causesFromGetAll.containsAll(causesFromGetSingle));
	}

	/**
	 * Tests, if all information for a {@link CauseOfTransmission} are loaded and
	 * valid in the factory.
	 * 
	 * @param id          The ID of the {@link CauseOfTransmission}. The IDs are
	 *                    subsequent numbers defined in the standard.
	 * @param description The description of the {@link CauseOfTransmission}. The
	 *                    descriptions are human readable and defined in the
	 *                    standard.
	 * @return The {@link CauseOfTransmission}.
	 */
	private CauseOfTransmission testSingleCause(int id, String description) {
		CauseOfTransmission type = CauseOfTransmissionFactory.getCauseOfTransmission(id).orElse(null);
		assertNotNull("No Cause of Transmission with id=" + id + " in dictionary", type);
		assertEquals("id of Cause of Transmission does not match! Expected=" + id + ", Actual=" + type.getId(), id,
				type.getId());
		assertEquals("description of Cause of Transmission(" + id + ") does not match! Expected=" + description
				+ ", Actual=" + type.getDescription(), description, type.getDescription());
		return type;
	}

}