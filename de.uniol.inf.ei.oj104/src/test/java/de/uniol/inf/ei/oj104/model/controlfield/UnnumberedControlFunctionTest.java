/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.controlfield;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;

/**
 * Class that contains test for {@link NumberedSupervisoryFunction}.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class UnnumberedControlFunctionTest {

	/**
	 * Test of converting from and to bytes of a STARTDT activation.
	 */
	@Test
	public void testStartDTAct() {
		byte[] bytes = new byte[4];
		bytes[0] = 0x07;
		bytes[1] = 0x00;
		bytes[2] = 0x00;
		bytes[3] = 0x00;
		UnnumberedControlFunction object = new UnnumberedControlFunction();
		try {
			object.fromBytes(bytes);
		} catch (IEC608705104ProtocolException e) {
			fail(e.getMessage());
		}
		assertEquals("Control function type of unnumbered control function does not match! Expected=STARTDT, Actual="
				+ object.getType(), "STARTDT", object.getType().toString());
		assertEquals("Act/Con of unnumbered control function does not match! Expected=act, Actual="
				+ (object.isActivate() ? "act" : "con"), true, object.isActivate());

		byte[] bytes2 = null;
		try {
			bytes2 = object.toBytes();
		} catch (IEC608705104ProtocolException e) {
			fail(e.getMessage());
		}
		for (int i = 0; i < 4; i++) {
			assertEquals("Byte representation of " + object + " does not match!", bytes[i], bytes2[i]);
		}
	}

	/**
	 * Test of converting from and to bytes of a STARTDT confirmation.
	 */
	@Test
	public void testStartDTCon() {
		byte[] bytes = new byte[4];
		bytes[0] = 0x0B;
		bytes[1] = 0x00;
		bytes[2] = 0x00;
		bytes[3] = 0x00;
		UnnumberedControlFunction object = new UnnumberedControlFunction();
		try {
			object.fromBytes(bytes);
		} catch (IEC608705104ProtocolException e) {
			fail(e.getMessage());
		}
		assertEquals("Control function type of unnumbered control function does not match! Expected=STARTDT, Actual="
				+ object.getType(), "STARTDT", object.getType().toString());
		assertEquals("Act/Con of unnumbered control function does not match! Expected=con, Actual="
				+ (object.isActivate() ? "act" : "con"), false, object.isActivate());

		byte[] bytes2 = null;
		try {
			bytes2 = object.toBytes();
		} catch (IEC608705104ProtocolException e) {
			fail(e.getMessage());
		}
		for (int i = 0; i < 4; i++) {
			assertEquals("Byte representation of " + object + " does not match!", bytes[i], bytes2[i]);
		}
	}

	/**
	 * Test of converting from and to bytes of a STOPDT activation.
	 */
	@Test
	public void testStopDTAct() {
		byte[] bytes = new byte[4];
		bytes[0] = 0x13;
		bytes[1] = 0x00;
		bytes[2] = 0x00;
		bytes[3] = 0x00;
		UnnumberedControlFunction object = new UnnumberedControlFunction();
		try {
			object.fromBytes(bytes);
		} catch (IEC608705104ProtocolException e) {
			fail(e.getMessage());
		}
		assertEquals("Control function type of unnumbered control function does not match! Expected=STOPDT, Actual="
				+ object.getType(), "STOPDT", object.getType().toString());
		assertEquals("Act/Con of unnumbered control function does not match! Expected=act, Actual="
				+ (object.isActivate() ? "act" : "con"), true, object.isActivate());

		byte[] bytes2 = null;
		try {
			bytes2 = object.toBytes();
		} catch (IEC608705104ProtocolException e) {
			fail(e.getMessage());
		}
		for (int i = 0; i < 4; i++) {
			assertEquals("Byte representation of " + object + " does not match!", bytes[i], bytes2[i]);
		}
	}

	/**
	 * Test of converting from and to bytes of a STOPDT confirmation.
	 */
	@Test
	public void testStopDTCon() {
		byte[] bytes = new byte[4];
		bytes[0] = 0x23;
		bytes[1] = 0x00;
		bytes[2] = 0x00;
		bytes[3] = 0x00;
		UnnumberedControlFunction object = new UnnumberedControlFunction();
		try {
			object.fromBytes(bytes);
		} catch (IEC608705104ProtocolException e) {
			fail(e.getMessage());
		}
		assertEquals("Control function type of unnumbered control function does not match! Expected=STOPDT, Actual="
				+ object.getType(), "STOPDT", object.getType().toString());
		assertEquals("Act/Con of unnumbered control function does not match! Expected=con, Actual="
				+ (object.isActivate() ? "act" : "con"), false, object.isActivate());

		byte[] bytes2 = null;
		try {
			bytes2 = object.toBytes();
		} catch (IEC608705104ProtocolException e) {
			fail(e.getMessage());
		}
		for (int i = 0; i < 4; i++) {
			assertEquals("Byte representation of " + object + " does not match!", bytes[i], bytes2[i]);
		}
	}

	/**
	 * Test of converting from and to bytes of a TESTFR activation.
	 */
	@Test
	public void testTestFRAct() {
		byte[] bytes = new byte[4];
		bytes[0] = 0x43;
		bytes[1] = 0x00;
		bytes[2] = 0x00;
		bytes[3] = 0x00;
		UnnumberedControlFunction object = new UnnumberedControlFunction();
		try {
			object.fromBytes(bytes);
		} catch (IEC608705104ProtocolException e) {
			fail(e.getMessage());
		}
		assertEquals("Control function type of unnumbered control function does not match! Expected=TESTFR, Actual="
				+ object.getType(), "TESTFR", object.getType().toString());
		assertEquals("Act/Con of unnumbered control function does not match! Expected=act, Actual="
				+ (object.isActivate() ? "act" : "con"), true, object.isActivate());

		byte[] bytes2 = null;
		try {
			bytes2 = object.toBytes();
		} catch (IEC608705104ProtocolException e) {
			fail(e.getMessage());
		}
		for (int i = 0; i < 4; i++) {
			assertEquals("Byte representation of " + object + " does not match!", bytes[i], bytes2[i]);
		}
	}

	/**
	 * Test of converting from and to bytes of a TESTFR confirmation.
	 */
	@Test
	public void testTestFRCon() {
		byte[] bytes = new byte[4];
		bytes[0] = (byte) 0x83;
		bytes[1] = 0x00;
		bytes[2] = 0x00;
		bytes[3] = 0x00;
		UnnumberedControlFunction object = new UnnumberedControlFunction();
		try {
			object.fromBytes(bytes);
		} catch (IEC608705104ProtocolException e) {
			fail(e.getMessage());
		}
		assertEquals("Control function type of unnumbered control function does not match! Expected=TESTFR, Actual="
				+ object.getType(), "TESTFR", object.getType().toString());
		assertEquals("Act/Con of unnumbered control function does not match! Expected=con, Actual="
				+ (object.isActivate() ? "act" : "con"), false, object.isActivate());

		byte[] bytes2 = null;
		try {
			bytes2 = object.toBytes();
		} catch (IEC608705104ProtocolException e) {
			fail(e.getMessage());
		}
		for (int i = 0; i < 4; i++) {
			assertEquals("Byte representation of " + object + " does not match!", bytes[i], bytes2[i]);
		}
	}

}