/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.controlfield;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.controlfield.NumberedSupervisoryFunction;

/**
 * Class that contains test for {@link NumberedSupervisoryFunction}.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class NumberedSupervisoryFunctionTest {

	/**
	 * Test of converting from and to bytes of numbered supervisory control fields
	 * with all possible sequence numbers.
	 */
	@Test
	public void test() {
		byte[] bytes;
		NumberedSupervisoryFunction object;
		for (int i = 0; i < 256; i++) {
			bytes = new byte[4];
			bytes[0] = 0x01;
			bytes[1] = 0x00;
			bytes[2] = (byte) ((i & 0xff) << 1);
			bytes[3] = (byte) ((i & 0xff) >> 7);
			object = new NumberedSupervisoryFunction();
			try {
				object.fromBytes(bytes);
			} catch (IEC608705104ProtocolException e) {
				fail(e.getMessage());
			}
			assertEquals("Receive sequence number of information transfer does not match! Expected=" + i + ", Actual="
					+ object.getReceiveSequenceNumber(), i, object.getReceiveSequenceNumber());

			byte[] bytes2 = null;
			try {
				bytes2 = object.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail(e.getMessage());
			}
			for (int k = 0; k < 4; k++) {
				assertEquals("Byte representation of " + object + " does not match!", bytes[k], bytes2[k]);
			}
		}
	}

}