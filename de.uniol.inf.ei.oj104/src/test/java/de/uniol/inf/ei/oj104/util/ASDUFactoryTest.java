/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;

import de.uniol.inf.ei.oj104.model.ASDU;
import de.uniol.inf.ei.oj104.model.ASDUType;

/**
 * Class that contains test for {@link ASDUFactory}.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class ASDUFactoryTest {

	/**
	 * Test of loading all information about the ASDU types.
	 */
	@Test
	public void test() {
		Collection<ASDUType> typesFromGetAll = ASDUFactory.getAllASDUTypes();
		Collection<ASDUType> typesFromGetSingle = new ArrayList<>();

		typesFromGetSingle.add(testSingleASDU(0, "not defined", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(1, "single-point information", "M_SP_NA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.SinglePointInformation", "", "0 1",
				"2 3 5 11 12 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36"));
		typesFromGetSingle.add(testSingleASDU(2, "single-point information with time tag", "M_SP_TA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.SinglePointInformation",
				"de.uniol.inf.ei.oj104.model.timetag.ThreeOctetBinaryTime", "0", "3 5 11 12"));
		typesFromGetSingle.add(testSingleASDU(3, "double-point information", "M_DP_NA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.DoublePointInformation", "", "0 1",
				"2 3 5 11 12 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36"));
		typesFromGetSingle.add(testSingleASDU(4, "double-point information with time tag", "M_DP_TA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.DoublePointInformation",
				"de.uniol.inf.ei.oj104.model.timetag.ThreeOctetBinaryTime", "0", "3 5 11 12"));
		typesFromGetSingle.add(testSingleASDU(5, "step position information", "M_ST_NA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.ValueWithTransientStateIndication de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorWithOV",
				"", "0 1", "2 3 5 11 12 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36"));
		typesFromGetSingle.add(testSingleASDU(6, "step position information with time tag", "M_ST_TA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.ValueWithTransientStateIndication de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorWithOV",
				"de.uniol.inf.ei.oj104.model.timetag.ThreeOctetBinaryTime", "0", "3 5 11 12"));
		typesFromGetSingle.add(testSingleASDU(7, "bitstring of 32 bits", "M_BO_NA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.BinaryStateInformation de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorWithOV",
				"", "0 1", "2 3 5 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36"));
		typesFromGetSingle.add(testSingleASDU(8, "bitstring of 32 bits with time tag", "M_BO_TA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.BinaryStateInformation de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorWithOV",
				"de.uniol.inf.ei.oj104.model.timetag.ThreeOctetBinaryTime", "0", "3 5"));
		typesFromGetSingle.add(testSingleASDU(9, "measured value, normalized value", "M_ME_NA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.NormalizedValue de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorWithOV",
				"", "0 1", "2 3 5 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36"));
		typesFromGetSingle.add(testSingleASDU(10, "measured value, normalized value with time tag", "M_ME_TA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.NormalizedValue de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorWithOV",
				"de.uniol.inf.ei.oj104.model.timetag.ThreeOctetBinaryTime", "0", "3 5"));
		typesFromGetSingle.add(testSingleASDU(11, "measured value, scaled value", "M_ME_NB_1",
				"de.uniol.inf.ei.oj104.model.informationelement.ScaledValue de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorWithOV",
				"", "0 1", "1 2 3 5 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36"));
		typesFromGetSingle.add(testSingleASDU(12, "measured value, scaled value with time tag", "M_ME_TB_1",
				"de.uniol.inf.ei.oj104.model.informationelement.ScaledValue de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorWithOV",
				"de.uniol.inf.ei.oj104.model.timetag.ThreeOctetBinaryTime", "0", "3 5"));
		typesFromGetSingle.add(testSingleASDU(13, "measured value, short floating point number", "M_ME_NC_1",
				"de.uniol.inf.ei.oj104.model.informationelement.ShortFloatingPointNumber de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorWithOV",
				"", "0 1", "1 2 3 5 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36"));
		typesFromGetSingle.add(testSingleASDU(14, "measured value, short floating point number with time tag",
				"M_ME_TC_1",
				"de.uniol.inf.ei.oj104.model.informationelement.ShortFloatingPointNumber de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorWithOV",
				"de.uniol.inf.ei.oj104.model.timetag.ThreeOctetBinaryTime", "0", "3 5"));
		typesFromGetSingle.add(testSingleASDU(15, "integrated totals", "M_IT_NA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.BinaryCounterReading", "", "0 1", "3 37 38 39 40 41"));
		typesFromGetSingle.add(testSingleASDU(16, "integrated totals with time tag", "M_IT_TA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.BinaryCounterReading",
				"de.uniol.inf.ei.oj104.model.timetag.ThreeOctetBinaryTime", "0", "3 37 38 39 40 41"));
		typesFromGetSingle.add(testSingleASDU(17, "event of protection equipment with time tag", "M_EP_TA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.PESingleEvent de.uniol.inf.ei.oj104.model.timetag.TwoOctetBinaryTime",
				"de.uniol.inf.ei.oj104.model.timetag.ThreeOctetBinaryTime", "0", "3"));
		typesFromGetSingle.add(testSingleASDU(18, "packed start events of protection equipment with time tag",
				"M_EP_TB_1",
				"de.uniol.inf.ei.oj104.model.informationelement.PEStartEvent de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorForPE de.uniol.inf.ei.oj104.model.timetag.TwoOctetBinaryTime",
				"de.uniol.inf.ei.oj104.model.timetag.ThreeOctetBinaryTime", "0", "3"));
		typesFromGetSingle.add(testSingleASDU(19,
				"packed output circuit information of protection equipment with time tag", "M_EP_TC_1",
				"de.uniol.inf.ei.oj104.model.informationelement.PEOutputCircuitInformation de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorForPE de.uniol.inf.ei.oj104.model.timetag.TwoOctetBinaryTime",
				"de.uniol.inf.ei.oj104.model.timetag.ThreeOctetBinaryTime", "0", "3"));
		typesFromGetSingle.add(testSingleASDU(20, "packed single-point information with status change detection",
				"M_PS_NA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.StatusAndChangeDetection de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorWithOV",
				"", "0 1", "2 3 5 11 12 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36"));
		typesFromGetSingle.add(testSingleASDU(21, "measured value, normalized value without quality descriptor",
				"M_ME_ND_1", "de.uniol.inf.ei.oj104.model.informationelement.NormalizedValue", "", "0 1",
				"1 2 3 5 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36"));
		typesFromGetSingle.add(testSingleASDU(22, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(23, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(24, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(25, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(26, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(27, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(28, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(29, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(30, "single-point information with time tag CP56Time2a", "M_SP_TB_1",
				"de.uniol.inf.ei.oj104.model.informationelement.SinglePointInformation",
				"de.uniol.inf.ei.oj104.model.timetag.SevenOctetBinaryTime", "0", "3 5 11 12"));
		typesFromGetSingle.add(testSingleASDU(31, "double-point information with time tag CP56Time2a", "M_DP_TB_1",
				"de.uniol.inf.ei.oj104.model.informationelement.DoublePointInformation",
				"de.uniol.inf.ei.oj104.model.timetag.SevenOctetBinaryTime", "0", "3 5 11 12"));
		typesFromGetSingle.add(testSingleASDU(32, "step position information with time tag CP56Time2a", "M_ST_TB_1",
				"de.uniol.inf.ei.oj104.model.informationelement.ValueWithTransientStateIndication de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorWithOV",
				"de.uniol.inf.ei.oj104.model.timetag.SevenOctetBinaryTime", "0", "3 5 11 12"));
		typesFromGetSingle.add(testSingleASDU(33, "bitstring of 32 bits with time tag CP56Time2a", "M_BO_TB_1",
				"de.uniol.inf.ei.oj104.model.informationelement.BinaryStateInformation de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorWithOV",
				"de.uniol.inf.ei.oj104.model.timetag.SevenOctetBinaryTime", "0", "3 5"));
		typesFromGetSingle.add(testSingleASDU(34, "measured value, normalized value with time tag CP56Time2a",
				"M_ME_TD_1",
				"de.uniol.inf.ei.oj104.model.informationelement.NormalizedValue de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorWithOV",
				"de.uniol.inf.ei.oj104.model.timetag.SevenOctetBinaryTime", "0", "3 5"));
		typesFromGetSingle.add(testSingleASDU(35, "measured value, scaled value with time tag CP56Time2a", "M_ME_TE_1",
				"de.uniol.inf.ei.oj104.model.informationelement.ScaledValue de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorWithOV",
				"de.uniol.inf.ei.oj104.model.timetag.SevenOctetBinaryTime", "0", "3 5"));
		typesFromGetSingle.add(testSingleASDU(36,
				"measured value, short floating point number with time tag CP56Time2a", "M_ME_TF_1",
				"de.uniol.inf.ei.oj104.model.informationelement.ShortFloatingPointNumber de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorWithOV",
				"de.uniol.inf.ei.oj104.model.timetag.SevenOctetBinaryTime", "0", "3 5"));
		typesFromGetSingle.add(testSingleASDU(37, "integrated totals with time tag CP56Time2a", "M_IT_TB_1",
				"de.uniol.inf.ei.oj104.model.informationelement.BinaryCounterReading",
				"de.uniol.inf.ei.oj104.model.timetag.SevenOctetBinaryTime", "0", "3 37 38 39 40 41"));
		typesFromGetSingle.add(testSingleASDU(38, "event of protection equipment with time tag CP56Time2a", "M_EP_TD_1",
				"de.uniol.inf.ei.oj104.model.informationelement.PESingleEvent de.uniol.inf.ei.oj104.model.timetag.TwoOctetBinaryTime",
				"de.uniol.inf.ei.oj104.model.timetag.SevenOctetBinaryTime", "0", "3"));
		typesFromGetSingle.add(testSingleASDU(39,
				"packed start events of protection equipment with time tag CP56Time2a", "M_EP_TE_1",
				"de.uniol.inf.ei.oj104.model.informationelement.PEStartEvent de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorForPE de.uniol.inf.ei.oj104.model.timetag.TwoOctetBinaryTime",
				"de.uniol.inf.ei.oj104.model.timetag.SevenOctetBinaryTime", "0", "3"));
		typesFromGetSingle.add(testSingleASDU(40,
				"packed output circuit information of protection equipment with time tag CP56Time2a", "M_EP_TF_1",
				"de.uniol.inf.ei.oj104.model.informationelement.PEOutputCircuitInformation de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorForPE de.uniol.inf.ei.oj104.model.timetag.TwoOctetBinaryTime",
				"de.uniol.inf.ei.oj104.model.timetag.SevenOctetBinaryTime", "0", "3"));
		typesFromGetSingle.add(testSingleASDU(41, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(42, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(43, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(44, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(45, "single command", "C_SC_NA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.SingleCommand", "", "0", "6 8"));
		typesFromGetSingle.add(testSingleASDU(46, "double command", "C_DC_NA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.DoubleCommand", "", "0", "6 8"));
		typesFromGetSingle.add(testSingleASDU(47, "regulating step command", "C_RC_NA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.RegulatingStepCommand", "", "0", "6 8"));
		typesFromGetSingle.add(testSingleASDU(48, "set point command, normalized value", "C_SE_NA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.NormalizedValue de.uniol.inf.ei.oj104.model.informationelement.SetPointCommandQualifier",
				"", "0", "6 8"));
		typesFromGetSingle.add(testSingleASDU(49, "set point command, scaled value", "C_SE_NB_1",
				"de.uniol.inf.ei.oj104.model.informationelement.ScaledValue de.uniol.inf.ei.oj104.model.informationelement.SetPointCommandQualifier",
				"", "0", "6 8"));
		typesFromGetSingle.add(testSingleASDU(50, "set point command, short floating point number", "C_SE_NC_1",
				"de.uniol.inf.ei.oj104.model.informationelement.ShortFloatingPointNumber de.uniol.inf.ei.oj104.model.informationelement.SetPointCommandQualifier",
				"", "0", "6 8"));
		typesFromGetSingle.add(testSingleASDU(51, "bitstring of 32 bits", "C_BO_NA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.BinaryStateInformation", "", "0", "6 8"));
		typesFromGetSingle.add(testSingleASDU(52, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(53, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(54, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(55, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(56, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(57, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(58, "single command with time tag CP56Time2a", "C_SC_TA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.SingleCommand",
				"de.uniol.inf.ei.oj104.model.timetag.SevenOctetBinaryTime", "0", "7 9 10 44 45 46 47"));
		typesFromGetSingle.add(testSingleASDU(59, "double command with time tag CP56Time2a", "C_DC_TA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.DoubleCommand",
				"de.uniol.inf.ei.oj104.model.timetag.SevenOctetBinaryTime", "0", "7 9 10 44 45 46 47"));
		typesFromGetSingle.add(testSingleASDU(60, "regulating step command with time tag CP56Time2a", "C_RC_TA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.RegulatingStepCommand",
				"de.uniol.inf.ei.oj104.model.timetag.SevenOctetBinaryTime", "0", "7 9 10 44 45 46 47"));
		typesFromGetSingle.add(testSingleASDU(61, "set point command, normalized value with time tag CP56Time2a",
				"C_SE_TA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.NormalizedValue de.uniol.inf.ei.oj104.model.informationelement.SetPointCommandQualifier",
				"de.uniol.inf.ei.oj104.model.timetag.SevenOctetBinaryTime", "0", "7 9 10 44 45 46 47"));
		typesFromGetSingle.add(testSingleASDU(62, "set point command, scaled value with time tag CP56Time2a",
				"C_SE_TB_1",
				"de.uniol.inf.ei.oj104.model.informationelement.ScaledValue de.uniol.inf.ei.oj104.model.informationelement.SetPointCommandQualifier",
				"de.uniol.inf.ei.oj104.model.timetag.SevenOctetBinaryTime", "0", "7 9 10 44 45 46 47"));
		typesFromGetSingle.add(testSingleASDU(63,
				"set point command, short floating point number with time tag CP56Time2a", "C_SE_TC_1",
				"de.uniol.inf.ei.oj104.model.informationelement.ShortFloatingPointNumber de.uniol.inf.ei.oj104.model.informationelement.SetPointCommandQualifier",
				"de.uniol.inf.ei.oj104.model.timetag.SevenOctetBinaryTime", "0", "7 9 10 44 45 46 47"));
		typesFromGetSingle.add(testSingleASDU(64, "bitstring of 32 bits with time tag CP56Time2a", "C_BO_TA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.BinaryStateInformation",
				"de.uniol.inf.ei.oj104.model.timetag.SevenOctetBinaryTime", "0", "7 9 10 44 45 46 47"));
		typesFromGetSingle.add(testSingleASDU(65, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(66, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(67, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(68, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(69, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(70, "end of initialization", "M_EI_NA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.CauseOfInitialization", "", "0", "4"));
		typesFromGetSingle.add(testSingleASDU(71, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(72, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(73, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(74, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(75, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(76, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(77, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(78, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(79, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(80, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(81, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(82, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(83, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(84, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(85, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(86, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(87, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(88, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(89, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(90, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(91, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(92, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(93, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(94, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(95, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(96, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(97, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(98, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(99, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(100, "interrogation command", "C_IC_NA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.InterrogationQualifier", "", "0", "6 8"));
		typesFromGetSingle.add(testSingleASDU(101, "counter interrogation command", "C_CI_NA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.CounterInterrogationCommandQualifier", "", "0", "6"));
		typesFromGetSingle.add(testSingleASDU(102, "read command", "C_RD_NA_1", "", "", "0", "5"));
		typesFromGetSingle.add(testSingleASDU(103, "clock synchronization command", "C_CS_NA_1",
				"de.uniol.inf.ei.oj104.model.timetag.SevenOctetBinaryTime", "", "0", "6"));
		typesFromGetSingle.add(testSingleASDU(104, "test command", "C_TS_NA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.FixedTestBitPattern", "", "0", "6"));
		typesFromGetSingle.add(testSingleASDU(105, "reset process command", "C_RP_NA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.ResetProcessCommandQualifier", "", "0", "6"));
		typesFromGetSingle.add(testSingleASDU(106, "delay acquisition command", "C_CD_NA_1",
				"de.uniol.inf.ei.oj104.model.timetag.TwoOctetBinaryTime", "", "0", "3 6"));
		typesFromGetSingle.add(testSingleASDU(107, "test command with time tag CP56Time2a", "C_TS_TA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.FixedTestBitPattern",
				"de.uniol.inf.ei.oj104.model.timetag.SevenOctetBinaryTime", "0", "6"));
		typesFromGetSingle.add(testSingleASDU(108, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(109, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(110, "parameter of measured value, normalized value", "P_ME_NA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.NormalizedValue de.uniol.inf.ei.oj104.model.informationelement.MeasuredValuesParameterQualifier",
				"", "0", "6"));
		typesFromGetSingle.add(testSingleASDU(111, "parameter of measured value, scaled value", "P_ME_NB_1",
				"de.uniol.inf.ei.oj104.model.informationelement.ScaledValue de.uniol.inf.ei.oj104.model.informationelement.MeasuredValuesParameterQualifier",
				"", "0", "6"));
		typesFromGetSingle.add(testSingleASDU(112, "parameter of measured value, short floating point number",
				"P_ME_NC_1",
				"de.uniol.inf.ei.oj104.model.informationelement.ShortFloatingPointNumber de.uniol.inf.ei.oj104.model.informationelement.MeasuredValuesParameterQualifier",
				"", "0", "6"));
		typesFromGetSingle.add(testSingleASDU(113, "parameter activation", "P_AC_NA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.ParameterActivationQualifier", "", "0", "6 8"));
		typesFromGetSingle.add(testSingleASDU(114, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(115, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(116, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(117, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(118, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(119, "reserved for further compatible definitions", "", "", "", "", ""));
		typesFromGetSingle.add(testSingleASDU(120, "file ready", "F_FR_NA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.NameOfFile de.uniol.inf.ei.oj104.model.informationelement.FileOrSectionLength de.uniol.inf.ei.oj104.model.informationelement.FileReadyQualifier",
				"", "0", "13 44 45 46 47"));
		typesFromGetSingle.add(testSingleASDU(121, "section ready", "F_SR_NA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.NameOfFile de.uniol.inf.ei.oj104.model.informationelement.NameOfSection de.uniol.inf.ei.oj104.model.informationelement.FileOrSectionLength de.uniol.inf.ei.oj104.model.informationelement.SectionReadyQualifier",
				"", "0", "13 44 45 46 47"));
		typesFromGetSingle.add(testSingleASDU(122, "call directory, select file, call file, call section", "F_SC_NA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.NameOfFile de.uniol.inf.ei.oj104.model.informationelement.NameOfSection de.uniol.inf.ei.oj104.model.informationelement.SelectAndCallQualifier",
				"", "0", "13 44 45 46 47"));
		typesFromGetSingle.add(testSingleASDU(123, "last section, last segment", "F_LS_NA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.NameOfFile de.uniol.inf.ei.oj104.model.informationelement.NameOfSection de.uniol.inf.ei.oj104.model.informationelement.LastSectionOrSegmentQualifier de.uniol.inf.ei.oj104.model.informationelement.Checksum",
				"", "0", "13 44 45 46 47"));
		typesFromGetSingle.add(testSingleASDU(124, "ack file, ack section", "F_AF_NA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.NameOfFile de.uniol.inf.ei.oj104.model.informationelement.NameOfSection de.uniol.inf.ei.oj104.model.informationelement.AckFileOrSegmentQualifier",
				"", "0", "13 44 45 46 47"));
		typesFromGetSingle.add(testSingleASDU(125, "segment", "F_SG_NA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.NameOfFile de.uniol.inf.ei.oj104.model.informationelement.NameOfSection de.uniol.inf.ei.oj104.model.informationelement.Segment",
				"", "0", "13 44 45 46 47"));
		typesFromGetSingle.add(testSingleASDU(126, "directory", "F_DR_TA_1",
				"de.uniol.inf.ei.oj104.model.informationelement.NameOfFile de.uniol.inf.ei.oj104.model.informationelement.FileOrSectionLength de.uniol.inf.ei.oj104.model.informationelement.FileStatus",
				"de.uniol.inf.ei.oj104.model.timetag.SevenOctetBinaryTime", "1", "3 5"));

		assertTrue("Methods getAllASDUTypes and getASDUType do not return the same amount of ASDU types",
				typesFromGetAll.size() == typesFromGetSingle.size());
		assertTrue("Methods getAllASDUTypes and getASDUType do not return the same ASDU types",
				typesFromGetAll.containsAll(typesFromGetSingle));
	}

	/**
	 * Tests, if all information for an {@link ASDU} are loaded and valid in the
	 * factory.
	 * 
	 * @param id          The ID of the {@link ASDUType}. The IDs are subsequent
	 *                    numbers defined in the standard.
	 * @param description The description of the {@link ASDUType}. The descriptions
	 *                    are human readable and defined in the standard.
	 * @param code        The code of the {@link ASDUType}, e.g. "M_SP_NA_1". The
	 *                    codes are defined in the standard.
	 * @param ieClasses   The list of used information element classes (full
	 *                    qualified class names) in the {@link ASDUType}.
	 * @param ttClass     The used time tag class (full qualified class names) in
	 *                    the {@link ASDUType}. May be null.
	 * @param sqs         The list of possible structure qualifiers (their codes) in
	 *                    the {@link ASDUType}.
	 * @param cots        The list of possible causes of transmission (their codes)
	 *                    in the {@link ASDUType}.
	 * @return The type of the {@link ASDU}.
	 */
	private ASDUType testSingleASDU(int id, String description, String code, String ieClasses, String ttClass,
			String sqs, String cots) {
		ASDUType type = ASDUFactory.getASDUType(id).orElse(null);
		assertNotNull("No ASDU with id=" + id + "in dictionary", type);
		assertEquals("id of ASDU does not match! Expected=" + id + ", Actual=" + type.getId(), id, type.getId());
		assertEquals("description of ASDU(" + id + ") does not match! Expected=" + description + ", Actual="
				+ type.getDescription(), description, type.getDescription());
		assertEquals("code of ASDU(" + id + ") does not match! Expected=" + code + ", Actual=" + type.getCode(), code,
				type.getCode());
		StringBuffer ieList = new StringBuffer();
		ASDUFactory.getInformationElementClasses(id).forEach(ie -> ieList.append(ie.getName() + " "));
		if (ieList.length() > 0) {
			ieList.setLength(ieList.length() - 1);
		}
		assertEquals("information element classes of ASDU(" + id + ") do not match! Expected=" + ieClasses + ", Actual="
				+ ieList.toString(), ieClasses, ieList.toString());
		String actualTtClassName = ASDUFactory.getTimeTagClass(id).isPresent()
				? ASDUFactory.getTimeTagClass(id).get().getName()
				: "";
		assertEquals("time tag class of ASDU(" + id + ") does not match! Expected=" + ttClass + ", Actual="
				+ actualTtClassName, ttClass, actualTtClassName);
		StringBuffer sqList = new StringBuffer();
		ASDUFactory.getStructureQualifiers(id).forEach(sq -> sqList.append(sq.getCode() + " "));
		if (sqList.length() > 0) {
			sqList.setLength(sqList.length() - 1);
		}
		assertEquals("structure qualifiers of ASDU(" + id + ") do not match! Expected=" + sqs + ", Actual="
				+ sqList.toString(), sqs, sqList.toString());
		StringBuffer cotList = new StringBuffer();
		ASDUFactory.getCausesOfTransmission(id).forEach(cot -> cotList.append(cot.getId() + " "));
		if (cotList.length() > 0) {
			cotList.setLength(cotList.length() - 1);
		}
		assertEquals("caus of transmission ids of ASDU(" + id + ") do not match! Expected=" + cots + ", Actual="
				+ cotList.toString(), cots, cotList.toString());
		return type;
	}

}