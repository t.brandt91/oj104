/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.junit.Test;

import de.uniol.inf.ei.oj104.model.ASDU;
import de.uniol.inf.ei.oj104.model.ASDUAddress;
import de.uniol.inf.ei.oj104.model.ASDUType;
import de.uniol.inf.ei.oj104.model.CauseOfTransmission;
import de.uniol.inf.ei.oj104.model.Confirm;
import de.uniol.inf.ei.oj104.model.DataUnitIdentifier;
import de.uniol.inf.ei.oj104.model.DataUnitType;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.model.IInformationObject;
import de.uniol.inf.ei.oj104.model.ITimeTag;
import de.uniol.inf.ei.oj104.model.InformationElementSequence;
import de.uniol.inf.ei.oj104.model.SequenceInformationObject;
import de.uniol.inf.ei.oj104.model.SingleInformationObject;
import de.uniol.inf.ei.oj104.model.StructureQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.AckFileOrSegmentQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.BinaryCounterReading;
import de.uniol.inf.ei.oj104.model.informationelement.BinaryStateInformation;
import de.uniol.inf.ei.oj104.model.informationelement.CauseOfInitialization;
import de.uniol.inf.ei.oj104.model.informationelement.Checksum;
import de.uniol.inf.ei.oj104.model.informationelement.DoubleCommand;
import de.uniol.inf.ei.oj104.model.informationelement.DoublePointInformation;
import de.uniol.inf.ei.oj104.model.informationelement.FileOrSectionLength;
import de.uniol.inf.ei.oj104.model.informationelement.FileReadyQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.FileStatus;
import de.uniol.inf.ei.oj104.model.informationelement.FixedTestBitPattern;
import de.uniol.inf.ei.oj104.model.informationelement.InterrogationQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.LastSectionOrSegmentQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.MeasuredValuesParameterQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.NameOfFile;
import de.uniol.inf.ei.oj104.model.informationelement.NameOfSection;
import de.uniol.inf.ei.oj104.model.informationelement.NormalizedValue;
import de.uniol.inf.ei.oj104.model.informationelement.PEOutputCircuitInformation;
import de.uniol.inf.ei.oj104.model.informationelement.PEStartEvent;
import de.uniol.inf.ei.oj104.model.informationelement.ParameterActivationQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorForPE;
import de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorWithOV;
import de.uniol.inf.ei.oj104.model.informationelement.RegulatingStepCommand;
import de.uniol.inf.ei.oj104.model.informationelement.ResetProcessCommandQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.ScaledValue;
import de.uniol.inf.ei.oj104.model.informationelement.SectionReadyQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.Segment;
import de.uniol.inf.ei.oj104.model.informationelement.SelectAndCallQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.SetPointCommandQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.ShortFloatingPointNumber;
import de.uniol.inf.ei.oj104.model.informationelement.SingleCommand;
import de.uniol.inf.ei.oj104.model.informationelement.SinglePointInformation;
import de.uniol.inf.ei.oj104.model.informationelement.StatusAndChangeDetection;
import de.uniol.inf.ei.oj104.model.informationelement.ValueWithTransientStateIndication;
import de.uniol.inf.ei.oj104.model.timetag.SevenOctetBinaryTime;
import de.uniol.inf.ei.oj104.model.timetag.TwoOctetBinaryTime;

/**
 * Class that contains test for {@link JsonParser}.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class JsonParserTest {

	/**
	 * Creates a test with a random ASDU with given characteristics.
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}. The IDs are subsequent
	 *                   numbers defined in the standard.
	 * @param ieClasses  The list of used information element classes in the
	 *                   {@link ASDUType}.
	 * @param ieCreators The list of constructors for the used information element
	 *                   classes.
	 */
	private void testWithRandomASDU(int asduTypeId, List<Class<? extends IInformationElement>> ieClasses,
			List<Method> ieCreators) {
		Random random = new Random();

		// Create data unit identifier
		StructureQualifier sq = StructureQualifier.values()[random.nextInt(2)];
		ASDUType asduType = ASDUFactory.getASDUType(asduTypeId).get();
		int numIEs = random.nextInt(10);
		DataUnitType dataUnitType = new DataUnitType(asduType, sq, numIEs);
		List<CauseOfTransmission> cots = ASDUFactory.getCausesOfTransmission(asduTypeId);
		CauseOfTransmission cot = cots.get(random.nextInt(cots.size()));
		Confirm confirm = Confirm.values()[random.nextInt(Confirm.values().length)];
		boolean test = random.nextBoolean();
		int originatorAddress = 0;
		ASDUAddress asduAddress = new ASDUAddress(random.nextInt(Short.MAX_VALUE));
		DataUnitIdentifier dataUnitIdentifier = new DataUnitIdentifier(dataUnitType, cot, confirm, test,
				originatorAddress, asduAddress);

		// Create information objects
		Optional<Class<? extends ITimeTag>> timeTagClass = ASDUFactory.getTimeTagClass(asduTypeId);
		ITimeTag timeTag = null;
		if (timeTagClass.isPresent()) {
			timeTag = RandomInformationElementCreator.createTimeTag(timeTagClass.get());
		}
		List<IInformationObject> ios = new ArrayList<>();
		int ioAddress = random.nextInt(Integer.MAX_VALUE);
		List<List<IInformationElement>> ies = new ArrayList<>(numIEs);
		for (int i = 0; i < numIEs; i++) {
			List<IInformationElement> innerIEs = new ArrayList<>(ieCreators.size());
			for (Method ieCreator : ieCreators) {
				try {
					IInformationElement ie = (IInformationElement) ieCreator.invoke(this);
					innerIEs.add(ie);
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					fail();
				}
			}
			ies.add(innerIEs);
		}

		if (sq == StructureQualifier.SEQUENCE) {
			List<InformationElementSequence> ieSequences = new ArrayList<>(numIEs);
			for (int i = 0; i < numIEs; i++) {
				List<IInformationElement> sequenceIEs = new ArrayList<>(1);
				for (IInformationElement ie : ies.get(i)) {
					sequenceIEs.add(ie);
				}
				ieSequences.add(new InformationElementSequence(ieClasses, sequenceIEs,
						timeTagClass.orElseGet(() -> null), timeTag));
			}
			ios.add(new SequenceInformationObject(ieClasses, timeTagClass.orElseGet(() -> null), numIEs, ioAddress,
					ieSequences));
		} else {
			// SINGLE
			for (int i = 0; i < numIEs; i++) {
				List<IInformationElement> sequenceIEs = new ArrayList<>(ieCreators.size());
				for (IInformationElement ie : ies.get(i)) {
					sequenceIEs.add(ie);
				}
				ios.add(new SingleInformationObject(ieClasses, sequenceIEs, timeTagClass.orElseGet(() -> null), timeTag,
						ioAddress));
			}
		}

		// Create ASDU
		ASDU asdu = new ASDU(dataUnitIdentifier, ios);

		// Create json
		String json = asdu.toString();

		// Create asdu from json
		ASDU parsedASDU = (ASDU) JsonParser.deserialize(json, ASDU.class);
		assertEquals(asdu, parsedASDU);
	}

	// Single-point information
	@Test
	public void testASDU_M_SP_XX_1() {
		int[] asduTypeIds = new int[] { 1, 2, 30 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(1);
		ieClasses.add(SinglePointInformation.class);
		List<Method> ieCreators = new ArrayList<>(1);
		try {
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createSinglePointInformationElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// Double-point information
	@Test
	public void testASDU_M_DP_XX_1() {
		int[] asduTypeIds = new int[] { 3, 4, 31 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(1);
		ieClasses.add(DoublePointInformation.class);
		List<Method> ieCreators = new ArrayList<>(1);
		try {
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createDoublePointInformationElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// Step position information
	@Test
	public void testASDU_M_ST_XX_1() {
		int[] asduTypeIds = new int[] { 5, 6, 32 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(2);
		ieClasses.add(ValueWithTransientStateIndication.class);
		ieClasses.add(QualityDescriptorWithOV.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators.add(RandomInformationElementCreator.class
					.getDeclaredMethod("createValueWithTransientStateIndicationElement"));
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createQualityDescriptorWithOVElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// Bitstring of 32 bits
	@Test
	public void testASDU_M_BO_XX_1() {
		int[] asduTypeIds = new int[] { 7, 8, 33 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(2);
		ieClasses.add(BinaryStateInformation.class);
		ieClasses.add(QualityDescriptorWithOV.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createBinaryStateInformationElement"));
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createQualityDescriptorWithOVElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// measured value, normalized value
	@Test
	public void testASDU_M_ME_XX1_1() {
		int[] asduTypeIds = new int[] { 9, 10, 34 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(2);
		ieClasses.add(NormalizedValue.class);
		ieClasses.add(QualityDescriptorWithOV.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNormalizedValueElement"));
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createQualityDescriptorWithOVElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// measured value, scaled value
	@Test
	public void testASDU_M_ME_XX2_1() {
		int[] asduTypeIds = new int[] { 11, 12, 35 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(2);
		ieClasses.add(ScaledValue.class);
		ieClasses.add(QualityDescriptorWithOV.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createScaledValueElement"));
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createQualityDescriptorWithOVElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// measured value, short floating point number
	@Test
	public void testASDU_M_ME_XX3_1() {
		int[] asduTypeIds = new int[] { 13, 14, 36 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(2);
		ieClasses.add(ShortFloatingPointNumber.class);
		ieClasses.add(QualityDescriptorWithOV.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createShortFloatingPointNumberElement"));
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createQualityDescriptorWithOVElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// integrated totals
	@Test
	public void testASDU_M_IT_XX_1() {
		int[] asduTypeIds = new int[] { 15, 16, 37 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(2);
		ieClasses.add(BinaryCounterReading.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators
					.add(RandomInformationElementCreator.class.getDeclaredMethod("createBinaryCounterReadingElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// event of protection equipment with time tag
	@Test
	public void testASDU_M_EP_TX1_1() {
		int[] asduTypeIds = new int[] { 17, 38 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(1);
		ieClasses.add(TwoOctetBinaryTime.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createTwoOctetBinaryTimeElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// packed start events of protection equipment with time tag
	@Test
	public void testASDU_M_EP_TX2_1() {
		int[] asduTypeIds = new int[] { 18, 39 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(3);
		ieClasses.add(PEStartEvent.class);
		ieClasses.add(QualityDescriptorForPE.class);
		ieClasses.add(TwoOctetBinaryTime.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createPEStartEventElement"));
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createQualityDescriptorForPEElement"));
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createTwoOctetBinaryTimeElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// packed output circuit information of protection equipment with time tag
	@Test
	public void testASDU_M_EP_TX3_1() {
		int[] asduTypeIds = new int[] { 19, 40 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(3);
		ieClasses.add(PEOutputCircuitInformation.class);
		ieClasses.add(QualityDescriptorForPE.class);
		ieClasses.add(TwoOctetBinaryTime.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createPEOutputCircuitInformationElement"));
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createQualityDescriptorForPEElement"));
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createTwoOctetBinaryTimeElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// packed single-point information with status change detection
	@Test
	public void testASDU_M_PS_NA_1() {
		int[] asduTypeIds = new int[] { 20 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(2);
		ieClasses.add(StatusAndChangeDetection.class);
		ieClasses.add(QualityDescriptorWithOV.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createStatusAndChangeDetectionElement"));
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createQualityDescriptorWithOVElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// measured value, normalized value without quality descriptor
	@Test
	public void testASDU_M_ME_ND_1() {
		int[] asduTypeIds = new int[] { 21 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(1);
		ieClasses.add(NormalizedValue.class);
		List<Method> ieCreators = new ArrayList<>(1);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNormalizedValueElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// single command
	@Test
	public void testASDU_C_SC_XA_1() {
		int[] asduTypeIds = new int[] { 45, 58 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(1);
		ieClasses.add(SingleCommand.class);
		List<Method> ieCreators = new ArrayList<>(1);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createSingleCommandElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// double command
	@Test
	public void testASDU_C_DC_XA_1() {
		int[] asduTypeIds = new int[] { 46, 59 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(1);
		ieClasses.add(DoubleCommand.class);
		List<Method> ieCreators = new ArrayList<>(1);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createDoubleCommandElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// regulating step command
	@Test
	public void testASDU_C_RC_XA_1() {
		int[] asduTypeIds = new int[] { 47, 60 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(1);
		ieClasses.add(RegulatingStepCommand.class);
		List<Method> ieCreators = new ArrayList<>(1);
		try {
			ieCreators
					.add(RandomInformationElementCreator.class.getDeclaredMethod("createRegulatingStepCommandElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// set point command, normalized value
	@Test
	public void testASDU_C_SE_XA_1() {
		int[] asduTypeIds = new int[] { 48, 61 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(2);
		ieClasses.add(NormalizedValue.class);
		ieClasses.add(SetPointCommandQualifier.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNormalizedValueElement"));
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createSetPointCommandQualifierElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// set point command, scaled value
	@Test
	public void testASDU_C_SE_XB_1() {
		int[] asduTypeIds = new int[] { 49, 62 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(2);
		ieClasses.add(ScaledValue.class);
		ieClasses.add(SetPointCommandQualifier.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createScaledValueElement"));
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createSetPointCommandQualifierElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// set point command, short floating point number
	@Test
	public void testASDU_C_SE_XC_1() {
		int[] asduTypeIds = new int[] { 50, 63 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(2);
		ieClasses.add(ShortFloatingPointNumber.class);
		ieClasses.add(SetPointCommandQualifier.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createShortFloatingPointNumberElement"));
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createSetPointCommandQualifierElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// bitstring of 32 bits
	@Test
	public void testASDU_C_BO_XA_1() {
		int[] asduTypeIds = new int[] { 51, 64 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(1);
		ieClasses.add(BinaryStateInformation.class);
		List<Method> ieCreators = new ArrayList<>(1);
		try {
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createBinaryStateInformationElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// end of initialization
	@Test
	public void testASDU_M_EI_NA_1() {
		int[] asduTypeIds = new int[] { 70 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(1);
		ieClasses.add(CauseOfInitialization.class);
		List<Method> ieCreators = new ArrayList<>(1);
		try {
			ieCreators
					.add(RandomInformationElementCreator.class.getDeclaredMethod("createCauseOfInitializationElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// interrogation command
	@Test
	public void testASDU_C_IC_NA_1() {
		int[] asduTypeIds = new int[] { 100 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(1);
		ieClasses.add(InterrogationQualifier.class);
		List<Method> ieCreators = new ArrayList<>(1);
		try {
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createInterrogationQualifierElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// read command
	@Test
	public void testASDU_C_RD_NA_1() {
		int[] asduTypeIds = new int[] { 102 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>();
		List<Method> ieCreators = new ArrayList<>();

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// clock synchronization command
	@Test
	public void testASDU_C_CS_NA_1() {
		int[] asduTypeIds = new int[] { 103 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(1);
		ieClasses.add(SevenOctetBinaryTime.class);
		List<Method> ieCreators = new ArrayList<>(1);
		try {
			ieCreators
					.add(RandomInformationElementCreator.class.getDeclaredMethod("createSevenOctetBinaryTimeElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// test command
	@Test
	public void testASDU_C_TS_NA_1() {
		int[] asduTypeIds = new int[] { 104, 107 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(1);
		ieClasses.add(FixedTestBitPattern.class);
		List<Method> ieCreators = new ArrayList<>(1);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createFixedTestBitPatternElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// reset process command
	@Test
	public void testASDU_C_RP_NA_1() {
		int[] asduTypeIds = new int[] { 105 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(1);
		ieClasses.add(ResetProcessCommandQualifier.class);
		List<Method> ieCreators = new ArrayList<>(1);
		try {
			ieCreators.add(RandomInformationElementCreator.class
					.getDeclaredMethod("createResetProcessCommandQualifierElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// delay acquisition command
	@Test
	public void testASDU_C_CD_NA_1() {
		int[] asduTypeIds = new int[] { 106 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(1);
		ieClasses.add(TwoOctetBinaryTime.class);
		List<Method> ieCreators = new ArrayList<>(1);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createTwoOctetBinaryTimeElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// parameter of measured value, normalized value
	@Test
	public void testASDU_P_ME_NA_1() {
		int[] asduTypeIds = new int[] { 110 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(2);
		ieClasses.add(NormalizedValue.class);
		ieClasses.add(MeasuredValuesParameterQualifier.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNormalizedValueElement"));
			ieCreators.add(RandomInformationElementCreator.class
					.getDeclaredMethod("createMeasuredValuesParameterQualifierElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// parameter of measured value, scaled value
	@Test
	public void testASDU_P_ME_NB_1() {
		int[] asduTypeIds = new int[] { 111 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(2);
		ieClasses.add(ScaledValue.class);
		ieClasses.add(MeasuredValuesParameterQualifier.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createScaledValueElement"));
			ieCreators.add(RandomInformationElementCreator.class
					.getDeclaredMethod("createMeasuredValuesParameterQualifierElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// parameter of measured value, scaled value
	@Test
	public void testASDU_P_ME_NC_1() {
		int[] asduTypeIds = new int[] { 112 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(2);
		ieClasses.add(ShortFloatingPointNumber.class);
		ieClasses.add(MeasuredValuesParameterQualifier.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createShortFloatingPointNumberElement"));
			ieCreators.add(RandomInformationElementCreator.class
					.getDeclaredMethod("createMeasuredValuesParameterQualifierElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// parameter activation
	@Test
	public void testASDU_P_AC_NA_1() {
		int[] asduTypeIds = new int[] { 113 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(1);
		ieClasses.add(ParameterActivationQualifier.class);
		List<Method> ieCreators = new ArrayList<>(1);
		try {
			ieCreators.add(RandomInformationElementCreator.class
					.getDeclaredMethod("createParameterActivationQualifierElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// file ready
	@Test
	public void testASDU_F_FR_NA_1() {
		int[] asduTypeIds = new int[] { 120 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(3);
		ieClasses.add(NameOfFile.class);
		ieClasses.add(FileOrSectionLength.class);
		ieClasses.add(FileReadyQualifier.class);
		List<Method> ieCreators = new ArrayList<>(3);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNameOfFileElement"));
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createFileOrSectionLengthElement"));
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createFileReadyQualifierElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// section ready
	@Test
	public void testASDU_F_SR_NA_1() {
		int[] asduTypeIds = new int[] { 121 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(4);
		ieClasses.add(NameOfFile.class);
		ieClasses.add(NameOfSection.class);
		ieClasses.add(FileOrSectionLength.class);
		ieClasses.add(SectionReadyQualifier.class);
		List<Method> ieCreators = new ArrayList<>(4);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNameOfFileElement"));
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNameOfSectionElement"));
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createFileOrSectionLengthElement"));
			ieCreators
					.add(RandomInformationElementCreator.class.getDeclaredMethod("createSectionReadyQualifierElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// call directory, select file, call file, call section
	@Test
	public void testASDU_F_SC_NA_1() {
		int[] asduTypeIds = new int[] { 122 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(3);
		ieClasses.add(NameOfFile.class);
		ieClasses.add(NameOfSection.class);
		ieClasses.add(SelectAndCallQualifier.class);
		List<Method> ieCreators = new ArrayList<>(3);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNameOfFileElement"));
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNameOfSectionElement"));
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createSelectAndCallQualifierElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// last section, last segment
	@Test
	public void testASDU_F_LS_NA_1() {
		int[] asduTypeIds = new int[] { 123 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(4);
		ieClasses.add(NameOfFile.class);
		ieClasses.add(NameOfSection.class);
		ieClasses.add(LastSectionOrSegmentQualifier.class);
		ieClasses.add(Checksum.class);
		List<Method> ieCreators = new ArrayList<>(4);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNameOfFileElement"));
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNameOfSectionElement"));
			ieCreators.add(RandomInformationElementCreator.class
					.getDeclaredMethod("createLastSectionOrSegmentQualifierElement"));
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createChecksumElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// ack file, ack section
	@Test
	public void testASDU_F_AF_NA_1() {
		int[] asduTypeIds = new int[] { 124 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(3);
		ieClasses.add(NameOfFile.class);
		ieClasses.add(NameOfSection.class);
		ieClasses.add(AckFileOrSegmentQualifier.class);
		List<Method> ieCreators = new ArrayList<>(3);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNameOfFileElement"));
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNameOfSectionElement"));
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createAckFileOrSegmentQualifierElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// segment
	@Test
	public void testASDU_F_SG_NA_1() {
		int[] asduTypeIds = new int[] { 125 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(4);
		ieClasses.add(NameOfFile.class);
		ieClasses.add(NameOfSection.class);
		ieClasses.add(Segment.class);
		List<Method> ieCreators = new ArrayList<>(4);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNameOfFileElement"));
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNameOfSectionElement"));
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createSegmentElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

	// directory
	@Test
	public void testASDU_F_DR_TA_1() {
		int[] asduTypeIds = new int[] { 126 };
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(3);
		ieClasses.add(NameOfFile.class);
		ieClasses.add(FileOrSectionLength.class);
		ieClasses.add(FileStatus.class);
		List<Method> ieCreators = new ArrayList<>(3);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNameOfFileElement"));
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createFileOrSectionLengthElement"));
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createFileStatusElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
			fail();
		}

		for (int asduTypeId : asduTypeIds) {
			testWithRandomASDU(asduTypeId, ieClasses, ieCreators);
		}
	}

}