/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * Information element that contains a name of a section.
 * <p>
 * Abbreviation: NOS
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
@JsonIgnoreProperties({ "default" })
public class NameOfSection extends ByteValue implements IInformationElement {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = -4549960744595892915L;

	/**
	 * Checks, whether the name of the section is default.
	 * 
	 * @return {@link #getValue()} == 0.
	 */
	public boolean isDefault() {
		return getValue() == 0;
	}

	/**
	 * Empty default constructor.
	 */
	public NameOfSection() {
		super();
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param value The name of the section.
	 */
	public NameOfSection(byte value) {
		super(value);
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

}