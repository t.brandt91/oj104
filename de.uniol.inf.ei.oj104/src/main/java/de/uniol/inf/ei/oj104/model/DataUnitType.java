/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model;

import java.util.Arrays;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.util.ASDUFactory;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * A data unit type contains the information about the {@link ASDUType}, the
 * {@link StructureQualifier}, and the number of information elements in the
 * {@link ASDU}.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class DataUnitType implements IByteEncodedEntity {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = -7104599408405487669L;

	/**
	 * The ASDU type defines the structure, type and format of the following
	 * {@link IInformationObject}s. An ASDU type has an id, a code and a
	 * description. The ids and codes are defined in the standard. The ids range
	 * from 0 to 127, while the code is also a unique string, if not empty, e.g.
	 * "M_SP_NA_1". For reserved ids, the code is in the standard empty. The
	 * description is not necessary to be machine readable but may be useful for
	 * humans.
	 */
	private ASDUType typeIdentification;

	/**
	 * A structure qualifiers qualifies the structure of the following
	 * {@link IInformationObject}s as follows:
	 * <p>
	 * If the qualifier is {@link StructureQualifier#SINGLE}, individual single
	 * {@link IInformationElement}s or a combination of {@link IInformationElement}s
	 * in a number of {@link IInformationObject}s of the same type are addressed.
	 * <p>
	 * If the qualifier is {@link StructureQualifier#SEQUENCE}, an
	 * {@link InformationElementSequence} of single {@link IInformationElement}s or
	 * equal combinations of {@link IInformationElement}s of a single object are
	 * addressed.
	 * <p>
	 * This is done by providing a constructor for either
	 * {@link SingleInformationObject}s or {@link SequenceInformationObject}s.
	 */
	private StructureQualifier structureQualifier;

	/**
	 * The total amount of the {@link IInformationElement}s in the {@link ASDU} over
	 * all {@link IInformationObject}s.
	 */
	private int numberOfInformationElements;

	/**
	 * Returns the ASDU type that defines the structure, type and format of the
	 * following {@link IInformationObject}s.
	 * 
	 * @return An ASDU type with an id, a code and a description. The ids and codes
	 *         are defined in the standard. The ids range from 0 to 127, while the
	 *         code is also a unique string, if not empty, e.g. "M_SP_NA_1". For
	 *         reserved ids, the code is in the standard empty. The description is
	 *         not necessary to be machine readable but may be useful for humans.
	 */
	public ASDUType getTypeIdentification() {
		return typeIdentification;
	}

	/**
	 * Sets the ASDU type that defines the structure, type and format of the
	 * following {@link IInformationObject}s.
	 * 
	 * @param typeIdentification An ASDU type with an id, a code and a description.
	 *                           The ids and codes are defined in the standard. The
	 *                           ids range from 0 to 127, while the code is also a
	 *                           unique string, if not empty, e.g. "M_SP_NA_1". For
	 *                           reserved ids, the code is in the standard empty.
	 *                           The description is not necessary to be machine
	 *                           readable but may be useful for humans.
	 */
	public void setTypeIdentification(ASDUType typeIdentification) {
		this.typeIdentification = typeIdentification;
	}

	/**
	 * Return the structure qualifiers that qualifies the structure of the following
	 * {@link IInformationObject}s.
	 * 
	 * @return Either {@link StructureQualifier#SINGLE} or
	 *         {@link StructureQualifier#SEQUENCE}. If the qualifier is
	 *         {@link StructureQualifier#SINGLE}, individual single
	 *         {@link IInformationElement}s or a combination of
	 *         {@link IInformationElement}s in a number of
	 *         {@link IInformationObject}s of the same type are addressed. If the
	 *         qualifier is {@link StructureQualifier#SEQUENCE}, an
	 *         {@link InformationElementSequence} of single
	 *         {@link IInformationElement}s or equal combinations of
	 *         {@link IInformationElement}s of a single object are addressed.
	 */
	public StructureQualifier getStructureQualifier() {
		return structureQualifier;
	}

	/**
	 * Sets the structure qualifiers that qualifies the structure of the following
	 * {@link IInformationObject}s.
	 * 
	 * @param structureQualifier Either {@link StructureQualifier#SINGLE} or
	 *                           {@link StructureQualifier#SEQUENCE}. If the
	 *                           qualifier is {@link StructureQualifier#SINGLE},
	 *                           individual single {@link IInformationElement}s or a
	 *                           combination of {@link IInformationElement}s in a
	 *                           number of {@link IInformationObject}s of the same
	 *                           type are addressed. If the qualifier is
	 *                           {@link StructureQualifier#SEQUENCE}, an
	 *                           {@link InformationElementSequence} of single
	 *                           {@link IInformationElement}s or equal combinations
	 *                           of {@link IInformationElement}s of a single object
	 *                           are addressed.
	 */
	public void setStructureQualifier(StructureQualifier structureQualifier) {
		this.structureQualifier = structureQualifier;
	}

	/**
	 * Returns the total amount of the {@link IInformationElement}s in the
	 * {@link ASDU} over all {@link IInformationObject}s.
	 * 
	 * @return An integer greater than 0.
	 */
	public int getNumberOfInformationElements() {
		return numberOfInformationElements;
	}

	/**
	 * Sets the total amount of the {@link IInformationElement}s in the {@link ASDU}
	 * over all {@link IInformationObject}s.
	 * 
	 * @param numberOfInformationElements and integer greater than 0.
	 */
	public void setNumberOfInformationElements(int numberOfInformationElements) {
		this.numberOfInformationElements = numberOfInformationElements;
	}

	/**
	 * Empty default constructor.
	 */
	public DataUnitType() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param typeIdentification          The ASDU type defines the structure, type
	 *                                    and format of the following
	 *                                    {@link IInformationObject}s. An ASDU type
	 *                                    has an id, a code and a description. The
	 *                                    ids and codes are defined in the standard.
	 *                                    The ids range from 0 to 127, while the
	 *                                    code is also a unique string, if not
	 *                                    empty, e.g. "M_SP_NA_1". For reserved ids,
	 *                                    the code is in the standard empty. The
	 *                                    description is not necessary to be machine
	 *                                    readable but may be useful for humans.
	 * @param structureQualifier          A structure qualifiers qualifies the
	 *                                    structure of the following
	 *                                    {@link IInformationObject}s as follows:
	 *                                    <p>
	 *                                    If the qualifier is
	 *                                    {@link StructureQualifier#SINGLE},
	 *                                    individual single
	 *                                    {@link IInformationElement}s or a
	 *                                    combination of
	 *                                    {@link IInformationElement}s in a number
	 *                                    of {@link IInformationObject}s of the same
	 *                                    type are addressed.
	 *                                    <p>
	 *                                    If the qualifier is
	 *                                    {@link StructureQualifier#SEQUENCE}, an
	 *                                    {@link InformationElementSequence} of
	 *                                    single {@link IInformationElement}s or
	 *                                    equal combinations of
	 *                                    {@link IInformationElement}s of a single
	 *                                    object are addressed.
	 *                                    <p>
	 *                                    This is done by providing a constructor
	 *                                    for either
	 *                                    {@link SingleInformationObject}s or
	 *                                    {@link SequenceInformationObject}s.
	 * @param numberOfInformationElements The total amount of the
	 *                                    {@link IInformationElement}s in the
	 *                                    {@link ASDU} over all
	 *                                    {@link IInformationObject}s.
	 */
	public DataUnitType(ASDUType typeIdentification, StructureQualifier structureQualifier,
			int numberOfInformationElements) {
		this.typeIdentification = typeIdentification;
		this.structureQualifier = structureQualifier;
		this.numberOfInformationElements = numberOfInformationElements;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + numberOfInformationElements;
		result = prime * result + ((structureQualifier == null) ? 0 : structureQualifier.hashCode());
		result = prime * result + ((typeIdentification == null) ? 0 : typeIdentification.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		DataUnitType other = (DataUnitType) obj;
		if (numberOfInformationElements != other.numberOfInformationElements) {
			return false;
		}
		if (structureQualifier != other.structureQualifier) {
			return false;
		}
		if (typeIdentification == null) {
			if (other.typeIdentification != null) {
				return false;
			}
		} else if (!typeIdentification.equals(other.typeIdentification)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		}

		typeIdentification = ASDUFactory.getASDUType(bytes[0] & 0xff)
				.orElseThrow(() -> new IEC608705104ProtocolException(getClass(),
						"Can not determine ASDU type from " + (bytes[0] & 0xff) + "!"));
		numberOfInformationElements = bytes[1] & 0x7f;
		structureQualifier = StructureQualifier.getQualifier((((bytes[1] & 0xff) & 0x80) == 0x80) ? 1 : 0)
				.orElseThrow(() -> new IEC608705104ProtocolException(getClass(),
						"Can not determine structure qualifier from " + (bytes[1] & 0xff) + "!"));
		return Arrays.copyOfRange(bytes, getEncodedSize(), bytes.length);
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		return new byte[] { (byte) typeIdentification.getId(),
				(byte) (numberOfInformationElements | (structureQualifier.getCode() << 7)) };
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return 2;
	}

}