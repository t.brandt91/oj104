/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * Information, what kind a parameter of a measured value is:
 * <ul>
 * <li>not used</li>
 * <li>threshold value</li>
 * <li>smoothing factor</li>
 * <li>low limit</li>
 * <li>high limit</li>
 * <li>in compatible range</li>
 * <li>in private range</li>
 * </ul>
 * To be used in {@link IInformationElement}s.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
@JsonIgnoreProperties({ "notUsed", "thresholdValue", "smoothingFactor", "lowLimit", "highLimit", "inCompatibleRange",
		"inPrivateRange" })
public class MeasuredValuesParameterKind implements Serializable {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = 2697769196570556554L;

	/**
	 * The kind of the parameter.
	 * <ul>
	 * <li>0 = not used</li>
	 * <li>1 = threshold value</li>
	 * <li>2 = smoothing factor</li>
	 * <li>3 = low limit</li>
	 * <li>4 = high limit</li>
	 * <li>5-31 = in compatible range</li>
	 * <li>32-63 = in private range</li>
	 * </ul>
	 */
	private int value;

	/**
	 * Returns the kind of the parameter.
	 * 
	 * @return An integer in:
	 *         <ul>
	 *         <li>0 = not used</li>
	 *         <li>1 = threshold value</li>
	 *         <li>2 = smoothing factor</li>
	 *         <li>3 = low limit</li>
	 *         <li>4 = high limit</li>
	 *         <li>5-31 = in compatible range</li>
	 *         <li>32-63 = in private range</li>
	 *         </ul>
	 */
	public int getValue() {
		return value;
	}

	/**
	 * Sets the kind of the parameter.
	 * 
	 * @param value An integer in:
	 *              <ul>
	 *              <li>0 = not used</li>
	 *              <li>1 = threshold value</li>
	 *              <li>2 = smoothing factor</li>
	 *              <li>3 = low limit</li>
	 *              <li>4 = high limit</li>
	 *              <li>5-31 = in compatible range</li>
	 *              <li>32-63 = in private range</li>
	 *              </ul>
	 */
	public void setValue(int value) {
		this.value = value;
	}

	/**
	 * Checks, whether the kind of the parameter means not used.
	 * 
	 * @return {@link #getValue()} == 0.
	 */
	public boolean isNotUsed() {
		return value == 0;
	}

	/**
	 * Checks, whether the kind of the parameter means threshold value.
	 * 
	 * @return {@link #getValue()} == 1.
	 */
	public boolean isThresholdValue() {
		return value == 1;
	}

	/**
	 * Checks, whether the kind of the parameter means smoothing factor.
	 * 
	 * @return {@link #getValue()} == 2.
	 */
	public boolean isSmoothingFactor() {
		return value == 2;
	}

	/**
	 * Checks, whether the kind of the parameter means low limit.
	 * 
	 * @return {@link #getValue()} == 3.
	 */
	public boolean isLowLimit() {
		return value == 3;
	}

	/**
	 * Checks, whether the kind of the parameter means high limit.
	 * 
	 * @return {@link #getValue()} == 4.
	 */
	public boolean isHighLimit() {
		return value == 4;
	}

	/**
	 * Checks, whether the kind of the parameter is in compatible range.
	 * 
	 * @return {@link #getValue()} >= 5 && {@link #getValue()} <= 31.
	 */
	public boolean isInCompatibleRange() {
		return value >= 5 && value <= 31;
	}

	/**
	 * Checks, whether the kind of the parameter is in private range.
	 * 
	 * @return {@link #getValue()} >= 32 && {@link #getValue()} <= 63.
	 */
	public boolean isInPrivateRange() {
		return value >= 32 && value <= 63;
	}

	/**
	 * Empty default constructor.
	 */
	public MeasuredValuesParameterKind() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param value The qualifier, an integer in:
	 *              <ul>
	 *              <li>0 = not used</li>
	 *              <li>1 = threshold value</li>
	 *              <li>2 = smoothing factor</li>
	 *              <li>3 = low limit</li>
	 *              <li>4 = high limit</li>
	 *              <li>5-31 = in compatible range</li>
	 *              <li>32-63 = in private range</li>
	 *              </ul>
	 */
	public MeasuredValuesParameterKind(int value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + value;
		return result;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		MeasuredValuesParameterKind other = (MeasuredValuesParameterKind) obj;
		if (value != other.value) {
			return false;
		}
		return true;
	}

}