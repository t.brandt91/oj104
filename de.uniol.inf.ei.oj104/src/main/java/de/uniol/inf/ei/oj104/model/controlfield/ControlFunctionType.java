/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.controlfield;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Optional;

import de.uniol.inf.ei.oj104.model.APDU;

/**
 * Enumeration for control function types.
 * <p>
 * A control function type is the content of an
 * {@link UnnumberedControlFunction} with the flag activation or confirm.
 * Possible types are {@link ControlFunctionType#STARTDT},
 * {@link ControlFunctionType#STOPDT}, and {@link ControlFunctionType#TESTFR}.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public enum ControlFunctionType implements Serializable {

	/**
	 * The STARTDT indicates the start of a data transfer. Activated by master,
	 * confirmed by slave.
	 */
	STARTDT((byte) 0x07, (byte) 0x0B),

	/**
	 * The STOPDT indicates the stop of a data transfer. Activated by master,
	 * confirmed by slave.
	 */
	STOPDT((byte) 0x13, (byte) 0x23),

	/**
	 * The TESTFR can be used to test the connection between master and slave. Can
	 * be activated by master or slave, confirmed by the other one.
	 */
	TESTFR((byte) 0x43, (byte) 0x83);

	/**
	 * The bytes, coding that the {@link APDU} marks the activation of a control
	 * function.
	 */
	private byte activate;

	/**
	 * The bytes, coding that the {@link APDU} marks the confirmation of a control
	 * function.
	 */
	private byte confirm;

	/**
	 * Constructor with fields.
	 * 
	 * @param activate The bytes, coding that the {@link APDU} marks the activation
	 *                 of a control function.
	 * @param confirm  The bytes, coding that the {@link APDU} marks the
	 *                 confirmation of a control function.
	 */
	private ControlFunctionType(byte activate, byte confirm) {
		this.activate = activate;
		this.confirm = confirm;
	}

	/**
	 * Returns the bytes, coding that the {@link APDU} marks the activation of a
	 * control function.
	 * 
	 * @return A byte.
	 */
	public int getActivate() {
		return activate;
	}

	/**
	 * Returns the bytes, coding that the {@link APDU} marks the confirmation of a
	 * control function.
	 * 
	 * @return A byte.
	 */
	public int getConfirm() {
		return confirm;
	}

	/**
	 * Retrieve an enum entry.
	 * 
	 * @param code The code of the control function type: 0x07 = STARTDT activation,
	 *             0x0B = STARTDT confirmation, 0x13 = STOPDT activation, 0x23 =
	 *             STOPDT confirmation, 0x43 = TESTFR activation, 0x83 = TESTFR
	 *             confirmation.
	 * @return An optional of the {@link ControlFunctionType} or
	 *         {@link Optional#empty()}, if there is none for the code.
	 */
	public static Optional<ControlFunctionType> getType(int code) {
		return Arrays.asList(values()).stream().filter(type -> type.getActivate() == code || type.getConfirm() == code)
				.findAny();
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return 1;
	}

}