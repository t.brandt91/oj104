/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.timetag;

import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * Time tag with information about minutes, seconds, and milliseconds.
 * <p>
 * Time tags are special {@link IInformationElement}s.
 * <p>
 * Abbreviation: CP24Time2a
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
@JsonIgnoreProperties({ "timestamp" })
public class ThreeOctetBinaryTime extends TwoOctetBinaryTime {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = -3140637220585668138L;

	/**
	 * The minutes of the time tag (>= 0 and < 60).
	 */
	private int minutes;

	/**
	 * True, if the time tag is a substitution.
	 */
	private boolean substituted;

	/**
	 * True, if the time tag is invalid.
	 */
	private boolean invalid;

	/**
	 * Returns the minutes of the time tag.
	 * 
	 * @return An integer in [0;60)
	 */
	public int getMinutes() {
		return minutes;
	}

	/**
	 * Sets the minutes of the time tag.
	 * 
	 * @param minutes An integer in [0;60)
	 */
	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	/**
	 * Checks, whether the time tag is a substitution.
	 * 
	 * @return True, if the time tag is a substitution.
	 */
	public boolean isSubstituted() {
		return substituted;
	}

	/**
	 * Sets, whether the time tag is a substitution.
	 * 
	 * @param substituted True, if the time tag is a substitution.
	 */
	public void setSubstituted(boolean substituted) {
		this.substituted = substituted;
	}

	/**
	 * Checks, whether the time tag is invalid.
	 * 
	 * @return True, if the time tag is invalid.
	 */
	public boolean isInvalid() {
		return invalid;
	}

	/**
	 * Sets, whether the time tag is invalid.
	 * 
	 * @param invalid True, if the time tag is invalid.
	 */
	public void setInvalid(boolean invalid) {
		this.invalid = invalid;
	}

	@Override
	public long getTimestamp() {
		return super.getTimestamp() + minutes * 60000;
	}

	/**
	 * Empty default constructor.
	 */
	public ThreeOctetBinaryTime() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param milliseconds The milliseconds of the time tag (>= 0 and < 1000).
	 * @param seconds      The seconds of the time tag (>= 0 and < 60).
	 * @param minutes      The minutes of the time tag (>= 0 and < 60).
	 * @param substituted  True, if the time tag is a substitution.
	 * @param invalid      True, if the time tag is invalid.
	 */
	public ThreeOctetBinaryTime(int milliseconds, int seconds, int minutes, boolean substituted, boolean invalid) {
		super(milliseconds, seconds);
		this.minutes = minutes;
		this.substituted = substituted;
		this.invalid = invalid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (invalid ? 1231 : 1237);
		result = prime * result + minutes;
		result = prime * result + (substituted ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ThreeOctetBinaryTime other = (ThreeOctetBinaryTime) obj;
		if (invalid != other.invalid) {
			return false;
		}
		if (minutes != other.minutes) {
			return false;
		}
		if (substituted != other.substituted) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		}

		byte[] remainingBytes = super.fromBytes(bytes);
		minutes = remainingBytes[0] & 0x3f;
		substituted = (remainingBytes[0] & 0x40) == 0x40;
		invalid = (remainingBytes[0] & 0x80) == 0x80;
		return Arrays.copyOfRange(bytes, getEncodedSize(), bytes.length);
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		byte[] millisecondBytes = super.toBytes();
		byte minuteByte = (byte) minutes;
		minuteByte |= substituted ? 0x40 : 0x00;
		minuteByte |= invalid ? 0x80 : 0x00;
		return ArrayUtils.add(millisecondBytes, minuteByte);
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return 3;
	}

}