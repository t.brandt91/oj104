/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model;

import org.apache.commons.lang3.ArrayUtils;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * An application protocol data unit (APDU) is the complete content of each IEC
 * 608750-5-101/104 message. It consists of an application protocol control
 * information (APCI) and an application service data unit (ASDU). The
 * {@link APCI} is the header containing the start tag, the length of the
 * {@link APDU} and information about the type of {@link APDU}. The {@link ASDU}
 * is the body containing the {@link IInformationObject}s.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class APDU implements IByteEncodedEntity {

	private static final long serialVersionUID = 3797084234678806052L;

	/**
	 * The header information. The APCI (in binary form) starts with a specific
	 * start tag, contain the length of the rest of the {@link APDU} (length minus
	 * start tag and length information), and a control field.
	 */
	private APCI apci;

	/**
	 * The body with the real payload. The {@link ASDU} contains information about
	 * the data unit and a list of information objects that contain the transmitted
	 * information, e.g. measurements. The amount of information objects is defined
	 * in the information about the data unit.
	 */
	private ASDU asdu;

	/**
	 * Returns the header information.
	 * 
	 * @return The APCI (in binary form) starts with a specific start tag, contain
	 *         the length of the rest of the {@link APDU} (length minus start tag
	 *         and length information), and a control field.
	 */
	public APCI getApci() {
		return apci;
	}

	/**
	 * Sets the header information.
	 * 
	 * @param apci The APCI (in binary form) starts with a specific start tag,
	 *             contain the length of the rest of the {@link APDU} (length minus
	 *             start tag and length information), and a control field.
	 */
	public void setApci(APCI apci) {
		this.apci = apci;
	}

	/**
	 * Returns the body with the real payload.
	 * 
	 * @return The {@link ASDU} contains information about the data unit and a list
	 *         of information objects that contain the transmitted information, e.g.
	 *         measurements. The amount of information objects is defined in the
	 *         information about the data unit.
	 */
	public ASDU getAsdu() {
		return asdu;
	}

	/**
	 * Sets the body with the real payload.
	 * 
	 * @param asdu The {@link ASDU} contains information about the data unit and a
	 *             list of information objects that contain the transmitted
	 *             information, e.g. measurements. The amount of information objects
	 *             is defined in the information about the data unit.
	 */
	public void setAsdu(ASDU asdu) {
		this.asdu = asdu;
	}

	/**
	 * Empty default constructor.
	 */
	public APDU() {
	}

	/**
	 * Constructor only with header (not all APDUs contain ASDUs)
	 * 
	 * @param apci The header information. The APCI (in binary form) starts with a
	 *             specific start tag, contain the length of the rest of the
	 *             {@link APDU} (length minus start tag and length information), and
	 *             a control field.
	 */
	public APDU(APCI apci) {
		this.apci = apci;
	}

	/**
	 * Constructor with header and payload.
	 * 
	 * @param apci The header information. The APCI (in binary form) starts with a
	 *             specific start tag, contain the length of the rest of the
	 *             {@link APDU} (length minus start tag and length information), and
	 *             a control field.
	 * @param asdu The body with the real payload. The {@link ASDU} contains
	 *             information about the data unit and a list of information objects
	 *             that contain the transmitted information, e.g. measurements. The
	 *             amount of information objects is defined in the information about
	 *             the data unit.
	 */
	public APDU(APCI apci, ASDU asdu) {
		this.apci = apci;
		this.asdu = asdu;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((apci == null) ? 0 : apci.hashCode());
		result = prime * result + ((asdu == null) ? 0 : asdu.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		APDU other = (APDU) obj;
		if (apci == null) {
			if (other.apci != null) {
				return false;
			}
		} else if (!apci.equals(other.apci)) {
			return false;
		}
		if (asdu == null) {
			if (other.asdu != null) {
				return false;
			}
		} else if (!asdu.equals(other.asdu)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		apci = new APCI();
		byte[] remainingBytes = apci.fromBytes(bytes);
		if (remainingBytes.length > 0) {
			asdu = new ASDU();
			remainingBytes = asdu.fromBytes(remainingBytes);
		}
		return remainingBytes;
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		byte[] bytes = apci.toBytes();
		if (asdu != null) {
			bytes = ArrayUtils.addAll(bytes, asdu.toBytes());
		}
		return bytes;
	}

}