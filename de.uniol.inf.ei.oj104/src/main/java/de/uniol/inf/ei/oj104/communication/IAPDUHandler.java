/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.communication;

import java.io.IOException;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.APDU;
import de.uniol.inf.ei.oj104.model.ASDU;
import de.uniol.inf.ei.oj104.model.controlfield.InformationTransfer;
import de.uniol.inf.ei.oj104.model.controlfield.UnnumberedControlFunction;

/**
 * Interface for services that handle {@link APDU}s. An {@link IAPDUHandler} is
 * the central part of the communication architecture of OJ104. The other parts
 * are an {@link ICommunicationHandler} and an {@link IASDUHandler}. An
 * {@link IAPDUHandler} has two main objectives:
 * <ol>
 * <li>Handling of incoming {@link APDU}s from an {@link ICommunicationHandler}.
 * This handling includes the handling of send and reseice sequence numbers and
 * timers as well as the sending of responses via an
 * {@link ICommunicationHandler}.</li>
 * <li>Sending of {@link UnnumberedControlFunction}s to start or stop the data
 * transfer.</li>
 * </ol>
 * Default implementation: {@link StandardAPDUHandler}.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public interface IAPDUHandler {

	/**
	 * Sets the {@link IASDUHandler}. It is required to set an {@link IASDUHandler}
	 * before messages are sent or received. Use the {@link DummyASDUHandler} if
	 * {@link ASDU}s don't need to be handled.
	 */
	public void setASDUHandler(IASDUHandler handler);

	/**
	 * Sets the {@link ICommunicationHandler}. It is required to set an
	 * {@link ICommunicationHandler} before messages are sent or received.
	 */
	public void setCommunicationHandler(ICommunicationHandler handler);

	/**
	 * This method is called from an {@link ICommunicationHandler} when an {@link APDU} is received.
	 */
	public void handleAPDU(APDU apdu) throws IEC608705104ProtocolException, IOException;

	/**
	 * Builds and sends an {@link InformationTransfer} {@link APDU} for a given {@link ASDU}.
	 */
	public void buildAndSendAPDU(ASDU asdu) throws IEC608705104ProtocolException, IOException;

	/**
	 * Builds and sends an {@link UnnumberedControlFunction} with a startDT act bit.
	 */
	public void startDataTransfer() throws IEC608705104ProtocolException, IOException;

	/**
	 * Builds and sends an {@link UnnumberedControlFunction} with a stopDT act bit.
	 */
	public void stopDataTransfer() throws IEC608705104ProtocolException, IOException;

	/**
	 * Returns true if
	 * <ol>
	 * <li>this {@link IAPDUHandler} has sent a startDT message and received a confirm, or</li>
	 * <li>this {@link IAPDUHandler} has received a startDT message and has sent a confirm.</li>
	 * </ol>
	 */
	public boolean isDataTransferStarted();

	/**
	 * Ignore the need of a started data transfer etc. before data can be transferred.
	 */
	public void ignoreHandshakes(boolean ignore);

	/**
	 * Returns true, if the need of a started data transfer etc. before data can be transferred is ignored.
	 */
	public boolean areHandshakesIgnored();

	/**
	 * Do not set an timers for timeouts.
	 */
	public void ignoreTimeouts(boolean ignore);

	/**
	 * Returns true if timers for timeouts are not used.
	 */
	public boolean areTimeoutsIgnored();

	/**
	 * Set whether responses, e.g. startDT con, shall be sent.
	 */
	public void sendResponses(boolean send);

	/**
	 * Returns true if responses, e.g. startDT con, are to be sent.
	 */
	public boolean areResponsesSent();

	/**
	 * Stops all running threads, e.g. timers.
	 */
	public void stopAllThreads();
	
	/**
	 * Resets the send, receive and acknowledge sequence number to 0.
	 */
	public void resetState();

}