/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.communication;

import java.util.Optional;

import de.uniol.inf.ei.oj104.model.ASDU;

/**
 * Dummy implementation if an {@link IASDUHandler}. An {@link IASDUHandler} is
 * part of the communication architecture of OJ104, together with an
 * {@link IAPDUHandler} and an {@link ICommunicationHandler}.<br />
 * <br />
 * An {@link IASDUHandler} has the objective to provide an {@link ASDU} as
 * response to an incoming {@link ASDU} from an {@link IAPDUHandler}. The
 * {@link DummyASDUHandler} never responds with an {@link ASDU} but with
 * {@link Optional#empty()}.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class DummyASDUHandler implements IASDUHandler {

	@Override
	public Optional<ASDU> handleASDU(ASDU asdu) {
		// no response
		return Optional.empty();
	}

}