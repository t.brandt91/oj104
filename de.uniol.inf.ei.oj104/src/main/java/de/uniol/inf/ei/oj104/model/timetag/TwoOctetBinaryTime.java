/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.timetag;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.model.ITimeTag;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * Time tag with information about seconds and milliseconds.
 * <p>
 * Time tags are special {@link IInformationElement}s.
 * <p>
 * Abbreviation: CP16Time2a
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
@JsonIgnoreProperties({ "timestamp" })
public class TwoOctetBinaryTime implements ITimeTag {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = -7275607603790280464L;

	/**
	 * The milliseconds of the time tag (>= 0 and < 1000).
	 */
	private int milliseconds;

	/**
	 * The seconds of the time tag (>= 0 and < 60).
	 */
	private int seconds;

	/**
	 * Returns the milliseconds of the time tag.
	 * @return An integer in [0;1000)
	 */
	public int getMilliseconds() {
		return milliseconds;
	}

	/**
	 * Sets the milliseconds of the time tag.
	 * @param milliseconds An integer in [0;1000)
	 */
	public void setMilliseconds(int milliseconds) {
		this.milliseconds = milliseconds;
	}

	/**
	 * Returns the seconds of the time tag.
	 * @return An integer in [0;60)
	 */
	public int getSeconds() {
		return seconds;
	}

	/**
	 * Sets the seconds of the time tag.
	 * @param seconds An integer in [0;60)
	 */
	public void setSeconds(int seconds) {
		this.seconds = seconds;
	}

	@Override
	public long getTimestamp() {
		return milliseconds + seconds * 1000;
	}

	/**
	 * Empty default constructor.
	 */
	public TwoOctetBinaryTime() {
	}

	/**
	 * Constructor with fields.
	 * @param milliseconds The milliseconds of the time tag (>= 0 and < 1000).
	 * @param seconds The seconds of the time tag (>= 0 and < 60).
	 */
	public TwoOctetBinaryTime(int milliseconds, int seconds) {
		this.milliseconds = milliseconds;
		this.seconds = seconds;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + milliseconds;
		result = prime * result + seconds;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TwoOctetBinaryTime other = (TwoOctetBinaryTime) obj;
		if (milliseconds != other.milliseconds) {
			return false;
		}
		if (seconds != other.seconds) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		}

		int timestamp = (bytes[0] & 0xff) + ((bytes[1] & 0xff) << 8);
		milliseconds = timestamp % 1000;
		seconds = timestamp / 1000;
		return Arrays.copyOfRange(bytes, getEncodedSize(), bytes.length);
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		int timestamp = milliseconds + seconds * 1000;
		return new byte[] { (byte) timestamp, (byte) (timestamp >> 8) };
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return 2;
	}

}