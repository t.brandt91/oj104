/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model;

import static com.fasterxml.jackson.annotation.JsonTypeInfo.As.PROPERTY;
import static com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import de.uniol.inf.ei.oj104.model.timetag.SevenOctetBinaryTime;
import de.uniol.inf.ei.oj104.model.timetag.ThreeOctetBinaryTime;
import de.uniol.inf.ei.oj104.model.timetag.TwoOctetBinaryTime;

/**
 * Interface for time tags that are special {@link IInformationElement}s
 * containing time informations.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
@JsonTypeInfo(use = NAME, include = PROPERTY)
@JsonSubTypes({ @JsonSubTypes.Type(value = TwoOctetBinaryTime.class, name = "TwoOctetBinaryTime"),
		@JsonSubTypes.Type(value = ThreeOctetBinaryTime.class, name = "ThreeOctetBinaryTime"),
		@JsonSubTypes.Type(value = SevenOctetBinaryTime.class, name = "SevenOctetBinaryTime") })
public interface ITimeTag extends IInformationElement {

	/**
	 * Returns the time tag as time stamp.
	 * 
	 * @return A unix time stamp.
	 */
	public long getTimestamp();

}