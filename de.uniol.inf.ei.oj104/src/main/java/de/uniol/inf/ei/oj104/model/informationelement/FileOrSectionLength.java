/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import java.util.Arrays;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * Information element that contains the length of a file or section.
 * <p>
 * Abbreviation: LOF
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class FileOrSectionLength implements IInformationElement {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = -7891819942608432599L;

	/**
	 * The length of the file or section.
	 */
	private int value;

	/**
	 * Returns the length of the file or section.
	 * 
	 * @return An integer greater than 0.
	 */
	public int getValue() {
		return value;
	}

	/**
	 * Sets the length of the file or section.
	 * 
	 * @param value An integer greater than 0.
	 */
	public void setValue(int value) {
		this.value = value;
	}

	/**
	 * Empty default constructor.
	 */
	public FileOrSectionLength() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param value The length of the file or section. An integer greater than 0.
	 */
	public FileOrSectionLength(int value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + value;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		FileOrSectionLength other = (FileOrSectionLength) obj;
		if (value != other.value) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		}

		value = bytes[0] & 0xff;
		for (int i = 1; i < getEncodedSize(); i++) {
			value |= (bytes[i] & 0xff) << (8 * i);
		}
		return Arrays.copyOfRange(bytes, getEncodedSize(), bytes.length);
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		byte[] bytes = new byte[getEncodedSize()];
		bytes[0] = (byte) value;
		for (int i = 1; i < getEncodedSize(); i++) {
			bytes[i] = (byte) (value >> (8 * i));
		}
		return bytes;
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return 3;
	}

}