/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.controlfield;

import java.util.Arrays;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.APCI;
import de.uniol.inf.ei.oj104.model.APDU;
import de.uniol.inf.ei.oj104.model.ASDU;
import de.uniol.inf.ei.oj104.model.IControlField;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * The information transfer is the control field for {@link APDU}s in I-format.
 * In I-format, {@link APDU}s can have an
 * {@link ASDU} as payload.
 * <p>
 * An information transfer control field is part of an {@link APCI} and defines
 * the following control information: send and receive sequence numbers.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class InformationTransfer implements IControlField {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = -8751768663522605249L;

	/**
	 * The send sequence number tells, how many messages the sender has sent before
	 * this message: sendSequenceNumber >= 0.
	 */
	private short sendSequenceNumber;

	/**
	 * The receive sequence number tells, how many messages the sender has received
	 * before this message: receiveSequenceNumber >= 0.
	 */
	private short receiveSequenceNumber;

	/**
	 * Returns the amount of messages the sender has sent before this message.
	 * 
	 * @return An integer greater than or equal to 0.
	 */
	public short getSendSequenceNumber() {
		return sendSequenceNumber;
	}

	/**
	 * Sets the amount of messages the sender has sent before this message.
	 * 
	 * @param sendSequenceNumber An integer greater than or equal to 0.
	 */
	public void setSendSequenceNumber(short sendSequenceNumber) {
		this.sendSequenceNumber = sendSequenceNumber;
	}

	/**
	 * Returns the amount of messages the sender has received before this message.
	 * 
	 * @return An integer greater than or equal to 0.
	 */
	public short getReceiveSequenceNumber() {
		return receiveSequenceNumber;
	}

	/**
	 * Sets the amount of messages the sender has received before this message.
	 * 
	 * @param receiveSequenceNumber An integer greater than or equal to 0.
	 */
	public void setReceiveSequenceNumber(short receiveSequenceNumber) {
		this.receiveSequenceNumber = receiveSequenceNumber;
	}

	/**
	 * Empty default constructor.
	 */
	public InformationTransfer() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param sendSequenceNumber    The send sequence number tells, how many
	 *                              messages the sender has sent before this
	 *                              message: sendSequenceNumber >= 0.
	 * @param receiveSequenceNumber The receive sequence number tells, how many
	 *                              messages the sender has received before this
	 *                              message: receiveSequenceNumber >= 0.
	 */
	public InformationTransfer(short sendSequenceNumber, short receiveSequenceNumber) {
		this.sendSequenceNumber = sendSequenceNumber;
		this.receiveSequenceNumber = receiveSequenceNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + receiveSequenceNumber;
		result = prime * result + sendSequenceNumber;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		InformationTransfer other = (InformationTransfer) obj;
		if (receiveSequenceNumber != other.receiveSequenceNumber) {
			return false;
		}
		if (sendSequenceNumber != other.sendSequenceNumber) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < IControlField.getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), IControlField.getEncodedSize(), bytes.length);
		} else if ((bytes[0] & 0x01) != 0) {
			throw new IEC608705104ProtocolException(getClass(),
					"First octet of information transfer must have 0 as LSB but is " + bytes[0] + "!");
		} else if ((bytes[2] & 0x01) != 0) {
			throw new IEC608705104ProtocolException(getClass(),
					"Third octet of information transfer must have 0 as LSB but is " + bytes[2] + "!");
		}
		sendSequenceNumber = (short) (((bytes[0] & 0xff) >> 1) | ((bytes[1] & 0xff) << 7));
		receiveSequenceNumber = (short) (((bytes[2] & 0xff) >> 1) | ((bytes[3] & 0xff) << 7));
		// & 0xff to mask the value; otherwise values above 0x07f are treated as
		// neagative values
		return Arrays.copyOfRange(bytes, IControlField.getEncodedSize(), bytes.length);
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		byte[] bytes = new byte[IControlField.getEncodedSize()];
		bytes[0] = (byte) (sendSequenceNumber << 1);
		bytes[1] = (byte) (sendSequenceNumber >> 7);
		bytes[2] = (byte) (receiveSequenceNumber << 1);
		bytes[3] = (byte) (receiveSequenceNumber >> 7);
		return bytes;
	}

}