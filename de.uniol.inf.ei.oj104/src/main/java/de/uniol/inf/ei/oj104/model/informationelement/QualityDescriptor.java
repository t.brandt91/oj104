/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import java.util.Arrays;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * Quality descriptors to be used in {@link IInformationElement}s.
 * <p>
 * Abbreviation: QDS
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class QualityDescriptor implements IQualityDescriptor {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = 3400858283243366445L;

	/**
	 * True, if the value is marked as blocked.
	 * <p>
	 * Abbreviation: BL
	 */
	private boolean blocked;

	/**
	 * True, if the value is marked as substituted.
	 * <p>
	 * Abbreviation: SB
	 */
	private boolean substituted;

	/**
	 * True, if the value is marked as not topical.
	 * <p>
	 * Abbreviation: NT
	 */
	private boolean notTopical;

	/**
	 * True, if the value is marked as invalid.
	 * <p>
	 * Abbreviation: IV
	 */
	private boolean invalid;

	@Override
	public boolean isBlocked() {
		return blocked;
	}

	@Override
	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	@Override
	public boolean isSubstituted() {
		return substituted;
	}

	@Override
	public void setSubstituted(boolean substituted) {
		this.substituted = substituted;
	}

	@Override
	public boolean isNotTopical() {
		return notTopical;
	}

	@Override
	public void setNotTopical(boolean notTopical) {
		this.notTopical = notTopical;
	}

	@Override
	public boolean isInvalid() {
		return invalid;
	}

	@Override
	public void setInvalid(boolean invalid) {
		this.invalid = invalid;
	}

	/**
	 * Empty default constructor.
	 */
	public QualityDescriptor() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param blocked     True, if the value is marked as blocked.
	 *                    <p>
	 *                    Abbreviation: BL
	 * @param substituted True, if the value is marked as substituted.
	 *                    <p>
	 *                    Abbreviation: SB
	 * @param notTopical  True, if the value is marked as not topical.
	 *                    <p>
	 *                    Abbreviation: NT
	 * @param invalid     True, if the value is marked as invalid.
	 *                    <p>
	 *                    Abbreviation: IV
	 */
	public QualityDescriptor(boolean blocked, boolean substituted, boolean notTopical, boolean invalid) {
		this.blocked = blocked;
		this.substituted = substituted;
		this.notTopical = notTopical;
		this.invalid = invalid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (blocked ? 1231 : 1237);
		result = prime * result + (substituted ? 1231 : 1237);
		result = prime * result + (notTopical ? 1231 : 1237);
		result = prime * result + (invalid ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		QualityDescriptor other = (QualityDescriptor) obj;
		if (blocked != other.blocked) {
			return false;
		}
		if (substituted != other.substituted) {
			return false;
		}
		if (notTopical != other.notTopical) {
			return false;
		}
		if (invalid != other.invalid) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		}

		int byteAsInt = bytes[0] & 0xff;

		blocked = (byteAsInt & 0x10) == 0x10;
		substituted = (byteAsInt & 0x20) == 0x20;
		notTopical = (byteAsInt & 0x40) == 0x40;
		invalid = (byteAsInt & 0x80) == 0x80;

		return Arrays.copyOfRange(bytes, getEncodedSize(), bytes.length);
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		byte b = 0;
		b |= blocked ? 0x10 : 0x00;
		b |= substituted ? 0x20 : 0x00;
		b |= notTopical ? 0x40 : 0x00;
		b |= invalid ? 0x80 : 0x00;
		return new byte[] { b };
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static final int getEncodedSize() {
		return 1;
	}

}