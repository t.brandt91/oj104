/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * File errors, to be used in {@link IInformationElement}s, can be:
 * <ul>
 * <li>default</li>
 * <li>not enough memory</li>
 * <li>checksum failed</li>
 * <li>unexpected communication service</li>
 * <li>unexpected name of file</li>
 * <li>unexpected name of section</li>
 * <li>in compatible range</li>
 * <li>in private range</li>
 * </ul>
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
@JsonIgnoreProperties({ "default", "notEnoughMemory", "checksumFailed", "unexpectedCommunicationService",
		"unexpectedNameOfFile", "unexpectedNameOfSection", "inCompatibleRange", "inPrivateRange" })
public class FileError implements Serializable {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = 111863164304989998L;

	/**
	 * The error value:
	 * <ul>
	 * <li>0 = default</li>
	 * <li>1 = not enough memory</li>
	 * <li>2 = checksum failed</li>
	 * <li>3 = unexpected communication service</li>
	 * <li>4 = unexpected name of file</li>
	 * <li>5 = unexpected name of section</li>
	 * <li>6-10 = in compatible range</li>
	 * <li>11-15 = in private range</li>
	 * </ul>
	 */
	private int value;

	/**
	 * Returns the error value.
	 * 
	 * @return An integer in:
	 *         <ul>
	 *         <li>0 = default</li>
	 *         <li>1 = not enough memory</li>
	 *         <li>2 = checksum failed</li>
	 *         <li>3 = unexpected communication service</li>
	 *         <li>4 = unexpected name of file</li>
	 *         <li>5 = unexpected name of section</li>
	 *         <li>6-10 = in compatible range</li>
	 *         <li>11-15 = in private range</li>
	 *         </ul>
	 */
	public int getValue() {
		return value;
	}

	/**
	 * Sets the error value.
	 * 
	 * @param value An integer in:
	 *              <ul>
	 *              <li>0 = default</li>
	 *              <li>1 = not enough memory</li>
	 *              <li>2 = checksum failed</li>
	 *              <li>3 = unexpected communication service</li>
	 *              <li>4 = unexpected name of file</li>
	 *              <li>5 = unexpected name of section</li>
	 *              <li>6-10 = in compatible range</li>
	 *              <li>11-15 = in private range</li>
	 *              </ul>
	 */
	public void setValue(int value) {
		this.value = value;
	}

	/**
	 * Checks, whether the file error is "default".
	 * 
	 * @return {@link #getValue()} == 0
	 */
	public boolean isDefault() {
		return value == 0;
	}

	/**
	 * Checks, whether the file error is "not enough memory".
	 * 
	 * @return {@link #getValue()} == 1
	 */
	public boolean isNotEnoughMemory() {
		return value == 1;
	}

	/**
	 * Checks, whether the file error is "checksum failed".
	 * 
	 * @return {@link #getValue()} == 2
	 */
	public boolean isChecksumFailed() {
		return value == 2;
	}

	/**
	 * Checks, whether the file error is "unexpected communication service".
	 * 
	 * @return {@link #getValue()} == 3
	 */
	public boolean isUnexpectedCommunicationService() {
		return value == 3;
	}

	/**
	 * Checks, whether the file error is "unexpected name of file".
	 * 
	 * @return {@link #getValue()} == 4
	 */
	public boolean isUnexpectedNameOfFile() {
		return value == 4;
	}

	/**
	 * Checks, whether the file error is "unexpected name of section".
	 * 
	 * @return {@link #getValue()} == 5
	 */
	public boolean isUnexpectedNameOfSection() {
		return value == 5;
	}

	/**
	 * Checks, whether the file error is in compatible range.
	 * 
	 * @return {@link #getValue()} >= 6 && {@link #getValue()} <= 10
	 */
	public boolean isInCompatibleRange() {
		return value >= 6 && value <= 10;
	}

	/**
	 * Checks, whether the file error is in private range.
	 * 
	 * @return {@link #getValue()} >= 11 && {@link #getValue()} <= 15
	 */
	public boolean isInPrivateRange() {
		return value >= 11 && value <= 15;
	}

	/**
	 * Empty default constructor.
	 */
	public FileError() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param value The error value:
	 *              <ul>
	 *              <li>0 = default</li>
	 *              <li>1 = not enough memory</li>
	 *              <li>2 = checksum failed</li>
	 *              <li>3 = unexpected communication service</li>
	 *              <li>4 = unexpected name of file</li>
	 *              <li>5 = unexpected name of section</li>
	 *              <li>6-10 = in compatible range</li>
	 *              <li>11-15 = in private range</li>
	 *              </ul>
	 */
	public FileError(int value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + value;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		FileError other = (FileError) obj;
		if (value != other.value) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

}