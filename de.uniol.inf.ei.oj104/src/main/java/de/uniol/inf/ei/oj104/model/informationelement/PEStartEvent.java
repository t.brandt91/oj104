/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import java.util.Arrays;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

//TODO javaDoc
//TODO Tests
//SPE
/**
 * Information element that contains information about a protection equipment
 * start event.
 * <p>
 * Abbreviation: SPE
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class PEStartEvent extends PEInformation implements IInformationElement {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = 1535005028868671902L;

	/**
	 * True for earth current.
	 * <p>
	 * Abbreviation: SIE
	 */
	private boolean earthCurrent;

	/**
	 * True for a start in reverse direction.
	 * <p>
	 * Abbreviation: SRD
	 */
	private boolean startInReverseDirection;

	/**
	 * Checks, whether earth current is present.
	 * <p>
	 * Abbreviation: SIE
	 * 
	 * @return True for earth current.
	 */
	public boolean isEarthCurrent() {
		return earthCurrent;
	}

	/**
	 * Sets, whether earth current is present.
	 * <p>
	 * Abbreviation: SIE
	 * 
	 * @param earthCurrent True for earth current.
	 */
	public void setEarthCurrent(boolean earthCurrent) {
		this.earthCurrent = earthCurrent;
	}

	/**
	 * Checks, whether earth start is in reverse direction.
	 * <p>
	 * Abbreviation: SRD
	 * 
	 * @return True for a start in reverse direction.
	 */
	public boolean isStartInReverseDirection() {
		return startInReverseDirection;
	}

	/**
	 * Sets, whether earth start is in reverse direction.
	 * <p>
	 * Abbreviation: SRD
	 * 
	 * @param startInReverseDirection True for a start in reverse direction.
	 */
	public void setStartInReverseDirection(boolean startInReverseDirection) {
		this.startInReverseDirection = startInReverseDirection;
	}

	/**
	 * Empty default constructor.
	 */
	public PEStartEvent() {
		super();
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param general                 True for general protection.
	 * @param phaseL1                 True for protection of phase L1.
	 * @param phaseL2                 True for protection of phase L2.
	 * @param phaseL3                 True for protection of phase L3.
	 * @param earthCurrent            True for earth current.
	 * @param startInReverseDirection True for a start in reverse direction.
	 */
	public PEStartEvent(boolean general, boolean phaseL1, boolean phaseL2, boolean phaseL3, boolean earthCurrent,
			boolean startInReverseDirection) {
		super(general, phaseL1, phaseL2, phaseL3);
		this.earthCurrent = earthCurrent;
		this.startInReverseDirection = startInReverseDirection;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (earthCurrent ? 1231 : 1237);
		result = prime * result + (startInReverseDirection ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PEStartEvent other = (PEStartEvent) obj;
		if (earthCurrent != other.earthCurrent) {
			return false;
		}
		if (startInReverseDirection != other.startInReverseDirection) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		super.fromBytes(bytes);

		int byteAsInt = bytes[0] & 0xff;

		earthCurrent = (byteAsInt & 0x10) == 0x10;
		startInReverseDirection = (byteAsInt & 0x20) == 0x20;

		return Arrays.copyOfRange(bytes, getEncodedSize(), bytes.length);
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		byte[] bytes = super.toBytes();
		bytes[0] |= earthCurrent ? 0x10 : 0x00;
		bytes[0] |= startInReverseDirection ? 0x20 : 0x00;
		return bytes;
	}

}