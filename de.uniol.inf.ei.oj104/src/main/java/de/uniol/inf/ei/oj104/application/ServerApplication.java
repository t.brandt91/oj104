/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.application;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Optional;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uniol.inf.ei.oj104.communication.DummyASDUHandler;
import de.uniol.inf.ei.oj104.communication.IAPDUHandler;
import de.uniol.inf.ei.oj104.communication.IASDUHandler;
import de.uniol.inf.ei.oj104.communication.ICommunicationHandler;
import de.uniol.inf.ei.oj104.communication.StandardAPDUHandler;
import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.APDU;
import de.uniol.inf.ei.oj104.model.ASDU;
import de.uniol.inf.ei.oj104.model.controlfield.UnnumberedControlFunction;
import de.uniol.inf.ei.oj104.util.RandomASDUCreator;
import de.uniol.inf.ei.oj104.util.SystemProperties;

/**
 * Server station (controlled station) as an {@link ICommunicationHandler} that
 * handles the sending and receiving of 104 messages. An
 * {@link ICommunicationHandler} is part of the communication architecure of
 * OJ104. The other parts are an {@link IAPDUHandler} and an
 * {@link IASDUHandler}. The main objective of a server
 * {@link ICommunicationHandler} is to send 104 messages and transfer received
 * ones to an {@link IAPDUHandler}.
 *
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class ServerApplication implements ICommunicationHandler {

	/**
	 * The logger instance for this class.
	 */
	private static final Logger logger = LoggerFactory.getLogger(ServerApplication.class);

	/**
	 * The default port to use.
	 */
	private static final int defaultPort = Integer
			.parseInt(SystemProperties.getProperties().getProperty("port.default", "2404"));

	/**
	 * Starts a TCP server listening on a port (args[0] or default port defined in
	 * {@link SystemProperties}).
	 */
	public static void main(String args[]) throws Exception {
		int port = defaultPort;
		if (args != null && args.length > 0) {
			try {
				port = Integer.parseInt(args[0]);
			} catch (NumberFormatException e) {
				logger.error("Fist argument of ServerApplication must be a port number!", e);
				return;
			}
		}
		new ServerApplication().start(port);
	}

	/**
	 * An {@link IASDUHandler} has the objective to provide an {@link ASDU} as
	 * response to an incoming {@link ASDU} from an {@link IAPDUHandler}. It is also
	 * possible to not respond with an {@link ASDU} ({@link Optional#empty()}).
	 */
	private IASDUHandler asduHandler = new DummyASDUHandler();

	/**
	 * An {@link IAPDUHandler} has two main objectives:
	 * <ol>
	 * <li>Handling of incoming {@link APDU}s from an {@link ICommunicationHandler}.
	 * This handling includes the handling of send and reseice sequence numbers and
	 * timers as well as the sending of responses via an
	 * {@link ICommunicationHandler}.</li>
	 * <li>Sending of {@link UnnumberedControlFunction}s to start or stop the data
	 * transfer.</li>
	 * </ol>
	 */
	private IAPDUHandler apduHandler = new StandardAPDUHandler();

	/**
	 * Thread to listen to user commands in order to send random {@link ASDUs}.
	 * Calls the {@link IAPDUHandler} to send the {@link ASDU}.
	 */
	private APDUWriter apduWriter = new APDUWriter(new BufferedReader(new InputStreamReader(System.in)));

	/**
	 * The used client socket.
	 */
	private Socket socket;

	/**
	 * Checks, whether the communication if finished.
	 * 
	 * @return True, if {@link #socket} is closed.
	 */
	private boolean isFinished() {
		synchronized (socket) {
			return socket.isClosed();
		}
	}

	/**
	 * Starts a server connection and the {@link #apduWriter}.
	 * 
	 * @param port The port to listen on.
	 */
	private void start(int port) {
		apduHandler.setASDUHandler(asduHandler);
		apduHandler.setCommunicationHandler(this);
		((StandardAPDUHandler) apduHandler).setLogger(logger);

		try (ServerSocket welcomeSocket = new ServerSocket(port)) {
			logger.info("Server started, listening on port {}", port);

			socket = welcomeSocket.accept();
			logger.info("Client connected from ip address {}", socket.getInetAddress());

			receive(); // receive startDT
			apduWriter.start();

			while (!isFinished()) {
				receive();
			}

			apduWriter.interrupt();
		} catch (IOException | IEC608705104ProtocolException e) {
			closeConnection();
		}
		logger.info("Server stopped");
	}

	@Override
	public void send(APDU apdu) throws IOException, IEC608705104ProtocolException {
		if (isFinished()) {
			throw new IOException("Socket already closed!");
		}
		byte[] bytes = apdu.toBytes();
		synchronized (socket) {
			DataOutputStream dOut = new DataOutputStream(socket.getOutputStream());
			dOut.write(bytes);
		}
		logger.debug("Sent (APDU): {}", apdu);
		logger.trace("Sent (bytes): {}", bytes);
	}

	@Override
	public void closeConnection() {
		apduHandler.stopAllThreads();
		try {
			synchronized (socket) {
				if (socket != null) {
					socket.close();
				}
			}
		} catch (IOException e) {
			logger.error("Error while closimng socket!", e);
		}
	}

	/**
	 * Waiting for and handling of incoming messages on the web socket. Calling
	 * {@link APDU#fromBytes(byte[])} and {@link IAPDUHandler#handleAPDU(APDU)}.
	 */
	public void receive() throws IOException, IEC608705104ProtocolException {
		DataInputStream in;

		synchronized (socket) {
			in = new DataInputStream(socket.getInputStream());
		}

		byte[] header = new byte[2];
		int headerLength = in.read(header, 0, 2);
		if (headerLength != 2) {
			return;
		}

		int restLength = header[1] & 0xff;
		byte[] rest = new byte[restLength];
		int length = in.read(rest, 0, restLength);
		if (restLength != length) {
			throw new IEC608705104ProtocolException(
					"Received " + (length + 2) + " bytes but expected " + (restLength + 2) + " bytes!");
		}

		byte[] bytes = ArrayUtils.addAll(header, rest);
		logger.trace("Received (bytes): {}", bytes);

		APDU apdu = new APDU();
		apdu.fromBytes(bytes);
		logger.debug("Received (APDU): {}", apdu);

		apduHandler.handleAPDU(apdu);
	}

	/**
	 * Thread to listen to user commands in order to send random {@link ASDUs}.
	 * Calls the {@link IAPDUHandler} to send the {@link ASDU}.
	 * 
	 * @author Michael Brand (michael.brand@uol.de)
	 *
	 */
	private class APDUWriter extends Thread {

		/**
		 * Shall the thread end?
		 */
		private boolean interrupted = false;

		/**
		 * Reader for the user commands.
		 */
		private BufferedReader in;

		/**
		 * Utility object to create random but valid {@link ASDU}s.
		 */
		private RandomASDUCreator asduCreator = new RandomASDUCreator();

		/**
		 * Creates a new writer thread.
		 * 
		 * @param in Reader for the user commands.
		 */
		public APDUWriter(BufferedReader in) {
			super("APDU writer");
			this.in = in;
		}

		@Override
		public void interrupt() {
			interrupted = true;
			super.interrupt();
		}

		@Override
		public void run() {
			while (!isFinished() && !interrupted) {
				try {
					if (apduHandler.isDataTransferStarted()) {
						System.out.print("Type in ASDU type id to send or 'stop' to stop: ");
						String input = in.readLine();
						if (input != null && input.toLowerCase().equals("stop")) {
							break;
						}
						int asduTypeId = Integer.parseInt(input);
						ASDU asdu = asduCreator.createASDUs(asduTypeId).get(0);
						apduHandler.buildAndSendAPDU(asdu);
					}
				} catch (IOException | IEC608705104ProtocolException e) {
					logger.error("Error while reading from socket!", e);
				} catch (NumberFormatException e) {
					logger.error("User input is not an ASDU number!", e);
				}
			}
			closeConnection();
		}
	}
}