/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.ArrayUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.util.ASDUFactory;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * ASDU stands for Application Service Data Unit. It is, so to say, the body of
 * an {@link APDU}. Every {@link ASDU} contains information about the data unit
 * and a list of information objects that contain the transmitted information,
 * e.g. measurements. The amount of information objects is defined in the
 * information about the data unit.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
@JsonIgnoreProperties({ "encodedSize" })
public class ASDU implements IByteEncodedEntity {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = 6942438556311568044L;

	/**
	 * The data unit identifier contains the following information about the ASDU:
	 * <ul>
	 * <li>The data unit type that contains the information about the
	 * {@link ASDUType}, the {@link StructureQualifier}, and the number of
	 * information elements in the {@link ASDU}.</li>
	 * <li>The cause of transmission that directs the ASDU to a specific application
	 * task (program) for processing.</li>
	 * <li>The confirm that indicates a positive or negative confirmation of
	 * activation requested by the primary application function.</li>
	 * <li>A boolean that indicated whether the ASDU was created during a test.</li>
	 * <li>The originator address of the ASDU.</li>
	 * <li>The station address of the ASDU.</li>
	 * </ul>
	 */
	private DataUnitIdentifier dataUnitIdentifier;

	/**
	 * The list of contained information objects. Every information object has an
	 * information object address (IOA).
	 * <p>
	 * There exist two implementations of information objects, namely
	 * {@link SingleInformationObject} and {@link SequenceInformationObject}.
	 */
	private List<IInformationObject> informationObjects;

	/**
	 * Returns the data unit identifier containing information about the data unit.
	 * 
	 * @return The data unit identifier contains the following information about the
	 *         ASDU:
	 *         <ul>
	 *         <li>The data unit type that contains the information about the
	 *         {@link ASDUType}, the {@link StructureQualifier}, and the number of
	 *         information elements in the {@link ASDU}.</li>
	 *         <li>The cause of transmission that directs the ASDU to a specific
	 *         application task (program) for processing.</li>
	 *         <li>The confirm that indicates a positive or negative confirmation of
	 *         activation requested by the primary application function.</li>
	 *         <li>A boolean that indicated whether the ASDU was created during a
	 *         test.</li>
	 *         <li>The originator address of the ASDU.</li>
	 *         <li>The station address of the ASDU.</li>
	 *         </ul>
	 */
	public DataUnitIdentifier getDataUnitIdentifier() {
		return dataUnitIdentifier;
	}

	/**
	 * Sets the data unit identifier containing information about the data unit.
	 * 
	 * @param dataUnitIdentifier The data unit identifier contains the following
	 *                           information about the ASDU:
	 *                           <ul>
	 *                           <li>The data unit type that contains the
	 *                           information about the {@link ASDUType}, the
	 *                           {@link StructureQualifier}, and the number of
	 *                           information elements in the {@link ASDU}.</li>
	 *                           <li>The cause of transmission that directs the ASDU
	 *                           to a specific application task (program) for
	 *                           processing.</li>
	 *                           <li>The confirm that indicates a positive or
	 *                           negative confirmation of activation requested by
	 *                           the primary application function.</li>
	 *                           <li>A boolean that indicated whether the ASDU was
	 *                           created during a test.</li>
	 *                           <li>The originator address of the ASDU.</li>
	 *                           <li>The station address of the ASDU.</li>
	 *                           </ul>
	 */
	public void setDataUnitIdentifier(DataUnitIdentifier dataUnitIdentifier) {
		this.dataUnitIdentifier = dataUnitIdentifier;
	}

	/**
	 * Returns the list of contained information objects.
	 * 
	 * @return A list of contained information objects. Every information object has
	 *         an information object address (IOA).
	 *         <p>
	 *         There exist two implementations of information objects, namely
	 *         {@link SingleInformationObject} and
	 *         {@link SequenceInformationObject}.
	 */
	public List<IInformationObject> getInformationObjects() {
		return informationObjects;
	}

	/**
	 * Sets the list of contained information objects.
	 * 
	 * @param informationObjects A list of contained information objects. Every
	 *                           information object has an information object
	 *                           address (IOA).
	 *                           <p>
	 *                           There exist two implementations of information
	 *                           objects, namely {@link SingleInformationObject} and
	 *                           {@link SequenceInformationObject}.
	 */
	public void setInformationObjects(List<IInformationObject> informationObjects) {
		this.informationObjects = informationObjects;
	}

	/**
	 * Empty default constructor.
	 */
	public ASDU() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param dataUnitIdentifier The data unit identifier contains the following
	 *                           information about the ASDU:
	 *                           <ul>
	 *                           <li>The data unit type that contains the
	 *                           information about the {@link ASDUType}, the
	 *                           {@link StructureQualifier}, and the number of
	 *                           information elements in the {@link ASDU}.</li>
	 *                           <li>The cause of transmission that directs the ASDU
	 *                           to a specific application task (program) for
	 *                           processing.</li>
	 *                           <li>The confirm that indicates a positive or
	 *                           negative confirmation of activation requested by
	 *                           the primary application function.</li>
	 *                           <li>A boolean that indicated whether the ASDU was
	 *                           created during a test.</li>
	 *                           <li>The originator address of the ASDU.</li>
	 *                           <li>The station address of the ASDU.</li>
	 * @param informationObjects The list of contained information objects. Every
	 *                           information object has an information object
	 *                           address (IOA).
	 *                           <p>
	 *                           There exist two implementations of information
	 *                           objects, namely {@link SingleInformationObject} and
	 *                           {@link SequenceInformationObject}.
	 */
	public ASDU(DataUnitIdentifier dataUnitIdentifier, List<IInformationObject> informationObjects) {
		this.dataUnitIdentifier = dataUnitIdentifier;
		this.informationObjects = informationObjects;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataUnitIdentifier == null) ? 0 : dataUnitIdentifier.hashCode());
		result = prime * result + ((informationObjects == null) ? 0 : informationObjects.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ASDU other = (ASDU) obj;
		if (dataUnitIdentifier == null) {
			if (other.dataUnitIdentifier != null) {
				return false;
			}
		} else if (!dataUnitIdentifier.equals(other.dataUnitIdentifier)) {
			return false;
		}
		if (informationObjects == null) {
			if (other.informationObjects != null) {
				return false;
			}
		} else if (!informationObjects.equals(other.informationObjects)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < DataUnitIdentifier.getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), DataUnitIdentifier.getEncodedSize(), bytes.length);
		}

		dataUnitIdentifier = new DataUnitIdentifier();
		byte[] remainingBytes = dataUnitIdentifier.fromBytes(bytes);

		int asduTypeId = dataUnitIdentifier.getDataUnitType().getTypeIdentification().getId();
		StructureQualifier sq = dataUnitIdentifier.getDataUnitType().getStructureQualifier();
		int numObjects = sq.getNumberOfInformationObjects(dataUnitIdentifier.getDataUnitType());
		try {
			Constructor<? extends IInformationObject> ioConstructor = sq.getInformationObjectConstructor();

			List<Class<? extends IInformationElement>> ieClasses = ASDUFactory.getInformationElementClasses(asduTypeId);
			Optional<Class<? extends ITimeTag>> ttClass = ASDUFactory.getTimeTagClass(asduTypeId);
			int numElements = dataUnitIdentifier.getDataUnitType().getNumberOfInformationElements();
			informationObjects = new ArrayList<>();
			for (int i = 0; i < numObjects; i++) {
				IInformationObject informationObject = null;
				switch (sq) {
				case SINGLE:
					informationObject = ioConstructor.newInstance(ieClasses, ttClass.orElseGet(() -> null));
					break;
				case SEQUENCE:
					informationObject = ioConstructor.newInstance(ieClasses, ttClass.orElseGet(() -> null),
							numElements);
					break;
				}
				remainingBytes = informationObject.fromBytes(remainingBytes);
				informationObjects.add(informationObject);
			}
		} catch (NoSuchMethodException | InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw new IEC608705104ProtocolException(sq.getInformationObjectClass(), "Can not instantiate!", e);
		}

		return remainingBytes;
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		byte[] bytes = dataUnitIdentifier.toBytes();
		for (IInformationObject object : informationObjects) {
			bytes = ArrayUtils.addAll(bytes, object.toBytes());
		}
		return bytes;
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public int getEncodedSize() throws IEC608705104ProtocolException {
		return toBytes().length;
	}

}