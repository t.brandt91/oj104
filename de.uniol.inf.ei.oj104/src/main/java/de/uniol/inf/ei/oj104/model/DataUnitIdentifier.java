/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model;

import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.util.CauseOfTransmissionFactory;
import de.uniol.inf.ei.oj104.util.JsonParser;
import de.uniol.inf.ei.oj104.util.SystemProperties;

/**
 * The data unit identifier contains the following information about the ASDU:
 * <ul>
 * <li>The data unit type that contains the information about the
 * {@link ASDUType}, the {@link StructureQualifier}, and the number of
 * information elements in the {@link ASDU}.</li>
 * <li>The cause of transmission that directs the ASDU to a specific application
 * task (program) for processing.</li>
 * <li>The confirm that indicates a positive or negative confirmation of
 * activation requested by the primary application function.</li>
 * <li>A boolean that indicated whether the ASDU was created during a test.</li>
 * <li>The originator address of the ASDU.</li>
 * <li>The station address of the ASDU.</li>
 * </ul>
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class DataUnitIdentifier implements IByteEncodedEntity {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = -315294490679979244L;

	/**
	 * The number of bytes used for cause of transmission. 1 or 2, defined in system
	 * properties.
	 */
	private static final int cotEncodedSize = Integer
			.parseInt(SystemProperties.getProperties().getProperty("cot.bytesize", "1"));

	/**
	 * The data unit type that contains the information about the {@link ASDUType},
	 * the {@link StructureQualifier}, and the number of information elements in the
	 * {@link ASDU}.
	 */
	private DataUnitType dataUnitType;

	/**
	 * The cause of transmission that directs the ASDU to a specific application
	 * task (program) for processing.
	 * <p>
	 * It has an id and a description. The ids are defined in the standard. The ids
	 * range from 0 to 63. The description is not necessary to be machine readable
	 * but may be useful for humans.
	 */
	private CauseOfTransmission causeOfTransmission;

	/**
	 * The confirm that indicates a positive or negative confirmation of activation
	 * requested by the primary application function.
	 */
	private Confirm confirm;

	/**
	 * A boolean that indicated whether the ASDU was created during a test.
	 */
	private boolean test;

	/**
	 * The originator address of the ASDU. 0 = not used/default.
	 */
	private int originatorAddress;

	/**
	 * The station address of the ASDU.
	 * <p>
	 * 0 is defined as not used. <br />
	 * 255 for addresses with 1 byte or 655535 for addresses with 2 bytes is defined
	 * as global address. <br />
	 * All other addresses between not used and global addresses are station
	 * addresses.
	 */
	private ASDUAddress commonAddressOfASDU;

	/**
	 * Returns the data unit type that contains the information about the
	 * {@link ASDUType}, the {@link StructureQualifier}, and the number of
	 * information elements in the {@link ASDU}.
	 * 
	 * @return The data unit type that contains the information about the
	 *         {@link ASDUType}, the {@link StructureQualifier}, and the number of
	 *         information elements in the {@link ASDU}.
	 */
	public DataUnitType getDataUnitType() {
		return dataUnitType;
	}

	/**
	 * Sets the data unit type that contains the information about the
	 * {@link ASDUType}, the {@link StructureQualifier}, and the number of
	 * information elements in the {@link ASDU}.
	 * 
	 * @param dataUnitType The data unit type that contains the information about
	 *                     the {@link ASDUType}, the {@link StructureQualifier}, and
	 *                     the number of information elements in the {@link ASDU}.
	 */
	public void setDataUnitType(DataUnitType dataUnitType) {
		this.dataUnitType = dataUnitType;
	}

	/**
	 * Returns the cause of transmission that directs the ASDU to a specific
	 * application task (program) for processing.
	 * 
	 * @return The cause of transmission has an id and a description. The ids are
	 *         defined in the standard. The ids range from 0 to 63. The description
	 *         is not necessary to be machine readable but may be useful for humans.
	 */
	public CauseOfTransmission getCauseOfTransmission() {
		return causeOfTransmission;
	}

	/**
	 * Sets the cause of transmission that directs the ASDU to a specific
	 * application task (program) for processing.
	 * 
	 * @param causeOfTransmission The cause of transmission has an id and a
	 *                            description. The ids are defined in the standard.
	 *                            The ids range from 0 to 63. The description is not
	 *                            necessary to be machine readable but may be useful
	 *                            for humans.
	 */
	public void setCauseOfTransmission(CauseOfTransmission causeOfTransmission) {
		this.causeOfTransmission = causeOfTransmission;
	}

	/**
	 * Returns the confirm that indicates a positive or negative confirmation of
	 * activation requested by the primary application function.
	 * 
	 * @return The confirm that indicates a positive or negative confirmation of
	 *         activation requested by the primary application function.
	 */
	public Confirm getConfirm() {
		return confirm;
	}

	/**
	 * Sets the confirm that indicates a positive or negative confirmation of
	 * activation requested by the primary application function.
	 * 
	 * @param confirm The confirm that indicates a positive or negative confirmation
	 *                of activation requested by the primary application function.
	 */
	public void setConfirm(Confirm confirm) {
		this.confirm = confirm;
	}

	/**
	 * Returns whether the ASDU was created during a test.
	 * 
	 * @return A boolean that indicated whether the ASDU was created during a test.
	 */
	public boolean isTest() {
		return test;
	}

	/**
	 * Sets whether the ASDU was created during a test.
	 * 
	 * @param test A boolean that indicated whether the ASDU was created during a
	 *             test.
	 */
	public void setTest(boolean test) {
		this.test = test;
	}

	/**
	 * Returns the originator address of the ASDU.
	 * 
	 * @return An integer in [0, 255]. 0 = not used/default.
	 */
	public int getOriginatorAddress() {
		return originatorAddress;
	}

	/**
	 * Sets the originator address of the ASDU.
	 * 
	 * @param originatorAddress An integer in [0, 255]. 0 = not used/default.
	 */
	public void setOriginatorAddress(int originatorAddress) {
		this.originatorAddress = originatorAddress;
	}

	/**
	 * Returns the station address of the ASDU.
	 * 
	 * @return 0 is defined as not used. <br />
	 *         255 for addresses with 1 byte or 655535 for addresses with 2 bytes is
	 *         defined as global address. <br />
	 *         All other addresses between not used and global addresses are station
	 *         addresses.
	 */
	public ASDUAddress getCommonAddressOfASDU() {
		return commonAddressOfASDU;
	}

	/**
	 * Sets the station address of the ASDU.
	 * 
	 * @param commonAddressOfASDU 0 is defined as not used. <br />
	 *                            255 for addresses with 1 byte or 655535 for
	 *                            addresses with 2 bytes is defined as global
	 *                            address. <br />
	 *                            All other addresses between not used and global
	 *                            addresses are station addresses.
	 */
	public void setCommonAddressOfASDU(ASDUAddress commonAddressOfASDU) {
		this.commonAddressOfASDU = commonAddressOfASDU;
	}

	/**
	 * Empty default constructor.
	 */
	public DataUnitIdentifier() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param dataUnitType        The data unit type that contains the information
	 *                            about the {@link ASDUType}, the
	 *                            {@link StructureQualifier}, and the number of
	 *                            information elements in the {@link ASDU}.
	 * @param causeOfTransmission The cause of transmission has an id and a
	 *                            description. The ids are defined in the standard.
	 *                            The ids range from 0 to 63. The description is not
	 *                            necessary to be machine readable but may be useful
	 *                            for humans.
	 * @param confirm             The confirm that indicates a positive or negative
	 *                            confirmation of activation requested by the
	 *                            primary application function.
	 * @param test                A boolean that indicated whether the ASDU was
	 *                            created during a test.
	 * @param originatorAddress   he originator address of the ASDU. 0 = not
	 *                            used/default.
	 * @param commonAddressOfASDU The station address of the ASDU.
	 *                            <p>
	 *                            0 is defined as not used. <br />
	 *                            255 for addresses with 1 byte or 655535 for
	 *                            addresses with 2 bytes is defined as global
	 *                            address. <br />
	 *                            All other addresses between not used and global
	 *                            addresses are station addresses.
	 */
	public DataUnitIdentifier(DataUnitType dataUnitType, CauseOfTransmission causeOfTransmission, Confirm confirm,
			boolean test, int originatorAddress, ASDUAddress commonAddressOfASDU) {
		this.dataUnitType = dataUnitType;
		this.causeOfTransmission = causeOfTransmission;
		this.confirm = confirm;
		this.test = test;
		this.originatorAddress = originatorAddress;
		this.commonAddressOfASDU = commonAddressOfASDU;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((causeOfTransmission == null) ? 0 : causeOfTransmission.hashCode());
		result = prime * result + ((commonAddressOfASDU == null) ? 0 : commonAddressOfASDU.hashCode());
		result = prime * result + ((confirm == null) ? 0 : confirm.hashCode());
		result = prime * result + ((dataUnitType == null) ? 0 : dataUnitType.hashCode());
		result = prime * result + originatorAddress;
		result = prime * result + (test ? 1231 : 1237);
		return result;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataUnitIdentifier other = (DataUnitIdentifier) obj;
		if (causeOfTransmission == null) {
			if (other.causeOfTransmission != null)
				return false;
		} else if (!causeOfTransmission.equals(other.causeOfTransmission))
			return false;
		if (commonAddressOfASDU == null) {
			if (other.commonAddressOfASDU != null)
				return false;
		} else if (!commonAddressOfASDU.equals(other.commonAddressOfASDU))
			return false;
		if (confirm != other.confirm)
			return false;
		if (dataUnitType == null) {
			if (other.dataUnitType != null)
				return false;
		} else if (!dataUnitType.equals(other.dataUnitType))
			return false;
		if (originatorAddress != other.originatorAddress)
			return false;
		if (test != other.test)
			return false;
		return true;
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		}

		dataUnitType = new DataUnitType();
		byte[] remainingBytes = dataUnitType.fromBytes(bytes);
		causeOfTransmission = CauseOfTransmissionFactory.getCauseOfTransmission((remainingBytes[0] & 0xff) & 0x3F)
				.orElseThrow(() -> new IEC608705104ProtocolException(getClass(), "Can not determine CoT!"));
		confirm = Confirm.getConfirm((((remainingBytes[0] & 0xff) & 0x40) == 0x40) ? 1 : 0).orElseThrow(
				() -> new IEC608705104ProtocolException(getClass(), "Can not determine positive/negative confirm!"));
		test = ((remainingBytes[0] & 0xff) & 0x80) == 0x80;
		if (cotEncodedSize == 2) {
			originatorAddress = remainingBytes[1] & 0xff;
		} else {
			originatorAddress = -1;
		}
		commonAddressOfASDU = new ASDUAddress();
		remainingBytes = commonAddressOfASDU
				.fromBytes(Arrays.copyOfRange(remainingBytes, cotEncodedSize, remainingBytes.length));
		return remainingBytes;
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		byte[] dataUnitTypeBytes = dataUnitType.toBytes();
		byte[] cotBytes = new byte[cotEncodedSize];
		cotBytes[0] = (byte) ((byte) causeOfTransmission.getId() | (byte) (confirm.getCode() << 6)
				| (test ? 0x80 : 0x00));
		if (cotEncodedSize == 2) {
			cotBytes[1] = (byte) originatorAddress;
		} else {
			cotBytes[1] = -1;
		}
		byte[] asduAddressBytes = commonAddressOfASDU.toBytes();
		return ArrayUtils.addAll(ArrayUtils.addAll(dataUnitTypeBytes, cotBytes), asduAddressBytes);
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return DataUnitType.getEncodedSize() + cotEncodedSize + ASDUAddress.getEncodedSize();
	}

}