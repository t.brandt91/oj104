/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * Information element that contains a set point with two possible values: on
 * and off.
 * <p>
 * Abbreviation: SPI
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class SinglePointInformation implements IInformationElement {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = 636929233851799808L;

	/**
	 * True if the set point is "on"; false for "off.
	 * <p>
	 * Abbreviation: SPI
	 */
	private boolean on;

	/**
	 * The quality descriptor for the set point. Can be blocked, substituted, not
	 * topical, or invalid.
	 */
	private QualityDescriptor qualityDescriptor = new QualityDescriptor();

	/**
	 * Checks, whether set point is "on".
	 * <p>
	 * Abbreviation: SPI
	 * 
	 * @return True if the set point is "on"; false for "off.
	 */
	public boolean isOn() {
		return on;
	}

	/**
	 * Sets, whether set point is "on".
	 * <p>
	 * Abbreviation: SPI
	 * 
	 * @param on True if the set point is "on"; false for "off.
	 */
	public void setOn(boolean on) {
		this.on = on;
	}

	/**
	 * Checks, whether the value is blocked.
	 * <p>
	 * Abbreviation: BL
	 * 
	 * @return True, if the value is marked as blocked.
	 */
	public boolean isBlocked() {
		return qualityDescriptor.isBlocked();
	}

	/**
	 * Sets, whether the value is blocked.
	 * <p>
	 * Abbreviation: BL
	 * 
	 * @param blocked True, if the value shall be marked as blocked.
	 */
	public void setBlocked(boolean blocked) {
		qualityDescriptor.setBlocked(blocked);
	}

	/**
	 * Checks, whether the value is substituted.
	 * <p>
	 * Abbreviation: SB
	 * 
	 * @return True, if the value is marked as substituted.
	 */
	public boolean isSubstituted() {
		return qualityDescriptor.isSubstituted();
	}

	/**
	 * Sets, whether the value is substituted.
	 * <p>
	 * Abbreviation: SB
	 * 
	 * @param substituted True, if the value shall be marked as substituted.
	 */
	public void setSubstituted(boolean substituted) {
		qualityDescriptor.setSubstituted(substituted);
	}

	/**
	 * Checks, whether the value is not topical.
	 * <p>
	 * Abbreviation: NT
	 * 
	 * @return True, if the value is marked as not topical.
	 */
	public boolean isNotTopical() {
		return qualityDescriptor.isNotTopical();
	}

	/**
	 * Sets, whether the value is not topical.
	 * <p>
	 * Abbreviation: NT
	 * 
	 * @param notTopical True, if the value shall be marked as not topical.
	 */
	public void setNotTopical(boolean notTopical) {
		qualityDescriptor.setNotTopical(notTopical);
	}

	/**
	 * Checks, whether the value is invalid.
	 * <p>
	 * Abbreviation: IV
	 * 
	 * @return True, if the value is marked as invalid.
	 */
	public boolean isInvalid() {
		return qualityDescriptor.isInvalid();
	}

	/**
	 * Sets, whether the value is invalid.
	 * <p>
	 * Abbreviation: IV
	 * 
	 * @param invalid True, if the value shall be marked as invalid.
	 */
	public void setInvalid(boolean invalid) {
		qualityDescriptor.setInvalid(invalid);
	}

	/**
	 * Empty default constructor.
	 */
	public SinglePointInformation() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param on                True if the set point is "on"; false for "off.
	 *                          <p>
	 *                          Abbreviation: SPI
	 * @param qualityDescriptor The quality descriptor for the set point. Can be
	 *                          blocked, substituted, not topical, or invalid.
	 */
	public SinglePointInformation(boolean on, QualityDescriptor qualityDescriptor) {
		this.on = on;
		this.qualityDescriptor = qualityDescriptor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (on ? 1231 : 1237);
		result = prime * result + ((qualityDescriptor == null) ? 0 : qualityDescriptor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SinglePointInformation other = (SinglePointInformation) obj;
		if (on != other.on) {
			return false;
		}
		if (qualityDescriptor == null) {
			if (other.qualityDescriptor != null) {
				return false;
			}
		} else if (!qualityDescriptor.equals(other.qualityDescriptor)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		}

		int byteAsInt = bytes[0] & 0xff;

		on = (byteAsInt & 0x01) == 0x01;
		qualityDescriptor = new QualityDescriptor();
		byte[] remainingBytes = qualityDescriptor.fromBytes(bytes);

		return remainingBytes;
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		byte[] qdsBytes = qualityDescriptor.toBytes();
		qdsBytes[0] |= (on ? 0x01 : 0x00);
		return qdsBytes;
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return 1;
	}

}