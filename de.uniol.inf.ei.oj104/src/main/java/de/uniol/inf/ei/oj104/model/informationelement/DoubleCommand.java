/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Optional;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * Information element that contains a command with three possible values.
 * <p>
 * Abbreviation: DCO
 * <p>
 * <ul>
 * <li>not permitted</li>
 * <li>off</li>
 * <li>on</li>
 * </ul>
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class DoubleCommand implements IInformationElement {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = -385922368643579139L;

	/**
	 * Enumeration for double command values.
	 * 
	 * @author Michael Brand (michael.brand@uol.de)
	 *
	 */
	public enum DoubleCommandEnum implements Serializable {

		/**
		 * Not permitted.
		 */
		NOT_PERMITTED_0(0),

		/**
		 * Off.
		 */
		OFF(1),

		/**
		 * On.
		 */
		ON(2),

		/**
		 * Not permitted.
		 */
		NOT_PERMITTED_3(3);

		/**
		 * The code of the command:
		 * <ul>
		 * <li>0, 3 = not permitted</li>
		 * <li>1 = off</li>
		 * <li>2 = on</li>
		 * </ul>
		 */
		private int code;

		/**
		 * Returns the code of the command.
		 * 
		 * @return An integer in:
		 *         <ul>
		 *         <li>0, 3 = not permitted</li>
		 *         <li>1 = off</li>
		 *         <li>2 = on</li>
		 *         </ul>
		 */
		public int getCode() {
			return code;
		}

		/**
		 * Constructor with fields.
		 * 
		 * @param code An integer in:
		 *             <ul>
		 *             <li>0, 3 = not permitted</li>
		 *             <li>1 = off</li>
		 *             <li>2 = on</li>
		 *             </ul>
		 */
		private DoubleCommandEnum(int code) {
			this.code = code;
		}

		/**
		 * Retrieve an enum entry.
		 * 
		 * @param code The code of the command:
		 *             <ul>
		 *             <li>0, 3 = not permitted</li>
		 *             <li>1 = off</li>
		 *             <li>2 = on</li>
		 *             </ul>
		 * @return An optional of the {@link DoubleCommandEnum} or
		 *         {@link Optional#empty()}, if there is none for the code.
		 */
		public static Optional<DoubleCommandEnum> getCommand(int code) {
			return Arrays.asList(values()).stream().filter(cmd -> cmd.getCode() == code).findAny();
		}
	}

	/**
	 * The command.
	 * <p>
	 * Abbreviation: DCS
	 * <p>
	 * Can have the following values:
	 * <ul>
	 * <li>{@link DoubleCommandEnum#NOT_PERMITTED_0}</li>
	 * <li>{@link DoubleCommandEnum#OFF}</li>
	 * <li>{@link DoubleCommandEnum#ON}</li>
	 * <li>{@link DoubleCommandEnum#NOT_PERMITTED_3}</li>
	 * </ul>
	 */
	private DoubleCommandEnum command;

	/**
	 * The command qualifier.
	 * <p>
	 * Abbreviation: QOC
	 * <p>
	 * Can have the following values:
	 * <ul>
	 * <li>0 = no additional information</li>
	 * <li>1 = short pulse duration</li>
	 * <li>2 = long pulse duration</li>
	 * <li>3 = persistent output</li>
	 * <li>4-8 = in compatible range</li>
	 * <li>9-15 = reserved for other predefined functions</li>
	 * <li>16-31 = in private range</li>
	 * </ul>
	 */
	private CommandQualifier qualifier;

	/**
	 * Returns the command.
	 * <p>
	 * Abbreviation: DCS
	 * <p>
	 * 
	 * @return One of the following values:
	 *         <ul>
	 *         <li>{@link DoubleCommandEnum#NOT_PERMITTED_0}</li>
	 *         <li>{@link DoubleCommandEnum#OFF}</li>
	 *         <li>{@link DoubleCommandEnum#ON}</li>
	 *         <li>{@link DoubleCommandEnum#NOT_PERMITTED_3}</li>
	 *         </ul>
	 */
	public DoubleCommandEnum getCommand() {
		return command;
	}

	/**
	 * Sets the command.
	 * 
	 * @param command
	 *                <p>
	 *                Abbreviation: DCS
	 *                <p>
	 *                Can have the following values:
	 *                <ul>
	 *                <li>{@link DoubleCommandEnum#NOT_PERMITTED_0}</li>
	 *                <li>{@link DoubleCommandEnum#OFF}</li>
	 *                <li>{@link DoubleCommandEnum#ON}</li>
	 *                <li>{@link DoubleCommandEnum#NOT_PERMITTED_3}</li>
	 *                </ul>
	 */
	public void setCommand(DoubleCommandEnum command) {
		this.command = command;
	}

	/**
	 * Returns the command qualifier.
	 * <p>
	 * Abbreviation: QOC
	 * 
	 * @return An integer in:
	 *         <ul>
	 *         <li>0 = no additional information</li>
	 *         <li>1 = short pulse duration</li>
	 *         <li>2 = long pulse duration</li>
	 *         <li>3 = persistent output</li>
	 *         <li>4-8 = in compatible range</li>
	 *         <li>9-15 = reserved for other predefined functions</li>
	 *         <li>16-31 = in private range</li>
	 *         </ul>
	 */
	public CommandQualifier getQualifier() {
		return qualifier;
	}

	/**
	 * Sets the command qualifier.
	 * <p>
	 * Abbreviation: QOC
	 * 
	 * @param qualifier An integer in:
	 *                  <ul>
	 *                  <li>0 = no additional information</li>
	 *                  <li>1 = short pulse duration</li>
	 *                  <li>2 = long pulse duration</li>
	 *                  <li>3 = persistent output</li>
	 *                  <li>4-8 = in compatible range</li>
	 *                  <li>9-15 = reserved for other predefined functions</li>
	 *                  <li>16-31 = in private range</li>
	 *                  </ul>
	 */
	public void setQualifier(CommandQualifier qualifier) {
		this.qualifier = qualifier;
	}

	/**
	 * Empty default constructor.
	 */
	public DoubleCommand() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param command   The command.
	 *                  <p>
	 *                  Abbreviation: DCS
	 *                  <p>
	 *                  Can have the following values:
	 *                  <ul>
	 *                  <li>{@link DoubleCommandEnum#NOT_PERMITTED_0}</li>
	 *                  <li>{@link DoubleCommandEnum#OFF}</li>
	 *                  <li>{@link DoubleCommandEnum#ON}</li>
	 *                  <li>{@link DoubleCommandEnum#NOT_PERMITTED_3}</li>
	 *                  </ul>
	 * @param qualifier The command qualifier.
	 *                  <p>
	 *                  Abbreviation: QOC
	 *                  <p>
	 *                  An integer in:
	 *                  <ul>
	 *                  <li>0 = no additional information</li>
	 *                  <li>1 = short pulse duration</li>
	 *                  <li>2 = long pulse duration</li>
	 *                  <li>3 = persistent output</li>
	 *                  <li>4-8 = in compatible range</li>
	 *                  <li>9-15 = reserved for other predefined functions</li>
	 *                  <li>16-31 = in private range
	 */
	public DoubleCommand(DoubleCommandEnum command, CommandQualifier qualifier) {
		this.command = command;
		this.qualifier = qualifier;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((command == null) ? 0 : command.hashCode());
		result = prime * result + ((qualifier == null) ? 0 : qualifier.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		DoubleCommand other = (DoubleCommand) obj;
		if (command != other.command) {
			return false;
		}
		if (qualifier == null) {
			if (other.qualifier != null) {
				return false;
			}
		} else if (!qualifier.equals(other.qualifier)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		}

		int byteAsInt = bytes[0] & 0xff;

		command = DoubleCommandEnum.getCommand(byteAsInt & 0x03).orElseThrow(
				() -> new IEC608705104ProtocolException(getClass(), byteAsInt + " is not a valid double command!"));
		qualifier = new CommandQualifier();
		byte[] remainingBytes = qualifier.fromBytes(bytes);

		return remainingBytes;
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		byte[] qdsBytes = qualifier.toBytes();
		qdsBytes[0] |= command.getCode();
		return qdsBytes;
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return 1;
	}

}
