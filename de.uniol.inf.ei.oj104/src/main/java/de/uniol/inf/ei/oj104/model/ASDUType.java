/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model;

import java.io.Serializable;

import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * The ASDU type defines the structure, type and format of the following
 * {@link IInformationObject}s. An ASDU type has an id, a code and a
 * description. The ids and codes are defined in the standard. The ids range
 * from 0 to 127, while the code is also a unique string, if not empty, e.g.
 * "M_SP_NA_1". For reserved ids, the code is in the standard empty. The
 * description is not necessary to be machine readable but may be useful for
 * humans.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class ASDUType implements Serializable {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = 1157525030901795703L;

	/**
	 * The unique id of the ASDU type in [0, 127].
	 */
	private int id;

	/**
	 * The description is not necessary to be machine readable but may be useful for
	 * humans.
	 */
	private String description;

	/**
	 * The code of the ASDU type, e.g. "M_SP_NA_1". It can be empty for reserved but
	 * not defined ids. If it is not empty, it is unique.
	 */
	private String code;

	/**
	 * Returns the unique id of the ASDU type.
	 * 
	 * @return An integer in [0, 127].
	 */
	public int getId() {
		return id;
	}

	/**
	 * Returns the description of the ASDU type.
	 * 
	 * @return A String describing the ASDU type for humans.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Returns the code of the ASDU type.
	 * 
	 * @return A string that can be empty for reserved but not defined ids. If it is
	 *         not empty, it is unique.
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Empty default constructor.
	 */
	public ASDUType() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param id          The unique id of the ASDU type in [0, 127].
	 * @param description The description is not necessary to be machine readable
	 *                    but may be useful for humans.
	 * @param code        The code of the ASDU type, e.g. "M_SP_NA_1". It can be
	 *                    empty for reserved but not defined ids. If it is not
	 *                    empty, it is unique.
	 */
	public ASDUType(int id, String description, String code) {
		this.id = id;
		this.description = description;
		this.code = code;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ASDUType other = (ASDUType) obj;
		if (code == null) {
			if (other.code != null) {
				return false;
			}
		} else if (!code.equals(other.code)) {
			return false;
		}
		if (description == null) {
			if (other.description != null) {
				return false;
			}
		} else if (!description.equals(other.description)) {
			return false;
		}
		if (id != other.id) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

}