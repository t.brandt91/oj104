/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.util.JsonParser;
import de.uniol.inf.ei.oj104.util.SystemProperties;

/**
 * Class for single information objects. A single information object has an
 * ordered list of information elements, can have a time tag, and has an
 * information object address (IOA). The classes of the information elements and
 * the timetag are managed in the base class,
 * {@link AbstractInformationElementSequence}.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class SingleInformationObject extends InformationElementSequence implements IInformationObject {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = -8086008708289268973L;

	/**
	 * The number of bytes for an encoded IOA, read from properties file
	 * <code>SystemProperties.xml</code>.
	 */
	private static final int ioaEncodedSize = Integer
			.parseInt(SystemProperties.getProperties().getProperty("ioa.bytesize", "1"));

	/**
	 * The IOA of the information object is an integer of two or three bytes (system
	 * property dependent). 0 := irrelevant
	 */
	private int informationObjectAddress;

	@Override
	public int getInformationObjectAddress() {
		return informationObjectAddress;
	}

	/**
	 * Sets the IOA of the information object.
	 * 
	 * @param informationObjectAddress An integer of two or three bytes (system
	 *                                 property dependent). 0 := irrelevant
	 */
	public void setInformationObjectAddress(int informationObjectAddress) {
		this.informationObjectAddress = informationObjectAddress;
	}

	/**
	 * Empty default constructor.
	 */
	public SingleInformationObject() {
	}

	/**
	 * Creates an empty information object.
	 * 
	 * @param informationElementClasses An ordered list of the classes of the
	 *                                  information elements, contained in the
	 *                                  sequence.
	 * @param timeTagClass              The class of the time tag object of the
	 *                                  sequence or <code>null</code>, if the
	 *                                  sequence has no time tag.
	 */
	public SingleInformationObject(List<Class<? extends IInformationElement>> informationElementClasses,
			Class<? extends ITimeTag> timeTagClass) {
		super(informationElementClasses, timeTagClass);
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param informationElementClasses An ordered list of the classes of the
	 *                                  information elements, contained in the
	 *                                  sequence.
	 * @param informationElements       An ordered list of the information elements,
	 *                                  contained in the sequence. The elements must
	 *                                  be of the correct classes, defined in
	 *                                  <code>informationElementClasses</code>.
	 * @param timeTagClass              The class of the time tag object of the
	 *                                  sequence or <code>null</code>, if the
	 *                                  sequence has no time tag.
	 * @param timeTag                   The time tag object for the sequence. Can be
	 *                                  <code>null</code>, if the sequence has no
	 *                                  time tag. The time tag must be of the
	 *                                  correct class, defined in
	 *                                  <code>timeTagClass</code>.
	 * @param informationObjectAddress  An integer of two or three bytes (system
	 *                                  property dependent). 0 := irrelevant
	 */
	public SingleInformationObject(List<Class<? extends IInformationElement>> informationElementClasses,
			List<IInformationElement> informationElements, Class<? extends ITimeTag> timeTagClass, ITimeTag timeTag,
			int informationObjectAddress) {
		super(informationElementClasses, informationElements, timeTagClass, timeTag);
		this.informationObjectAddress = informationObjectAddress;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + informationObjectAddress;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SingleInformationObject other = (SingleInformationObject) obj;
		if (informationObjectAddress != other.informationObjectAddress) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < ioaEncodedSize) {
			throw new IEC608705104ProtocolException(getClass(), ioaEncodedSize, bytes.length);
		}

		informationObjectAddress = bytes[0] & 0xff;
		for (int i = 1; i < ioaEncodedSize; i++) {
			informationObjectAddress |= (bytes[i] & 0xff) << 8 * i;
		}
		byte[] remainingBytes = Arrays.copyOfRange(bytes, ioaEncodedSize, bytes.length);
		return super.fromBytes(remainingBytes);
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		byte[] ioaBytes = new byte[ioaEncodedSize];
		ioaBytes[0] = (byte) informationObjectAddress;
		for (int i = 1; i < ioaEncodedSize; i++) {
			ioaBytes[i] = (byte) (informationObjectAddress >> 8 * i);
		}
		return ArrayUtils.addAll(ioaBytes, super.toBytes());
	}

}