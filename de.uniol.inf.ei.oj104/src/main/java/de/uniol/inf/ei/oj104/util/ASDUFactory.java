/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import de.uniol.inf.ei.oj104.model.ASDUType;
import de.uniol.inf.ei.oj104.model.CauseOfTransmission;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.model.ITimeTag;
import de.uniol.inf.ei.oj104.model.StructureQualifier;

/**
 * Util class that loads all information about {@link ASDUType}s from
 * /META-INF/ASDUs.xml and that provides several getters. A single
 * {@link ASDUType} entry has an asdu tag and the following attributes:
 * <ul>
 * <li>id: The consecutive number used in the IEC 60870-5-101 standard.</li>
 * <li>description: The short description used in the IEC 60870-5-101 standard,
 * e.g. "single-point information"</li>
 * </ul>
 * Furthermore, an asdu tag can have several subtags:
 * <ul>
 * <li>code: The code used in the IEC 60870-5-101 standard, e.g.
 * "M_SP_NA_1"</li>
 * <li>ieclasses: A list of all classes of {@link IInformationElement}s used in
 * the {@link ASDUType}, e.g.
 * de.uniol.inf.ei.oj104.model.informationelement.SinglePointInformation.</li>
 * <li>ttclass: The {@link ITimeTag} class used in the {@link ASDUType} if used,
 * e.g. de.uniol.inf.ei.oj104.model.timetag.ThreeOctetBinaryTime.</li>
 * <li>sq: A list of all possible sequence numbers for the {@link ASDUType}: 0
 * for sequence of information objects, 1 for sequence of information elements
 * in a single information object.</li>
 * <li>cot: A list of all possible {@link CauseOfTransmission} ids for the
 * {@link ASDUType}, e.g. 1.</li>
 * </ul>
 * It is also possible to define a range of equal {@link ASDUType}s, e.g. for a
 * private range. That can be done with an asdu_range tag that has the following
 * attributes:
 * <ul>
 * <li>first_id: The first consecutive number used in the IEC 60870-5-101
 * standard.</li>
 * <li>last_id: The last consecutive number used in the IEC 60870-5-101
 * standard.</li>
 * <li>description: The short description used in the IEC 60870-5-101 standard,
 * e.g. "reserved for further compatible definitions"</li>
 * </ul>
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class ASDUFactory {

	/**
	 * The logger instance for this class.
	 */
	private static final Logger logger = LoggerFactory.getLogger(ASDUFactory.class);

	/**
	 * The XML file containing the information about the ASDU types.
	 */
	private static final String xmlFile = "/META-INF/ASDUs.xml";

	/**
	 * The XML tag of a single ASDU entry in {@link #xmlFile}.
	 */
	private static final String asduTag = "asdu";

	/**
	 * The XML tag of an ASDU range entry, e.g. "reserved for further compatible
	 * definitions" in {@link #xmlFile}.
	 */
	private static final String asduRangeTag = "asdu_range";

	/**
	 * The key of the ID attribute in an {@link #asduTag}. The IDs are subsequent
	 * numbers defined in the standard.
	 */
	private static final String idAttribute = "id";

	/**
	 * The key of the first ID attribute in an {@link #asduRangeTag}. The IDs are
	 * subsequent numbers defined in the standard.
	 */
	private static final String firstIdAttribute = "first_id";

	/**
	 * The key of the last ID attribute in an {@link #asduRangeTag}. The IDs are
	 * subsequent numbers defined in the standard.
	 */
	private static final String lastIdAttribute = "last_id";

	/**
	 * The key of the description attribute in an {@link #asduTag} or
	 * {@link #asduRangeTag}. The descriptions are human readable and defined in the
	 * standard.
	 */
	private static final String descriptionAttribute = "description";

	/**
	 * The key of the code attribute in an {@link #asduTag}, e.g. "M_SP_NA_1". The
	 * codes are defined in the standard.
	 */
	private static final String codeTag = "code";

	/**
	 * The XML tag of the list of used information element classes (full qualified
	 * class names) in an {@link #asduTag}.
	 */
	private static final String informationElementClassesTag = "ieclasses";

	/**
	 * The XML tag of the used time tag class (full qualified class name) in an
	 * {@link #asduTag}. Must not be given.
	 */
	private static final String timeTagClassTag = "ttclass";

	/**
	 * The XML tag of the list of possible structure qualifiers (their codes) in an
	 * {@link #asduTag}.
	 */
	private static final String sqListTag = "sq";

	/**
	 * The XML tag of the list of possible causes of transmission (their codes) in
	 * an {@link #asduTag}.
	 */
	private static final String cotListTag = "cot";

	/**
	 * Mapping of all {@link ASDUType}s, read from XML, to their IDs.
	 */
	private static Map<Integer, ASDUType> types = new HashMap<>();

	/**
	 * Mapping of the list of {@link IInformationElement} classes for an
	 * {@link ASDUType}, read from XML, to the ID of the {@link ASDUType}.
	 */
	private static Map<Integer, List<Class<? extends IInformationElement>>> informationElements = new HashMap<>();

	/**
	 * Mapping of the {@link ITimeTag} class for an {@link ASDUType}, read from XML,
	 * to the ID of the {@link ASDUType}.
	 */
	private static Map<Integer, Class<? extends ITimeTag>> timeTags = new HashMap<>();

	/**
	 * Mapping of the list of {@link StructureQualifier}s for an {@link ASDUType},
	 * read from XML, to the ID of the {@link ASDUType}.
	 */
	private static Map<Integer, List<StructureQualifier>> sqs = new HashMap<>();

	/**
	 * Mapping of the list of {@link CauseOfTransmission}s for an {@link ASDUType},
	 * read from XML, to the ID of the {@link ASDUType}.
	 */
	private static Map<Integer, List<CauseOfTransmission>> cots = new HashMap<>();

	static {
		init();
	}

	/**
	 * Loads everything from XML file.
	 * <p>
	 * Calls {@link #loadFromASDUElements(Document, String)} for single asdus and
	 * ranges.
	 */
	private static void init() {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		try (InputStream in = CauseOfTransmissionFactory.class.getResourceAsStream(xmlFile)) {
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(in);
			doc.getDocumentElement().normalize();
			loadFromASDUElements(doc, asduTag);
			loadFromASDUElements(doc, asduRangeTag);
		} catch (SAXException | ParserConfigurationException | IOException e1) {
			logger.error("Error while parsing ASDUs.xml");
		}
	}

	/**
	 * Fills the following maps with information:
	 * <ul>
	 * <li>{@link #types}</li>
	 * <li>{@link #informationElements}</li>
	 * <li>{@link #timeTags}</li>
	 * <li>{@link #sqs}</li>
	 * <li>{@link #cots}</li>
	 * </ul>
	 * 
	 * @param id          The ID of the {@link ASDUType}. The IDs are subsequent
	 *                    numbers defined in the standard.
	 * @param description The description of the {@link ASDUType}. The descriptions
	 *                    are human readable and defined in the standard.
	 * @param code        The code of the {@link ASDUType}, e.g. "M_SP_NA_1". The
	 *                    codes are defined in the standard.
	 * @param ieClasses   The list of used information element classes (full
	 *                    qualified class names) in the {@link ASDUType}.
	 * @param ttClass     The used time tag class (full qualified class names) in
	 *                    the {@link ASDUType}. May be null.
	 * @param sqList      The list of possible structure qualifiers (their codes) in
	 *                    the {@link ASDUType}.
	 * @param cotList     The list of possible causes of transmission (their codes)
	 *                    in the {@link ASDUType}.
	 */
	@SuppressWarnings("unchecked")
	private static void createAndLinkASDUTyp(int id, String description, String code, String ieClasses, String ttClass,
			String sqList, String cotList) {
		ASDUType type = new ASDUType(id, description, code);
		types.put(id, type);

		if (ieClasses != null && !ieClasses.isEmpty()) {
			String[] ieClassesSplitted = ieClasses.split(" ");
			if (ieClassesSplitted.length == 0) {
				logger.error(ieClasses + " is not a list of information element classes!");
			}
			List<Class<? extends IInformationElement>> classes = new ArrayList<>();
			for (String clz : ieClassesSplitted) {
				try {
					Class<?> informationElementClass = Class.forName(clz);
					if (!IInformationElement.class.isAssignableFrom(informationElementClass)) {
						logger.error(informationElementClass + " is not implementing IInformationElement!");
						return;
					}
					classes.add((Class<? extends IInformationElement>) informationElementClass);
				} catch (ClassNotFoundException e) {
					logger.error("Could not find information element class {}", clz, e);
				}
			}
			ASDUFactory.informationElements.put(id, classes);
		}

		if (ttClass != null && !ttClass.isEmpty()) {
			try {
				Class<?> timeTagClass = Class.forName(ttClass);
				if (!ITimeTag.class.isAssignableFrom(timeTagClass)) {
					logger.error(timeTagClass + " is not implementing ITimeTag!");
					return;
				}
				timeTags.put(id, (Class<? extends ITimeTag>) timeTagClass);
			} catch (ClassNotFoundException e) {
				logger.error("Could not find time tag class {}", ttClass, e);
			}
		}

		if (sqList != null && !sqList.isEmpty()) {
			String[] sqListSplitted = sqList.split(" ");
			if (sqListSplitted.length == 0) {
				logger.error(sqList + " is not a list of structure qualifiers!");
			}
			List<StructureQualifier> sqs = new ArrayList<>();
			for (String sq : sqListSplitted) {
				try {
					sqs.add(StructureQualifier.getQualifier(Integer.parseInt(sq))
							.orElseThrow(() -> new IllegalArgumentException()));
				} catch (IllegalArgumentException | NullPointerException e) {
					logger.error(sq + " is not a valid structure qualifier!");
				}
			}
			ASDUFactory.sqs.put(id, sqs);
		}

		if (cotList != null && !cotList.isEmpty()) {
			String[] cotListSplitted = cotList.split(" ");
			if (cotListSplitted.length == 0) {
				logger.error(cotList + " is not a list of cause of transmission ids!");
			}
			List<CauseOfTransmission> cots = new ArrayList<>();
			for (String cot : cotListSplitted) {
				if (cot.equals("")) {
					continue;
				}
				try {
					cots.add(CauseOfTransmissionFactory.getCauseOfTransmission(Integer.parseInt(cot))
							.orElseThrow(() -> new NullPointerException()));
				} catch (NullPointerException e) {
					logger.error(cot + " is not a valid cause of transmission id!");
				}
			}
			ASDUFactory.cots.put(id, cots);
		}
	}

	/**
	 * Loads either all single ASDU entries or ASDU range entries from an XML
	 * document.
	 * <p>
	 * Calls
	 * {@link #createAndLinkASDUTyp(int, String, String, String, String, String, String)}
	 * for each found entry.
	 * 
	 * @param doc     The XML document.
	 * @param tagName Either {@link #asduTag} or {@link #asduRangeTag}.
	 */
	private static void loadFromASDUElements(Document doc, String tagName) {
		NodeList asduNodeList = doc.getElementsByTagName(tagName);
		for (int i = 0; i < asduNodeList.getLength(); i++) {
			Node asduNode = asduNodeList.item(i);
			if (asduNode.getNodeType() == Node.ELEMENT_NODE) {
				Element asdu = (Element) asduNode;
				String description = asdu.getAttribute(descriptionAttribute);
				String code = "";
				String ieClasses = "";
				String ttClass = "";
				String sqList = "";
				String cotList = "";

				for (int j = 0; j < asduNode.getChildNodes().getLength(); j++) {
					Node childNode = asduNode.getChildNodes().item(j);
					if (childNode.getNodeType() == Node.ELEMENT_NODE) {
						Element child = (Element) childNode;
						switch (child.getTagName()) {
						case codeTag:
							code = child.getTextContent();
							break;
						case informationElementClassesTag:
							ieClasses = child.getTextContent();
							break;
						case timeTagClassTag:
							ttClass = child.getTextContent();
							break;
						case sqListTag:
							sqList = child.getTextContent();
							break;
						case cotListTag:
							cotList = child.getTextContent();
							break;
						default:
							logger.error("Unknown XML tag: {}", child.getTagName());
							continue;
						}
					}
				}

				int startId = tagName.equals(asduRangeTag) ? Integer.parseInt(asdu.getAttribute(firstIdAttribute))
						: Integer.parseInt(asdu.getAttribute(idAttribute));
				int endId = tagName.equals(asduRangeTag) ? Integer.parseInt(asdu.getAttribute(lastIdAttribute))
						: Integer.parseInt(asdu.getAttribute(idAttribute));

				for (int id = startId; id <= endId; id++) {
					createAndLinkASDUTyp(id, description, code, ieClasses, ttClass, sqList, cotList);
				}
			}
		}
	}

	/**
	 * Get all stored {@link ASDUType}s.
	 */
	public static Collection<ASDUType> getAllASDUTypes() {
		return types.values();
	}

	/**
	 * Get the {@link ASDUType} with a given id if present.
	 */
	public static Optional<ASDUType> getASDUType(int id) {
		return Optional.ofNullable(types.get(id));
	}

	/**
	 * Get the id of all stored {@link ASDUType}s.
	 */
	public static Set<Integer> getAllASDUTypeIds() {
		return types.keySet();
	}

	/**
	 * Get the list of all classes of {@link IInformationElement}s used in the
	 * {@link ASDUType}, e.g.
	 * de.uniol.inf.ei.oj104.model.informationelement.SinglePointInformation.
	 */
	public static List<Class<? extends IInformationElement>> getInformationElementClasses(int asduTypeId) {
		return Optional.ofNullable(informationElements.get(asduTypeId)).orElse(new ArrayList<>());
	}

	/**
	 * Get the {@link ITimeTag} class used in the {@link ASDUType} if used, e.g.
	 * de.uniol.inf.ei.oj104.model.timetag.ThreeOctetBinaryTime.
	 */
	public static Optional<Class<? extends ITimeTag>> getTimeTagClass(int asduTypeId) {
		return Optional.ofNullable(timeTags.get(asduTypeId));
	}

	/**
	 * Get a list of all possible sequence numbers for the {@link ASDUType}: 0 for
	 * sequence of information objects, 1 for sequence of information elements in a
	 * single information object.
	 */
	public static List<StructureQualifier> getStructureQualifiers(int asduTypeId) {
		return Optional.ofNullable(sqs.get(asduTypeId)).orElse(new ArrayList<>());
	}

	/**
	 * Get a list of all possible {@link CauseOfTransmission} ids for the
	 * {@link ASDUType}, e.g. 1.
	 */
	public static List<CauseOfTransmission> getCausesOfTransmission(int asduTypeId) {
		return Optional.ofNullable(cots.get(asduTypeId)).orElse(new ArrayList<>());
	}

}