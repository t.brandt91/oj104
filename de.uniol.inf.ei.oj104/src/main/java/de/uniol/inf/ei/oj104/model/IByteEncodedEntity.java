/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model;

import java.io.Serializable;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;

/**
 * Interface for 104 elements that need to be de-/serialized from/to bytes for
 * communication. It prescribes methods for the serialization and
 * deserialization.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public interface IByteEncodedEntity extends Serializable {

	/**
	 * Deserializes a byte representation (see 104 standard) of an 104 element to
	 * its java object. The handling is, that you have an array of the complete 104
	 * message and give it to the contained elements in the correct order (e.g.,
	 * first APCI, then ASDU). The fromBytes method will always return the remaining
	 * bytes after deserialization.
	 * 
	 * @param bytes The byte representation of the 104 element. It may contain more
	 *              bytes than belonging to the object.
	 * @return The remaining bytes of the input after deserialization.
	 * @throws IEC608705104ProtocolException if the 104 object could not be created
	 *                                       from the bytes.
	 */
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException;

	/**
	 * Serializes an 104 element into a byte representation (see 104 standard).
	 * 
	 * @return The byte serialization.
	 * @throws IEC608705104ProtocolException if the 104 object could not be
	 *                                       serialized.
	 */
	public byte[] toBytes() throws IEC608705104ProtocolException;

}