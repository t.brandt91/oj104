/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.communication;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uniol.inf.ei.oj104.application.ClientApplication;
import de.uniol.inf.ei.oj104.application.ServerApplication;
import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.APCI;
import de.uniol.inf.ei.oj104.model.APDU;
import de.uniol.inf.ei.oj104.model.ASDU;
import de.uniol.inf.ei.oj104.model.IControlField;
import de.uniol.inf.ei.oj104.model.controlfield.ControlFunctionType;
import de.uniol.inf.ei.oj104.model.controlfield.InformationTransfer;
import de.uniol.inf.ei.oj104.model.controlfield.NumberedSupervisoryFunction;
import de.uniol.inf.ei.oj104.model.controlfield.UnnumberedControlFunction;
import de.uniol.inf.ei.oj104.util.SystemProperties;

/**
 * Standard implementation of an {@link IAPDUHandler}. An {@link IAPDUHandler}
 * is the central part of the communication architecture of OJ104. The other
 * parts are an {@link ICommunicationHandler} and an {@link IASDUHandler}. An
 * {@link IAPDUHandler} has two main objectives:
 * <ol>
 * <li>Handling of incoming {@link APDU}s from an {@link ICommunicationHandler}.
 * This handling includes the handling of send and reseice sequence numbers and
 * timers as well as the sending of responses via an
 * {@link ICommunicationHandler}.</li>
 * <li>Sending of {@link UnnumberedControlFunction}s to start or stop the data
 * transfer.</li>
 * </ol>
 * The {@link StandardAPDUHandler} is implemented to follow the protocol as
 * defined in the standard with all timeouts etc. The length of timeouts and
 * other application variables can be set via the {@link SystemProperties}.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class StandardAPDUHandler implements IAPDUHandler {

	/**
	 * Logger, if it is not clear whether an {@link ServerApplication} or an
	 * {@link ClientApplication} uses this APDU handler.
	 */
	private static final Logger defaultLogger = LoggerFactory.getLogger(StandardAPDUHandler.class);

	/**
	 * Duration for the timer t1, that defines how long to wait for responses.
	 */
	private static final int t1Duration = Integer.parseInt(SystemProperties.getProperties().getProperty("t1", "30000"));

	/**
	 * Duration for the timer t2, that defines how long to wait for an ASDU to build
	 * as a response.
	 */
	private static final int t2Duration = Integer.parseInt(SystemProperties.getProperties().getProperty("t2", "30000"));

	/**
	 * Duration for the timer t3, that defines how long, in case of inactivity, to
	 * wait before sending a test APDU.
	 */
	private static final int t3Duration = Integer.parseInt(SystemProperties.getProperties().getProperty("t3", "-1"));

	/**
	 * The amount of threads that can be used to handle ASDUs.
	 */
	private static final int numExecutors = Integer
			.parseInt(SystemProperties.getProperties().getProperty("asduhandler.numexecutors", "1"));

	/**
	 * The logger to use.
	 */
	private Logger logger = defaultLogger;

	/**
	 * The {@link IASDUHandler} has the objective to provide an {@link ASDU} as
	 * response to an incoming {@link ASDU} from an {@link IAPDUHandler}.
	 */
	private volatile IASDUHandler asduHandler;

	/**
	 * The {@link ICommunicationHandler} handle sthe sending and receiving of 104
	 * messages.
	 */
	private volatile ICommunicationHandler communicationHandler;

	/**
	 * Flag, whether a startDT message has been send or received.
	 */
	private boolean dtStarted = false;

	/**
	 * If set to true, it will neither be checked, if dtStarted is true nor if send
	 * and receive numbers are correct.
	 */
	private boolean ignoreHandshakes = false;

	/**
	 * If set to true, no timers are used to check timeouts.
	 */
	private boolean ignoreTimeouts = false;

	/**
	 * If set to false, no responses to APDUs will be send.
	 */
	private boolean sendResponses = true;

	/**
	 * Map of timers t1, that define how long to wait for responses. <br />
	 * There exists one timer per sent APDU.
	 */
	private Map<Short, Timer> t1PerSendAPDU = new HashMap<>();

	/**
	 * See t1PerSendAPDU. <br />
	 * Extra map of timers in case that sendState has been reseted to 0.
	 */
	private Map<Short, Timer> t1PerSendAPDU_afterReset = new HashMap<>();

	/**
	 * Timer that waits, in case of inactivity, before sending a test APDU.
	 */
	private Timer t3;

	/**
	 * Timer that waits for a response to a test APDU.
	 */
	private Timer testT1;

	/**
	 * Timer that waits for a response to a startDT APDU.
	 */
	private Timer startDtT1;

	/**
	 * Timer that waits for a response to a stopDT APDU.
	 */
	private Timer stopDtT1;

	/**
	 * The receive state is the amount of received APDUs. <br/>
	 * Send and receive state are send with an APDU to the communication partner for
	 * checking. <br />
	 * The current receive state needs to fit the send number in a received APDU.
	 */
	private Short receiveState = 0;

	/**
	 * The send state is the amount of sent APDUs. Send and receive state are send
	 * with an APDU to the communication partner for checking.
	 */
	private Short sendState = 0;
	
	/**
	 * Send state may need to be reseted to 0 if Short.MAX_VALUE is reached.
	 */
	private boolean resetSendState = false;

	/**
	 * The ack state is the amount of sent APDUs that are received by the
	 * communication partner (they are acknowledged).
	 */
	private short ackState = 0;

	/**
	 * Sent APDU mapped to their send sequence number. <br />
	 * Buffer is needed, if messages are not acknowledged. Then, they are sent
	 * again.
	 */
	private Map<Short, APDU> apduBuffer = new HashMap<>();
	
	/**
	 * See apduBuffer. <br />
	 * Extra map of APDUs in case that sendState has been reseted to 0.
	 */
	private Map<Short, APDU> apduBuffer_afterReset = new HashMap<>();

	/**
	 * Executor service for ASDU handler threads.
	 */
	private ExecutorService asduHandlerThreadService = Executors.newFixedThreadPool(numExecutors, new ThreadFactory() {

		int no = 0;

		@Override
		public Thread newThread(Runnable r) {
			return new Thread(r, "104ASDUHandlerThread #" + no++);
		}

	});

	/**
	 * Executor service for threads, that wait for ASDU handlers.
	 */
	private ExecutorService asduHandlerWaiterThreadService = Executors.newFixedThreadPool(numExecutors,
			new ThreadFactory() {

				int no = 0;

				@Override
				public Thread newThread(Runnable r) {
					return new Thread(r, "104ASDUHandlerWaiterThread #" + no++);
				}

			});

	/**
	 * Sets the logger to use.
	 */
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	@Override
	public void setASDUHandler(IASDUHandler handler) {
		asduHandler = handler;
	}

	@Override
	public void setCommunicationHandler(ICommunicationHandler handler) {
		communicationHandler = handler;
	}

	@Override
	public boolean isDataTransferStarted() {
		return dtStarted;
	}

	@Override
	public void ignoreHandshakes(boolean ignore) {
		ignoreHandshakes = ignore;
	}

	@Override
	public boolean areHandshakesIgnored() {
		return ignoreHandshakes;
	}

	@Override
	public void ignoreTimeouts(boolean ignore) {
		ignoreTimeouts = ignore;
	}

	@Override
	public boolean areTimeoutsIgnored() {
		return ignoreTimeouts;
	}

	@Override
	public void sendResponses(boolean send) {
		sendResponses = send;
	}

	@Override
	public boolean areResponsesSent() {
		return sendResponses;
	}

	@Override
	public void stopAllThreads() {
		t1PerSendAPDU.values().forEach(t1 -> cancelIfPresent(t1));
		t1PerSendAPDU_afterReset.values().forEach(t1 -> cancelIfPresent(t1));
		t1PerSendAPDU.clear();
		t1PerSendAPDU_afterReset.values().forEach(t1 -> cancelIfPresent(t1));
		t1PerSendAPDU_afterReset.clear();
		cancelIfPresent(t3);
		cancelIfPresent(testT1);
		cancelIfPresent(startDtT1);
		cancelIfPresent(stopDtT1);
		asduHandlerThreadService.shutdownNow();
		asduHandlerWaiterThreadService.shutdownNow();
	}

	/**
	 * Cancelled a timer with nullcheck.
	 */
	private void cancelIfPresent(Timer t) {
		if (t != null) {
			t.cancel();
			t.purge();
			t = null;
		}
	}

	/**
	 * Cancels and removes all t1 timers for APDUs with a send sequence number lower
	 * or equal to the given receive sequence number.
	 */
	private void cancelT1Timers(int receiveSequenceNumber) {
		if (t1PerSendAPDU_afterReset.isEmpty() || receiveSequenceNumber > sendState) {
			// we only need to deal with t1PerSendAPDU map
			t1PerSendAPDU.entrySet().stream().filter(entry -> entry.getKey() <= receiveSequenceNumber)
					.forEach(entry -> cancelIfPresent(entry.getValue()));
			t1PerSendAPDU.entrySet().removeIf(entry -> entry.getKey() <= receiveSequenceNumber);
		} else {
			// we need to deal with both maps: t1PerSendAPDU and t1PerSendAPDU_afterReset
			t1PerSendAPDU.entrySet().stream().forEach(entry -> cancelIfPresent(entry.getValue()));
			t1PerSendAPDU.entrySet().clear();

			t1PerSendAPDU_afterReset.entrySet().stream().filter(entry -> entry.getKey() <= receiveSequenceNumber)
					.forEach(entry -> cancelIfPresent(entry.getValue()));
			t1PerSendAPDU_afterReset.entrySet().removeIf(entry -> entry.getKey() <= receiveSequenceNumber);

			t1PerSendAPDU.putAll(t1PerSendAPDU_afterReset);
			t1PerSendAPDU_afterReset.clear();
			
			resetSendState = false;
		}
	}

	/**
	 * Creates a timer that closes the connection to the communication partner.
	 */
	private TimerTask createCloseConnectionTimerTask() {
		return new TimerTask() {

			@Override
			public void run() {
				communicationHandler.closeConnection();
			}

		};
	}

	/**
	 * Starts a t3 timer and stops the current t3 timer, if there is an active one.
	 * If t3 fires, a test APDU is sent and a t1 timer, that waits for the response
	 * to the test APDU, is started.
	 */
	private void startT3Timer() {
		cancelIfPresent(t3);
		t3 = new Timer("t3");
		TimerTask task = new TimerTask() {

			@Override
			public void run() {
				// send test ASDU
				APDU apdu = new APDU(new APCI(IControlField.getEncodedSize(),
						new UnnumberedControlFunction(ControlFunctionType.TESTFR, true)));
				sendAPDUandLogErrors(apdu);

				// start timer for response
				testT1 = new Timer("t1Test");
				TimerTask task = createCloseConnectionTimerTask();
				testT1.schedule(task, t1Duration);
			}

		};
		t3.schedule(task, t3Duration);
	}

	/**
	 * Sends an APDU via the communication handler and logs possible exceptions.
	 */
	private void sendAPDUandLogErrors(APDU apdu) {
		try {
			communicationHandler.send(apdu);
		} catch (IOException | IEC608705104ProtocolException e) {
			logger.error("Error while sending {}!", apdu, e);
		}
	}

	/*
	 * Retrieval of APDUs
	 */

	@Override
	public void handleAPDU(APDU apdu) throws IEC608705104ProtocolException, IOException {
		// cancel t3 timer
		if (!ignoreTimeouts) {
			cancelIfPresent(t3);
		}

		IControlField controlField = apdu.getApci().getControlField();
		if (controlField instanceof UnnumberedControlFunction) {
			handleUnnumberedControlFunction((UnnumberedControlFunction) controlField, apdu);
		} else if (controlField instanceof InformationTransfer) {
			handleInformationTransfer((InformationTransfer) controlField, apdu);
		} else if (controlField instanceof NumberedSupervisoryFunction) {
			handleNumberedSupervisoryFunction((NumberedSupervisoryFunction) controlField, apdu);
		} else {
			logger.warn("Unknown control field type: {}", controlField.getClass());
		}

		// start timer to send test APDU, if test APDUs are wanted and if channel
		// remains unused in that time
		if (!ignoreTimeouts) {
			startT3Timer();
		}
	}

	/**
	 * Handles APDUs in S format (an acknowledgement to which sequence number the
	 * APDUs has been received).
	 */
	private void handleNumberedSupervisoryFunction(NumberedSupervisoryFunction controlField, APDU apdu)
			throws IEC608705104ProtocolException, IOException {
		// 1. check whether data transfer is started
		if (!ignoreHandshakes && !dtStarted) {
			throw new IEC608705104ProtocolException("Received numbered supervisory function before startDT!");
		}

		// 2. cancel timers t1 (timeout for response)
		if (!ignoreTimeouts) {
			cancelT1Timers(controlField.getReceiveSequenceNumber());
		}

		// 3. check whether message has been acknowledged
		if (!ignoreHandshakes) {
			handleReceiveSequenceNumberForAck(controlField.getReceiveSequenceNumber(), apdu);
		}
	}

	/**
	 * Handles APDUs in I format (an APDU with an ASDU that needs to be handled).
	 */
	private void handleInformationTransfer(InformationTransfer controlField, APDU apdu)
			throws IEC608705104ProtocolException, IOException {
		// 1. check whether data transfer is started
		if (!ignoreHandshakes && !dtStarted) {
			throw new IEC608705104ProtocolException("Received information transfer before startDT!");
		}

		synchronized (receiveState) {
			// 2. check whether send and receive variables match
			if (!ignoreHandshakes && receiveState != controlField.getSendSequenceNumber()) {
				throw new IEC608705104ProtocolException("Send (" + controlField.getSendSequenceNumber()
						+ ") and receive sequence numbers (" + receiveState + ") do not match for '" + apdu + "'!");
			}

			receiveState = (short) (receiveState < Short.MAX_VALUE ? receiveState+1 : 0);
			logger.debug("Set receiveState to '{}'.", receiveState);

			// 3. cancel timers t1 (timeout for response).
			if (!ignoreTimeouts) {
				cancelT1Timers(controlField.getReceiveSequenceNumber());
			}

			// 4. check whether message has been acknowledged
			if (!ignoreHandshakes) {
				handleReceiveSequenceNumberForAck(controlField.getReceiveSequenceNumber(), apdu);
			}

		}

		// 5. call ASDU handler
		Future<Optional<ASDU>> futureResponseASDU = asduHandlerThreadService
				.submit(() -> asduHandler.handleASDU(apdu.getAsdu()));

		if (sendResponses) {
			asduHandlerWaiterThreadService.submit(() -> handleFutureResponseASDU(futureResponseASDU));
		}
	}

	/**
	 * Handles the future response of an ASDU handler. This includes the waiting for
	 * the handler and also the sending of an S format APDU as an answer, if the
	 * handler does not provide a response.
	 */
	private void handleFutureResponseASDU(Future<Optional<ASDU>> futureAsdu) {
		Optional<ASDU> responseASDU = Optional.empty();

		// Wait for response ASDU
		try {
			responseASDU = futureAsdu.get(t2Duration, TimeUnit.MILLISECONDS);
		} catch (InterruptedException | ExecutionException | TimeoutException e) {
			// timeout: build response in supervisory format
			responseASDU = Optional.empty();
		}

		try {
			// if ASDU handler returns ASDU before t2: build response with ASDU
			if (responseASDU.isPresent()) {
				// build response with ASDU
				buildAndSendAPDU(responseASDU.get());
			} else {
				buildAndSendSFormat();
			}
		} catch (IOException | IEC608705104ProtocolException e) {
			logger.error("Error while building and sending response APDU!", e);
		}
	}

	/**
	 * Handles a receive sequence number and cleans up the buffered APDUs or send
	 * them again.
	 */
	private synchronized void handleReceiveSequenceNumberForAck(short receiveSequenceNumber, APDU apdu) {
		if (receiveSequenceNumber > ackState) {
			ackState = receiveSequenceNumber;
			logger.debug("Set ackState to '{}'.", ackState);

			// delete buffered APDUs
			if (apduBuffer_afterReset.isEmpty() || receiveSequenceNumber > sendState) {
				// we only need to deal with apduBuffer map
				apduBuffer.entrySet().removeIf(entry -> entry.getKey() <= ackState);
			} else {
				// we need to deal with both maps: apduBuffer and apduBuffer_afterReset
				apduBuffer.clear();

				apduBuffer_afterReset.entrySet().removeIf(entry -> entry.getKey() <= ackState);

				apduBuffer.putAll(apduBuffer_afterReset);
				apduBuffer_afterReset.clear();
			}
			logger.debug("Anknowledged messages upto {}. Removed APDUs from buffer.", ackState);
		} else if (receiveSequenceNumber < ackState) {
			// send buffered APDUs again
			apduBuffer.entrySet().stream()
					.filter(entry -> entry.getKey() > receiveSequenceNumber && entry.getKey() <= ackState)
					.forEach(entry -> sendAPDUandLogErrors(entry.getValue()));
			if (!apduBuffer_afterReset.isEmpty() && receiveSequenceNumber <= sendState) {
				// we need to deal with both maps: apduBuffer and apduBuffer_afterReset
				apduBuffer_afterReset.entrySet().stream()
						.filter(entry -> entry.getKey() > receiveSequenceNumber && entry.getKey() <= ackState)
						.forEach(entry -> sendAPDUandLogErrors(entry.getValue()));
			}
			logger.warn("No anknowledgement for messages from {} upto {}. Sent APDUs again.", receiveSequenceNumber,
					ackState);
		}
	}

	/**
	 * Handles APDUs in U format (testFR, stopDT or startDT).
	 */
	private void handleUnnumberedControlFunction(UnnumberedControlFunction controlField, APDU apdu)
			throws IEC608705104ProtocolException, IOException {
		// 1. check whether data transfer is started
		if (!ignoreHandshakes && !dtStarted && controlField.getType() != ControlFunctionType.STARTDT) {
			throw new IEC608705104ProtocolException("Received unnumbered control function before startDT!");
		}

		APDU responseAPDU = null;
		switch (controlField.getType()) {
		case STARTDT:
			if (controlField.isActivate() && sendResponses) {
				// send confirm
				responseAPDU = new APDU(new APCI(IControlField.getEncodedSize(),
						new UnnumberedControlFunction(ControlFunctionType.STARTDT, false)));
			} else if (!ignoreTimeouts) {
				// stop t1 timer
				cancelIfPresent(startDtT1);
			}

			// set startDT flag
			dtStarted = true;
			break;
		case STOPDT:
			if (controlField.isActivate() && sendResponses) {
				// send confirm
				responseAPDU = new APDU(new APCI(IControlField.getEncodedSize(),
						new UnnumberedControlFunction(ControlFunctionType.STOPDT, false)));
			} else if (!ignoreTimeouts) {
				// stop t1 timer
				cancelIfPresent(stopDtT1);
			}

			// set startDT flag
			dtStarted = false;
			break;
		case TESTFR:
			if (controlField.isActivate() && sendResponses) {
				// send confirm
				responseAPDU = new APDU(new APCI(IControlField.getEncodedSize(),
						new UnnumberedControlFunction(ControlFunctionType.TESTFR, false)));
			} else if (!ignoreTimeouts) {
				// stop t1 timer
				cancelIfPresent(testT1);
			}
			break;
		default:
			throw new IEC608705104ProtocolException(
					"Received unknowen control function type: " + controlField.getType());
		}

		if (sendResponses && responseAPDU != null) {
			sendAPDUandLogErrors(responseAPDU);
		}

	}

	/*
	 * Sending of APDUs
	 */

	@Override
	public void startDataTransfer() throws IEC608705104ProtocolException, IOException {
		// 1. check whether data transfer is already started
		if (!ignoreHandshakes && dtStarted) {
			logger.warn("Data transfer is already started! Command is ignored.");
			return;
		}

		// 2. build APDU
		APDU apdu = new APDU(new APCI(IControlField.getEncodedSize(),
				new UnnumberedControlFunction(ControlFunctionType.STARTDT, true)));

		// 3. send APDU
		sendAPDUandLogErrors(apdu);

		if (!ignoreTimeouts) {
			// 4. start timer t1 (timeout for response)
			startDtT1 = new Timer("t1 for startDT APDU");
			TimerTask task = createCloseConnectionTimerTask();
			startDtT1.schedule(task, t1Duration);

			// 5. start timer to send test APDU, if test APDUs are wanted and if channel
			// remains unused in that time
			startT3Timer();
		}
	}

	@Override
	public void stopDataTransfer() throws IEC608705104ProtocolException, IOException {
		// 1. check whether data transfer is started
		if (!ignoreHandshakes && !dtStarted) {
			logger.warn("Data transfer is not started! Command is ignored.");
			return;
		}

		// 2. build APDU
		APDU apdu = new APDU(new APCI(IControlField.getEncodedSize(),
				new UnnumberedControlFunction(ControlFunctionType.STOPDT, true)));

		// 3. send APDU
		sendAPDUandLogErrors(apdu);

		if (!ignoreTimeouts) {
			// 4. start timer t1 (timeout for response)
			stopDtT1 = new Timer("t1 for stopDT APDU");
			TimerTask task = createCloseConnectionTimerTask();
			stopDtT1.schedule(task, t1Duration);

			// 5. start timer to send test APDU, if test APDUs are wanted and if channel
			// remains unused in that time
			startT3Timer();
		}
	}

	@Override
	public void buildAndSendAPDU(ASDU asdu) throws IEC608705104ProtocolException, IOException {
		if (!ignoreTimeouts) {
			cancelIfPresent(t3);
		}
		buildAndSendIFormat(asdu);
		if (!ignoreTimeouts) {
			startT3Timer();
		}
	}

	/**
	 * Builds an APDU in I format with an ASDU in it and sends it.
	 */
	private synchronized void buildAndSendIFormat(ASDU asdu) throws IEC608705104ProtocolException, IOException {
		// 1. check whether data transfer is started
		if (!ignoreHandshakes && !dtStarted) {
			throw new IEC608705104ProtocolException("Can not sent ASDU before startDT!");
		}

		APDU apdu;
		synchronized (sendState) {
			synchronized (receiveState) {
				// 2. build APDU
				apdu = new APDU(new APCI(IControlField.getEncodedSize() + asdu.getEncodedSize(),
						new InformationTransfer(sendState, receiveState)), asdu);
			}

			// 3. send APDU
			sendAPDUandLogErrors(apdu);
			apduBuffer.put(sendState, apdu);
			if(sendState == Short.MAX_VALUE) {
				sendState = 0;
				resetSendState = true;
			} else {
				sendState++;
			}
			logger.debug("Set sendState to '{}'.", sendState);

			if (!ignoreTimeouts) {
				// 4. start timer t1 (timeout for response)
				Timer t1 = new Timer("t1 for APDU with send sequence number " + sendState);
				TimerTask task = createCloseConnectionTimerTask();
				t1.schedule(task, t1Duration);
				if(resetSendState) {
					t1PerSendAPDU_afterReset.put(sendState, t1);
				} else {
					t1PerSendAPDU.put(sendState, t1);
				}
			}
		}
	}

	/**
	 * Builds an APDU in S format and sends it.
	 */
	private void buildAndSendSFormat() throws IEC608705104ProtocolException, IOException {
		APDU apdu;
		synchronized (receiveState) {
			apdu = new APDU(new APCI(IControlField.getEncodedSize(), new NumberedSupervisoryFunction(receiveState)));
		}
		sendAPDUandLogErrors(apdu);
	}

	@Override
	public void resetState() {
		receiveState = 0;
		sendState = 0;
		resetSendState = false;
		ackState = 0;
	}

}