/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.util;

import java.util.Calendar;
import java.util.Random;

import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.model.ITimeTag;
import de.uniol.inf.ei.oj104.model.informationelement.AckFileOrSegmentQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.BinaryCounterReading;
import de.uniol.inf.ei.oj104.model.informationelement.BinaryStateInformation;
import de.uniol.inf.ei.oj104.model.informationelement.CauseOfInitialization;
import de.uniol.inf.ei.oj104.model.informationelement.Checksum;
import de.uniol.inf.ei.oj104.model.informationelement.CommandQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.CounterInterrogationCommandQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.CounterInterrogationCommandQualifier.Freeze;
import de.uniol.inf.ei.oj104.model.informationelement.CounterInterrogationCommandRequest;
import de.uniol.inf.ei.oj104.model.informationelement.DoubleCommand;
import de.uniol.inf.ei.oj104.model.informationelement.DoubleCommand.DoubleCommandEnum;
import de.uniol.inf.ei.oj104.model.informationelement.DoublePointInformation;
import de.uniol.inf.ei.oj104.model.informationelement.DoublePointInformation.DoublePointInformationEnum;
import de.uniol.inf.ei.oj104.model.informationelement.FileError;
import de.uniol.inf.ei.oj104.model.informationelement.FileOrSectionLength;
import de.uniol.inf.ei.oj104.model.informationelement.FileReadyQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.FileStatus;
import de.uniol.inf.ei.oj104.model.informationelement.FixedTestBitPattern;
import de.uniol.inf.ei.oj104.model.informationelement.InterrogationQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.LastSectionOrSegmentQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.MeasuredValuesParameterKind;
import de.uniol.inf.ei.oj104.model.informationelement.MeasuredValuesParameterQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.NameOfFile;
import de.uniol.inf.ei.oj104.model.informationelement.NameOfSection;
import de.uniol.inf.ei.oj104.model.informationelement.NormalizedValue;
import de.uniol.inf.ei.oj104.model.informationelement.PEOutputCircuitInformation;
import de.uniol.inf.ei.oj104.model.informationelement.PESingleEvent;
import de.uniol.inf.ei.oj104.model.informationelement.PESingleEvent.EventState;
import de.uniol.inf.ei.oj104.model.informationelement.PEStartEvent;
import de.uniol.inf.ei.oj104.model.informationelement.ParameterActivationQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptor;
import de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorForPE;
import de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorWithOV;
import de.uniol.inf.ei.oj104.model.informationelement.RegulatingStepCommand;
import de.uniol.inf.ei.oj104.model.informationelement.RegulatingStepCommand.RegulatingStepCommandState;
import de.uniol.inf.ei.oj104.model.informationelement.ResetProcessCommandQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.ScaledValue;
import de.uniol.inf.ei.oj104.model.informationelement.SectionReadyQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.Segment;
import de.uniol.inf.ei.oj104.model.informationelement.SelectAndCallQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.SequenceNotation;
import de.uniol.inf.ei.oj104.model.informationelement.SetPointCommandQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.ShortFloatingPointNumber;
import de.uniol.inf.ei.oj104.model.informationelement.ShortValue;
import de.uniol.inf.ei.oj104.model.informationelement.SingleCommand;
import de.uniol.inf.ei.oj104.model.informationelement.SinglePointInformation;
import de.uniol.inf.ei.oj104.model.informationelement.StatusAndChangeDetection;
import de.uniol.inf.ei.oj104.model.informationelement.ValueWithTransientStateIndication;
import de.uniol.inf.ei.oj104.model.timetag.SevenOctetBinaryTime;
import de.uniol.inf.ei.oj104.model.timetag.ThreeOctetBinaryTime;
import de.uniol.inf.ei.oj104.model.timetag.TwoOctetBinaryTime;

/**
 * Factory to create information elements with random but valid content for
 * testing.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class RandomInformationElementCreator {

	/**
	 * Creates a {@link AckFileOrSegmentQualifier}.
	 * 
	 * @return a {@link AckFileOrSegmentQualifier} with random values.
	 */
	public static IInformationElement createAckFileOrSegmentQualifierElement() {
		Random random = new Random();
		int value = random.nextInt(16);
		int errorValue = random.nextInt(16);
		FileError error = new FileError(errorValue);
		return new AckFileOrSegmentQualifier(value, error);
	}

	/**
	 * Creates an {@link BinaryCounterReading}.
	 * 
	 * @return An {@link BinaryCounterReading} with random values.
	 */
	public static IInformationElement createBinaryCounterReadingElement() {
		Random random = new Random();
		int value = random.nextInt();
		byte sequenceNumber = (byte) random.nextInt(32);
		boolean carry = random.nextBoolean();
		boolean counterAdjusted = random.nextBoolean();
		boolean invalid = random.nextBoolean();
		SequenceNotation sequenceNotation = new SequenceNotation(sequenceNumber, carry, counterAdjusted, invalid);
		return new BinaryCounterReading(value, sequenceNotation);
	}

	/**
	 * Creates a {@link BinaryStateInformation}.
	 * 
	 * @return A {@link BinaryStateInformation} with random values.
	 */
	public static IInformationElement createBinaryStateInformationElement() {
		Random random = new Random();
		int value = random.nextInt();
		return new BinaryStateInformation(value);
	}

	/**
	 * Creates a {@link CauseOfInitialization}.
	 * 
	 * @return A {@link CauseOfInitialization} with random values.
	 */
	public static IInformationElement createCauseOfInitializationElement() {
		Random random = new Random();
		int value = random.nextInt(127);
		boolean changedLocalParameters = random.nextBoolean();
		return new CauseOfInitialization(value, changedLocalParameters);
	}

	/**
	 * Creates a {@link Checksum}.
	 * 
	 * @return A {@link Checksum} with random values.
	 */
	public static IInformationElement createChecksumElement() {
		Random random = new Random();
		byte[] value = new byte[1];
		random.nextBytes(value);
		return new Checksum(value[0]);
	}

	/**
	 * Creates a {@link CounterInterrogationCommand}.
	 * 
	 * @return A {@link CounterInterrogationCommand} with random values.
	 */
	public static IInformationElement createCounterInterrogationCommandQualifierElement() {
		Random random = new Random();
		Freeze freeze = Freeze.values()[random.nextInt(4)];
		int value = random.nextInt(64);
		CounterInterrogationCommandRequest request = new CounterInterrogationCommandRequest(value);
		return new CounterInterrogationCommandQualifier(request, freeze);
	}

	/**
	 * Creates a {@link DoubleCommand}.
	 * 
	 * @return A {@link DoubleCommand} with random values.
	 */
	public static IInformationElement createDoubleCommandElement() {
		Random random = new Random();
		DoubleCommandEnum command = DoubleCommandEnum.values()[random.nextInt(4)];
		int value = random.nextInt(32);
		boolean select = random.nextBoolean();
		CommandQualifier qualifier = new CommandQualifier(value, select);
		return new DoubleCommand(command, qualifier);
	}

	/**
	 * Creates a {@link DoublePointInformation}.
	 * 
	 * @return A {@link DoublePointInformation} with random values.
	 */
	public static IInformationElement createDoublePointInformationElement() {
		Random random = new Random();
		DoublePointInformationEnum value = DoublePointInformationEnum.values()[random.nextInt(4)];
		boolean blocked = random.nextBoolean();
		boolean substituted = random.nextBoolean();
		boolean notTopical = random.nextBoolean();
		boolean invalid = random.nextBoolean();
		QualityDescriptor qds = new QualityDescriptor(blocked, substituted, notTopical, invalid);
		return new DoublePointInformation(value, qds);
	}

	/**
	 * Creates a {@link FileOrSectionLength}.
	 * 
	 * @return A {@link FileOrSectionLength} with random values.
	 */
	public static IInformationElement createFileOrSectionLengthElement() {
		Random random = new Random();
		int value = random.nextInt(16777216);
		return new FileOrSectionLength(value);
	}

	/**
	 * Creates a {@link FileReadyQualifier}.
	 * 
	 * @return A {@link FileReadyQualifier} with random values.
	 */
	public static IInformationElement createFileReadyQualifierElement() {
		Random random = new Random();
		int value = random.nextInt(128);
		boolean negativeConfirm = random.nextBoolean();
		return new FileReadyQualifier(value, negativeConfirm);
	}

	/**
	 * Creates a {@link FileStatus}.
	 * 
	 * @return A {@link FileStatus} with random values.
	 */
	public static IInformationElement createFileStatusElement() {
		Random random = new Random();
		int value = random.nextInt(32);
		boolean lastFile = random.nextBoolean();
		boolean nameDefinesSubDir = random.nextBoolean();
		boolean activeTransfer = random.nextBoolean();
		return new FileStatus(value, lastFile, nameDefinesSubDir, activeTransfer);
	}

	/**
	 * Creates a {@link FixedTestBitPattern}.
	 * 
	 * @return a {@link FixedTestBitPattern} with random values.
	 */
	public static IInformationElement createFixedTestBitPatternElement() {
		Random random = new Random();
		short value = (short) random.nextInt(Short.MAX_VALUE);
		return new FixedTestBitPattern(value);
	}

	/**
	 * Creates a {@link InterrogationQualifier}.
	 * 
	 * @return a {@link InterrogationQualifier} with random values.
	 */
	public static IInformationElement createInterrogationQualifierElement() {
		Random random = new Random();
		int value = random.nextInt(256);
		return new InterrogationQualifier(value);
	}

	/**
	 * Creates a {@link LastSectionOrSegmentQualifier}.
	 * 
	 * @return a {@link LastSectionOrSegmentQualifier} with random values.
	 */
	public static IInformationElement createLastSectionOrSegmentQualifierElement() {
		Random random = new Random();
		int value = random.nextInt(256);
		return new LastSectionOrSegmentQualifier(value);
	}

	/**
	 * Creates a {@link MeasuredValuesParameterQualifier}.
	 * 
	 * @return a {@link MeasuredValuesParameterQualifier} with random values.
	 */
	public static IInformationElement createMeasuredValuesParameterQualifierElement() {
		Random random = new Random();
		int value = random.nextInt(64);
		MeasuredValuesParameterKind kindOfParameter = new MeasuredValuesParameterKind(value);
		boolean localParameterChange = random.nextBoolean();
		boolean notInOperation = random.nextBoolean();
		return new MeasuredValuesParameterQualifier(kindOfParameter, localParameterChange, notInOperation);
	}

	/**
	 * Creates a {@link NameOfFile}.
	 * 
	 * @return a {@link NameOfFile} with random values.
	 */
	public static IInformationElement createNameOfFileElement() {
		Random random = new Random();
		short value = (short) random.nextInt(Short.MAX_VALUE);
		return new NameOfFile(value);
	}

	/**
	 * Creates a {@link NameOfSection}.
	 * 
	 * @return a {@link NameOfSection} with random values.
	 */
	public static IInformationElement createNameOfSectionElement() {
		Random random = new Random();
		int value = random.nextInt(256);
		return new NameOfSection((byte) value);
	}

	/**
	 * Creates a {@link NormalizedValue}.
	 * 
	 * @return a {@link NormalizedValue} with random values.
	 */
	public static IInformationElement createNormalizedValueElement() {
		Random random = new Random();
		short value = (short) random.nextInt(Short.MAX_VALUE);
		return new NormalizedValue(value);
	}

	/**
	 * Creates a {@link ParameterActivationQualifier}.
	 * 
	 * @return a {@link ParameterActivationQualifier} with random values.
	 */
	public static IInformationElement createParameterActivationQualifierElement() {
		Random random = new Random();
		int value = random.nextInt(256);
		return new ParameterActivationQualifier(value);
	}

	/**
	 * Creates a {@link PEOutputCircuitInformation}.
	 * 
	 * @return a {@link PEOutputCircuitInformation} with random values.
	 */
	public static IInformationElement createPEOutputCircuitInformationElement() {
		Random random = new Random();
		boolean general = random.nextBoolean();
		boolean phaseL1 = random.nextBoolean();
		boolean phaseL2 = random.nextBoolean();
		boolean phaseL3 = random.nextBoolean();
		return new PEOutputCircuitInformation(general, phaseL1, phaseL2, phaseL3);
	}

	/**
	 * Creates a {@link PESingleEvent}.
	 * 
	 * @return a {@link PESingleEvent} with random values.
	 */
	public static IInformationElement createPESingleEventElement() {
		Random random = new Random();
		EventState eventState = EventState.values()[random.nextInt(EventState.values().length)];
		return new PESingleEvent(eventState, (QualityDescriptorForPE) createQualityDescriptorForPEElement());
	}

	/**
	 * Creates a {@link PEStartEvent}.
	 * 
	 * @return a {@link PEStartEvent} with random values.
	 */
	public static IInformationElement createPEStartEventElement() {
		Random random = new Random();
		boolean general = random.nextBoolean();
		boolean phaseL1 = random.nextBoolean();
		boolean phaseL2 = random.nextBoolean();
		boolean phaseL3 = random.nextBoolean();
		boolean earthCurrent = random.nextBoolean();
		boolean startInReverseDirection = random.nextBoolean();
		return new PEStartEvent(general, phaseL1, phaseL2, phaseL3, earthCurrent, startInReverseDirection);
	}

	/**
	 * Creates a {@link QualityDescriptorWithOV}.
	 * 
	 * @return a {@link QualityDescriptorWithOV} with random values.
	 */
	public static IInformationElement createQualityDescriptorWithOVElement() {
		Random random = new Random();
		boolean blocked = random.nextBoolean();
		boolean substituted = random.nextBoolean();
		boolean notTopical = random.nextBoolean();
		boolean invalid = random.nextBoolean();
		boolean overflow = random.nextBoolean();
		return new QualityDescriptorWithOV(blocked, substituted, notTopical, invalid, overflow);
	}

	/**
	 * Creates a {@link QualityDescriptorForPE}.
	 * 
	 * @return a {@link QualityDescriptorForPE} with random values.
	 */
	public static IInformationElement createQualityDescriptorForPEElement() {
		Random random = new Random();
		boolean blocked = random.nextBoolean();
		boolean substituted = random.nextBoolean();
		boolean notTopical = random.nextBoolean();
		boolean invalid = random.nextBoolean();
		boolean elapsedTimeInvalid = random.nextBoolean();
		return new QualityDescriptorForPE(blocked, substituted, notTopical, invalid, elapsedTimeInvalid);
	}

	/**
	 * Creates a {@link RegulatingStepCommand}.
	 * 
	 * @return a {@link RegulatingStepCommand} with random values.
	 */
	public static IInformationElement createRegulatingStepCommandElement() {
		Random random = new Random();
		int stateCode = random.nextInt(RegulatingStepCommandState.values().length);
		int value = random.nextInt(32);
		boolean select = random.nextBoolean();
		CommandQualifier qualifier = new CommandQualifier(value, select);
		return new RegulatingStepCommand(stateCode, qualifier);
	}

	/**
	 * Creates a {@link ResetProcessCommandQualifier}.
	 * 
	 * @return a {@link ResetProcessCommandQualifier} with random values.
	 */
	public static IInformationElement createResetProcessCommandQualifierElement() {
		Random random = new Random();
		int value = random.nextInt(256);
		return new ResetProcessCommandQualifier(value);
	}

	/**
	 * Creates a {@link ScaledValue}.
	 * 
	 * @return a {@link ScaledValue} with random values.
	 */
	public static IInformationElement createScaledValueElement() {
		Random random = new Random();
		short value = (short) random.nextInt(Short.MAX_VALUE);
		return new ScaledValue(value);
	}

	/**
	 * Creates a {@link SectionReadyQualifier}.
	 * 
	 * @return a {@link SectionReadyQualifier} with random values.
	 */
	public static IInformationElement createSectionReadyQualifierElement() {
		Random random = new Random();
		int value = random.nextInt(128);
		boolean sectionNotReady = random.nextBoolean();
		return new SectionReadyQualifier(value, sectionNotReady);
	}

	/**
	 * Creates a {@link Segment}.
	 * 
	 * @return a {@link Segment} with random values.
	 */
	public static IInformationElement createSegmentElement() {
		Random random = new Random();
		ShortValue length = new ShortValue((short) (random.nextInt(255) + 1));
		byte[] value = new byte[length.getValue()];
		random.nextBytes(value);
		return new Segment(length, value);
	}

	/**
	 * Creates a {@link SelectAndCallQualifier}.
	 * 
	 * @return a {@link SelectAndCallQualifier} with random values.
	 */
	public static IInformationElement createSelectAndCallQualifierElement() {
		Random random = new Random();
		int value = random.nextInt(16);
		int errorValue = random.nextInt(16);
		FileError error = new FileError(errorValue);
		return new SelectAndCallQualifier(value, error);
	}

	/**
	 * Creates a {@link SetPointCommandQualifier}.
	 * 
	 * @return a {@link SetPointCommandQualifier} with random values.
	 */
	public static IInformationElement createSetPointCommandQualifierElement() {
		Random random = new Random();
		int value = random.nextInt(127);
		boolean select = random.nextBoolean();
		return new SetPointCommandQualifier(value, select);
	}

	/**
	 * Creates a {@link SevenOctetBinaryTime}.
	 * 
	 * @return a {@link SevenOctetBinaryTime} with the system time stamp.
	 */
	public static IInformationElement createSevenOctetBinaryTimeElement() {
		return createTimeTag(SevenOctetBinaryTime.class);
	}

	/**
	 * Creates a {@link ShortFloatingPointNumber}.
	 * 
	 * @return a {@link ShortFloatingPointNumber} with random values.
	 */
	public static IInformationElement createShortFloatingPointNumberElement() {
		Random random = new Random();
		float value = random.nextFloat();
		return new ShortFloatingPointNumber(value);
	}

	/**
	 * Creates a {@link SingleCommand}.
	 * 
	 * @return a {@link SingleCommand} with random values.
	 */
	public static IInformationElement createSingleCommandElement() {
		Random random = new Random();
		boolean on = random.nextBoolean();
		int value = random.nextInt(32);
		boolean select = random.nextBoolean();
		CommandQualifier qualifier = new CommandQualifier(value, select);
		return new SingleCommand(on, qualifier);
	}

	/**
	 * Creates a {@link SinglePointInformation}.
	 * 
	 * @return a {@link SinglePointInformation} with random values.
	 */
	public static IInformationElement createSinglePointInformationElement() {
		Random random = new Random();
		boolean on = random.nextBoolean();
		boolean blocked = random.nextBoolean();
		boolean substituted = random.nextBoolean();
		boolean notTopical = random.nextBoolean();
		boolean invalid = random.nextBoolean();
		QualityDescriptor qds = new QualityDescriptor(blocked, substituted, notTopical, invalid);
		return new SinglePointInformation(on, qds);
	}

	/**
	 * Creates a {@link StatusAndChangeDetection}.
	 * 
	 * @return a {@link StatusAndChangeDetection} with random values.
	 */
	public static IInformationElement createStatusAndChangeDetectionElement() {
		Random random = new Random();
		short value1 = (short) random.nextInt(Short.MAX_VALUE);
		ShortValue status = new ShortValue(value1);
		short value2 = (short) random.nextInt(Short.MAX_VALUE);
		ShortValue changeDetection = new ShortValue(value2);
		return new StatusAndChangeDetection(status, changeDetection);
	}

	/**
	 * Creates a time tag.
	 * 
	 * @param timeTagClass The time tag class to use
	 * @return a time tag with the current system time stamp.
	 */
	public static ITimeTag createTimeTag(Class<? extends ITimeTag> timeTagClass) {
		Calendar cal = Calendar.getInstance();
		if (timeTagClass.equals(TwoOctetBinaryTime.class)) {
			return new TwoOctetBinaryTime(cal.get(Calendar.MILLISECOND), cal.get(Calendar.SECOND));
		} else if (timeTagClass.equals(ThreeOctetBinaryTime.class)) {
			return new ThreeOctetBinaryTime(cal.get(Calendar.MILLISECOND), cal.get(Calendar.SECOND),
					cal.get(Calendar.MINUTE), false, false);
		} else {
			return new SevenOctetBinaryTime(cal.get(Calendar.MILLISECOND), cal.get(Calendar.SECOND),
					cal.get(Calendar.MINUTE), false, false, cal.get(Calendar.HOUR_OF_DAY),
					cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.DAY_OF_WEEK), cal.get(Calendar.MONTH),
					cal.get(Calendar.YEAR) % 100);
		}
	}

	/**
	 * Creates a {@link TwoOctetBinaryTime}.
	 * 
	 * @return a {@link TwoOctetBinaryTime} with the current system time stamp.
	 */
	public static IInformationElement createTwoOctetBinaryTimeElement() {
		return createTimeTag(TwoOctetBinaryTime.class);
	}

	/**
	 * Creates a {@link ValueWithTransientStateIndication}.
	 * 
	 * @return a {@link ValueWithTransientStateIndication} with random values.
	 */
	public static IInformationElement createValueWithTransientStateIndicationElement() {
		Random random = new Random();
		byte[] value = new byte[1];
		random.nextBytes(value);
		boolean trans = random.nextBoolean();
		return new ValueWithTransientStateIndication((byte) (value[0] & 0x7f), trans);
	}

}