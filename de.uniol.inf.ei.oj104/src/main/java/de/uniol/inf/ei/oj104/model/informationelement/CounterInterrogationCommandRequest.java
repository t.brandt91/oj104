/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * A counter interrogation command request to be used in
 * {@link IInformationElement}s:
 * <ul>
 * <li>0 = not used</li>
 * <li>-1, 1-4 = request counter group 1, 2, or 4. -1 for no group</li>
 * <li>5 = general request counter</li>
 * <li>6-31 = in compatible range</li>
 * <li>32 - 63 = in private range</li>
 * </ul>
 * 
 * @author Michael
 *
 */
@JsonIgnoreProperties({ "notUsed", "requestCounterGroup", "generalRequestCounter", "inCompatibleRange",
		"inPrivateRange" })
public class CounterInterrogationCommandRequest implements Serializable {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = 6895168620878676761L;

	/**
	 * The request:
	 * <ul>
	 * <li>0 = not used</li>
	 * <li>-1, 1-4 = request counter group 1, 2, or 4. -1 for no group</li>
	 * <li>5 = general request counter</li>
	 * <li>6-31 = in compatible range</li>
	 * <li>32 - 63 = in private range</li>
	 * </ul>
	 */
	private int value;

	/**
	 * Returns the request.
	 * 
	 * @return An integer in:
	 *         <ul>
	 *         <li>0 = not used</li>
	 *         <li>-1, 1-4 = request counter group 1, 2, or 4. -1 for no group</li>
	 *         <li>5 = general request counter</li>
	 *         <li>6-31 = in compatible range</li>
	 *         <li>32 - 63 = in private range</li>
	 *         </ul>
	 */
	public int getValue() {
		return value;
	}

	/**
	 * Sets the request.
	 * 
	 * @param value An integer in:
	 *              <ul>
	 *              <li>0 = not used</li>
	 *              <li>-1, 1-4 = request counter group 1, 2, or 4. -1 for no
	 *              group</li>
	 *              <li>5 = general request counter</li>
	 *              <li>6-31 = in compatible range</li>
	 *              <li>32 - 63 = in private range</li>
	 *              </ul>
	 */
	public void setValue(int value) {
		this.value = value;
	}

	/**
	 * Checks, whether the request is not used.
	 * 
	 * @return {@link #getValue()} == 0.
	 */
	public boolean isNotUsed() {
		return value == 0;
	}

	/**
	 * Checks, for which counter group the request is (groups 1-4, -1 for no group).
	 * 
	 * @return {@link #getValue()} == -1 || ({@link #getValue()} >= 1 &&
	 *         {@link #getValue()} <= 4).
	 */
	public int getRequestCounterGroup() {
		return (value >= 1 && value <= 4) ? value : -1;
	}

	/**
	 * Checks, whether the request is for a general request.
	 * 
	 * @return {@link #getValue()} == 5.
	 */
	public boolean isGeneralRequestCounter() {
		return value == 5;
	}

	/**
	 * Checks, whether the request is in compatible range.
	 * 
	 * @return {@link #getValue()} >= 6 && {@link #getValue()} <= 31.
	 */
	public boolean isInCompatibleRange() {
		return value >= 6 && value <= 31;
	}

	/**
	 * Checks, whether the request is in private range.
	 * 
	 * @return {@link #getValue()} >= 32 && {@link #getValue()} <= 63.
	 */
	public boolean isInPrivateRange() {
		return value >= 32 && value <= 63;
	}

	/**
	 * Empty default constructor.
	 */
	public CounterInterrogationCommandRequest() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param value The request, an integer in:
	 *              <ul>
	 *              <li>0 = not used</li>
	 *              <li>-1, 1-4 = request counter group 1, 2, or 4. -1 for no
	 *              group</li>
	 *              <li>5 = general request counter</li>
	 *              <li>6-31 = in compatible range</li>
	 *              <li>32 - 63 = in private range</li>
	 *              </ul>
	 */
	public CounterInterrogationCommandRequest(int value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + value;
		return result;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CounterInterrogationCommandRequest other = (CounterInterrogationCommandRequest) obj;
		if (value != other.value) {
			return false;
		}
		return true;
	}

}