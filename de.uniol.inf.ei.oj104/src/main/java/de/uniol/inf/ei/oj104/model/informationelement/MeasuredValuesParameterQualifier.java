/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import java.util.Arrays;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * Information element that contains a qualifier for a parameter of a measured
 * value.
 * <p>
 * Abbreviation: QPM
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class MeasuredValuesParameterQualifier implements IInformationElement {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = 3906390364344587537L;

	/**
	 * The information, what kind a parameter of a measured value is:
	 * <ul>
	 * <li>not used</li>
	 * <li>threshold value</li>
	 * <li>smoothing factor</li>
	 * <li>low limit</li>
	 * <li>high limit</li>
	 * <li>in compatible range</li>
	 * <li>in private range</li>
	 * </ul>
	 */
	private MeasuredValuesParameterKind kindOfParameter;

	/**
	 * True for local parameter changes.
	 */
	private boolean localParameterChange;

	/**
	 * True, if the parameter is not in operation.
	 */
	private boolean notInOperation;

	/**
	 * Returns the information, what kind a parameter of a measured value is.
	 * 
	 * @return One of the following:
	 *         <ul>
	 *         <li>not used</li>
	 *         <li>threshold value</li>
	 *         <li>smoothing factor</li>
	 *         <li>low limit</li>
	 *         <li>high limit</li>
	 *         <li>in compatible range</li>
	 *         <li>in private range</li>
	 *         </ul>
	 */
	public MeasuredValuesParameterKind getKindOfParameter() {
		return kindOfParameter;
	}

	/**
	 * Sets the information, what kind a parameter of a measured value is.
	 * 
	 * @param kindOfParameter One of the following:
	 *                        <ul>
	 *                        <li>not used</li>
	 *                        <li>threshold value</li>
	 *                        <li>smoothing factor</li>
	 *                        <li>low limit</li>
	 *                        <li>high limit</li>
	 *                        <li>in compatible range</li>
	 *                        <li>in private range</li>
	 *                        </ul>
	 */
	public void setKindOfParameter(MeasuredValuesParameterKind kindOfParameter) {
		this.kindOfParameter = kindOfParameter;
	}

	/**
	 * Checks, whether the parameter change is local.
	 * 
	 * @return True for local parameter changes.
	 */
	public boolean isLocalParameterChange() {
		return localParameterChange;
	}

	/**
	 * Sets, whether the parameter change is local.
	 * 
	 * @param localParameterChange True for local parameter changes.
	 */
	public void setLocalParameterChange(boolean localParameterChange) {
		this.localParameterChange = localParameterChange;
	}

	/**
	 * Checks, whether the parameter is not in operation.
	 * 
	 * @return True, if the parameter is not in operation.
	 */
	public boolean isNotInOperation() {
		return notInOperation;
	}

	/**
	 * Sets, whether the parameter is not in operation.
	 * 
	 * @param notInOperation True, if the parameter is not in operation.
	 */
	public void setNotInOperation(boolean notInOperation) {
		this.notInOperation = notInOperation;
	}

	/**
	 * Empty default constructor.
	 */
	public MeasuredValuesParameterQualifier() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param kindOfParameter      One of the following:
	 *                             <ul>
	 *                             <li>not used</li>
	 *                             <li>threshold value</li>
	 *                             <li>smoothing factor</li>
	 *                             <li>low limit</li>
	 *                             <li>high limit</li>
	 *                             <li>in compatible range</li>
	 *                             <li>in private range</li>
	 *                             </ul>
	 * @param localParameterChange True for local parameter changes.
	 * @param notInOperation       True, if the parameter is not in operation.
	 */
	public MeasuredValuesParameterQualifier(MeasuredValuesParameterKind kindOfParameter, boolean localParameterChange,
			boolean notInOperation) {
		this.kindOfParameter = kindOfParameter;
		this.localParameterChange = localParameterChange;
		this.notInOperation = notInOperation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((kindOfParameter == null) ? 0 : kindOfParameter.hashCode());
		result = prime * result + (localParameterChange ? 1231 : 1237);
		result = prime * result + (notInOperation ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		MeasuredValuesParameterQualifier other = (MeasuredValuesParameterQualifier) obj;
		if (kindOfParameter == null) {
			if (other.kindOfParameter != null) {
				return false;
			}
		} else if (!kindOfParameter.equals(other.kindOfParameter)) {
			return false;
		}
		if (localParameterChange != other.localParameterChange) {
			return false;
		}
		if (notInOperation != other.notInOperation) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		}

		kindOfParameter = new MeasuredValuesParameterKind(bytes[0] & 0x3f);
		localParameterChange = (bytes[0] & 0x40) == 0x40;
		notInOperation = (bytes[0] & 0x80) == 0x80;
		return Arrays.copyOfRange(bytes, getEncodedSize(), bytes.length);
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		return new byte[] { (byte) (kindOfParameter.getValue() | (localParameterChange ? 0x40 : 0x00)
				| (notInOperation ? 0x80 : 0x00)) };
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return 1;
	}

}