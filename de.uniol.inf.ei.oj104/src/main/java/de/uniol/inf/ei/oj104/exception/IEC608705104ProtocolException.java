/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.exception;

import de.uniol.inf.ei.oj104.model.IByteEncodedEntity;

/**
 * Exception class for violations of the protocol itself, e.g. incorrect send or
 * receive sequence no.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class IEC608705104ProtocolException extends Exception {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = 6774433083636018011L;

	/**
	 * Calls {@link Exception#Exception(String)}.
	 */
	public IEC608705104ProtocolException(String string) {
		super(string);
	}

	/**
	 * Calls {@link Exception#Exception(String)} with the error message and the
	 * preamble for which class/object the error occured.
	 */
	public IEC608705104ProtocolException(Class<? extends IByteEncodedEntity> entityClass, String message) {
		super("Error in/with " + entityClass.getSimpleName() + ": " + message);
	}

	/**
	 * Calls {@link Exception#Exception(String, Throwable))} with the error message
	 * and the preamble for which class/object the error occured.
	 */
	public IEC608705104ProtocolException(Class<? extends IByteEncodedEntity> entityClass, String message,
			Throwable cause) {
		super("Error in/with " + entityClass.getSimpleName() + ": " + message, cause);
	}

	/**
	 * Calls {@link Exception#Exception(String)} with the error message that the
	 * actual amount of bytes does not match the expected one and the preamble for
	 * which class/object the error occured.
	 */
	public IEC608705104ProtocolException(Class<? extends IByteEncodedEntity> entityClass, int requiredBytes,
			int actualBytes) {
		super("Error in/with " + entityClass.getSimpleName() + ": " + entityClass.getSimpleName()
				+ " requires at least " + requiredBytes + " bytes but has " + actualBytes + " bytes!");
	}

}