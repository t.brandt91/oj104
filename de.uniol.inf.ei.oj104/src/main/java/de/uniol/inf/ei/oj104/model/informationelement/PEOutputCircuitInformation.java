/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * Information element that contains information about protection equipment of
 * output circuits.
 * <p>
 * Abbreviation: OCI
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class PEOutputCircuitInformation extends PEInformation implements IInformationElement {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = -1729247033146171706L;

	/**
	 * Empty default constructor.
	 */
	public PEOutputCircuitInformation() {
		super();
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param general True for general protection.
	 * @param phaseL1 True for protection of phase L1.
	 * @param phaseL2 True for protection of phase L2.
	 * @param phaseL3 True for protection of phase L3.
	 */
	public PEOutputCircuitInformation(boolean general, boolean phaseL1, boolean phaseL2, boolean phaseL3) {
		super(general, phaseL1, phaseL2, phaseL3);
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

}