/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.ArrayUtils;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.util.JsonParser;
import de.uniol.inf.ei.oj104.util.SystemProperties;

/**
 * Class for sequence information objects. A single information object has an
 * ordered list of information element sequences, a total amount of contained
 * information elements, and an information object address (IOA).
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class SequenceInformationObject extends AbstractInformationElementSequence implements IInformationObject {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = 658015138600650743L;

	/**
	 * The number of bytes for an encoded IOA, read from properties file
	 * <code>SystemProperties.xml</code>.
	 */
	private static final int ioaEncodedSize = Integer
			.parseInt(SystemProperties.getProperties().getProperty("ioa.bytesize", "1"));

	/**
	 * The overall, across the sequences, amount of information elements in this
	 * sequence information object.
	 */
	private int numberOfInformationElements;

	/**
	 * The IOA of the information object is an integer of two or three bytes (system
	 * property dependent). 0 := irrelevant
	 */
	private int informationObjectAddress;

	/**
	 * An ordered list of the information element sequences, contained in the
	 * object.
	 */
	private List<InformationElementSequence> informationElementSequences;

	/**
	 * Return the amount of information elements in this sequence information
	 * object.
	 * 
	 * @return The overall, across the sequences, amount of information elements in
	 *         this sequence information object.
	 */
	public int getNumberOfInformationElements() {
		return numberOfInformationElements;
	}

	/**
	 * Sets the amount of information elements in this sequence information object.
	 * 
	 * @param numberOfInformationElements The overall, across the sequences, amount
	 *                                    of information elements in this sequence
	 *                                    information object.
	 */
	public void setNumberOfInformationElements(int numberOfInformationElements) {
		this.numberOfInformationElements = numberOfInformationElements;
	}

	@Override
	public int getInformationObjectAddress() {
		return informationObjectAddress;
	}

	/**
	 * Sets the IOA of the information object.
	 * 
	 * @param informationObjectAddress An integer of two or three bytes (system
	 *                                 property dependent). 0 := irrelevant
	 */
	public void setInformationObjectAddress(int informationObjectAddress) {
		this.informationObjectAddress = informationObjectAddress;
	}

	/**
	 * Returns the information element sequences, contained in the object.
	 * 
	 * @return An ordered list of the information element sequences, contained in
	 *         the object.
	 */
	public List<InformationElementSequence> getInformationElementSequences() {
		return informationElementSequences;
	}

	/**
	 * Sets the information element sequences, contained in the object.
	 * 
	 * @param informationElementSequences An ordered list of the information element
	 *                                    sequences, contained in the object.
	 */
	public void setInformationElementSequences(List<InformationElementSequence> informationElementSequences) {
		this.informationElementSequences = informationElementSequences;
	}

	/**
	 * Empty default constructor.
	 */
	public SequenceInformationObject() {
	}

	/**
	 * Creates an empty information object.
	 * 
	 * @param informationElementClasses   An ordered list of the classes of the
	 *                                    information elements, contained in the
	 *                                    sequence.
	 * @param timeTagClass                The class of the time tag object of the
	 *                                    sequence or <code>null</code>, if the
	 *                                    sequence has no time tag.
	 * @param numberOfInformationElements The overall, across the sequences, amount
	 *                                    of information elements in this sequence
	 *                                    information object.
	 */
	// numberOfInformationElements needs to be integer. Otherwise this constructor
	// cannot be accessed via reflection
	public SequenceInformationObject(List<Class<? extends IInformationElement>> informationElementClasses,
			Class<? extends ITimeTag> timeTagClass, Integer numberOfInformationElements) {
		super(informationElementClasses, timeTagClass);
		this.numberOfInformationElements = numberOfInformationElements;
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param informationElementClasses   An ordered list of the classes of the
	 *                                    information elements, contained in the
	 *                                    sequence.
	 * @param timeTagClass                The class of the time tag object of the
	 *                                    sequence or <code>null</code>, if the
	 *                                    sequence has no time tag.
	 * @param numberOfInformationElements The overall, across the sequences, amount
	 *                                    of information elements in this sequence
	 *                                    information object.
	 * @param informationObjectAddress    An integer of two or three bytes (system
	 *                                    property dependent). 0 := irrelevant
	 * @param informationElementSequences An ordered list of the information element
	 *                                    sequences, contained in the object.
	 */
	public SequenceInformationObject(List<Class<? extends IInformationElement>> informationElementClasses,
			Class<? extends ITimeTag> timeTagClass, int numberOfInformationElements, int informationObjectAddress,
			List<InformationElementSequence> informationElementSequences) {
		this(informationElementClasses, timeTagClass, numberOfInformationElements);
		this.informationObjectAddress = informationObjectAddress;
		this.informationElementSequences = informationElementSequences;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((informationElementSequences == null) ? 0 : informationElementSequences.hashCode());
		result = prime * result + informationObjectAddress;
		result = prime * result + numberOfInformationElements;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SequenceInformationObject other = (SequenceInformationObject) obj;
		if (informationElementSequences == null) {
			if (other.informationElementSequences != null) {
				return false;
			}
		} else if (!informationElementSequences.equals(other.informationElementSequences)) {
			return false;
		}
		if (informationObjectAddress != other.informationObjectAddress) {
			return false;
		}
		if (numberOfInformationElements != other.numberOfInformationElements) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < ioaEncodedSize) {
			throw new IEC608705104ProtocolException(getClass(), ioaEncodedSize, bytes.length);
		}

		informationObjectAddress = (bytes[0] & 0xff);
		for (int i = 1; i < ioaEncodedSize; i++) {
			informationObjectAddress |= (bytes[i] & 0xff) << (8 * i);
		}
		byte[] remainingBytes = Arrays.copyOfRange(bytes, ioaEncodedSize, bytes.length);

		informationElementSequences = new ArrayList<>(numberOfInformationElements);
		for (int i = 0; i < numberOfInformationElements; i++) {
			InformationElementSequence sequence = new InformationElementSequence(getInformationElementClasses(),
					getTimeTagClass().orElseGet(() -> null));
			remainingBytes = sequence.fromBytes(remainingBytes);
			informationElementSequences.add(sequence);
		}
		return remainingBytes;
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		byte[] ioaBytes = new byte[ioaEncodedSize];
		ioaBytes[0] = (byte) informationObjectAddress;
		for (int i = 1; i < ioaEncodedSize; i++) {
			ioaBytes[i] = (byte) (informationObjectAddress >> 8 * i);
		}

		byte[] bytes = ioaBytes;
		for (InformationElementSequence sequence : informationElementSequences) {
			byte[] sequenceBytes = sequence.toBytes();
			bytes = ArrayUtils.addAll(bytes, sequenceBytes);
		}
		return bytes;
	}

}