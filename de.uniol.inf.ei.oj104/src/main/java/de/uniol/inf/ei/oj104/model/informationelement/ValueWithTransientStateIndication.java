/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import java.util.Arrays;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * Information element that contains a value with transient state indication.
 * <p>
 * Abbreviation: VTI
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class ValueWithTransientStateIndication implements IInformationElement {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = 1080247025927022500L;

	/**
	 * The value.
	 */
	private short value;

	/**
	 * True for a transient state indication.
	 */
	private boolean trans;

	/**
	 * Returns the value.
	 * @return A short.
	 */
	public short getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 * @param value A short.
	 */
	public void setValue(short value) {
		this.value = value;
	}

	/**
	 * Checks, whether there is an indication for a transient.
	 * @return True for a transient state indication.
	 */
	public boolean isTransient() {
		return trans;
	}

	/**
	 * Sets, whether there is an indication for a transient.
	 * @param trans True for a transient state indication.
	 */
	public void setTransient(boolean trans) {
		this.trans = trans;
	}

	/**
	 * Empty default constructor.
	 */
	public ValueWithTransientStateIndication() {
	}

	/**
	 * Constructor with fields.
	 * @param value The value.
	 * @param trans True for a transient state indication.
	 */
	public ValueWithTransientStateIndication(short value, boolean trans) {
		this.value = value;
		this.trans = trans;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (trans ? 1231 : 1237);
		result = prime * result + value;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ValueWithTransientStateIndication other = (ValueWithTransientStateIndication) obj;
		if (trans != other.trans) {
			return false;
		}
		if (value != other.value) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		}

		int byteAsInt = bytes[0] & 0xff;
		
		value = (short) (byteAsInt & 0x7f);
		trans = (byteAsInt & 0x80) == 0x80;

		return Arrays.copyOfRange(bytes, getEncodedSize(), bytes.length);
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		return new byte[] { (byte) (value | (trans ? 0x80 : 0x00)) };
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return 1;
	}

}