/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * Information element that contains a cause of an initialization.
 * <p>
 * Abbreviation: COI
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
@JsonIgnoreProperties({ "localPowerSwitchOn", "localManualReset", "remoteReset", "inCompatibleRange", "inPrivateRange",
		"execute" })
public class CauseOfInitialization implements IInformationElement {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = -4312665915743144693L;

	/**
	 * The cause of the initialization:
	 * <ul>
	 * <li>local power switch on</li>
	 * <li>local manual reset</li>
	 * <li>remote reset</li>
	 * <li>in compatible range</li>
	 * <li>in private range</li>
	 * </ul>
	 */
	private int value;

	/**
	 * Flag, whether local parameters are changed.
	 */
	private boolean changedLocalParameters;

	/**
	 * Returns the cause of the initialization.
	 * 
	 * @return An integer in:
	 *         <ul>
	 *         <li>local power switch on</li>
	 *         <li>local power switch reset</li>
	 *         <li>remote reset</li>
	 *         <li>in compatible range</li>
	 *         <li>in private range</li>
	 *         </ul>
	 */
	public int getValue() {
		return value;
	}

	/**
	 * Sets the cause of the initialization.
	 * 
	 * @param value An integer in:
	 *              <ul>
	 *              <li>local power switch on</li>
	 *              <li>local power switch reset</li>
	 *              <li>remote reset</li>
	 *              <li>in compatible range</li>
	 *              <li>in private range</li>
	 *              </ul>
	 */
	public void setValue(int value) {
		this.value = value;
	}

	/**
	 * Checks, whether a local power switch is turned on.
	 * 
	 * @return {@link #getValue()} == 0.
	 */
	public boolean isLocalPowerSwitchOn() {
		return value == 0;
	}

	/**
	 * Checks, whether it is a local, manual reset.
	 * 
	 * @return {@link #getValue()} == 1.
	 */
	public boolean isLocalManualReset() {
		return value == 1;
	}

	/**
	 * Checks, whether it is a remote reset.
	 * 
	 * @return {@link #getValue()} == 2.
	 */
	public boolean isRemoteReset() {
		return value == 2;
	}

	/**
	 * Checks, whether it is in the compatible range.
	 * 
	 * @return {@link #getValue()} >= 3 && {@link #getValue()} <= 31.
	 */
	public boolean isInCompatibleRange() {
		return value >= 3 && value <= 31;
	}

	/**
	 * Checks, whether it is in the private range.
	 * 
	 * @return {@link #getValue()} >= 31 && {@link #getValue()} <= 127.
	 */
	public boolean isInPrivateRange() {
		return value >= 31 && value <= 127;
	}

	/**
	 * Checks, whether local parameters are changed.
	 * 
	 * @return True, if local parameters are changed.
	 */
	public boolean isChangedLocalParameters() {
		return changedLocalParameters;
	}

	/**
	 * Sets, whether local parameters are changed.
	 * 
	 * @return True, if local parameters are changed.
	 */
	public void setChangedLocalParameters(boolean changedLocalParameters) {
		this.changedLocalParameters = changedLocalParameters;
	}

	/**
	 * Empty default constructor.
	 */
	public CauseOfInitialization() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param value                  An integer in:
	 *                               <ul>
	 *                               <li>local power switch on</li>
	 *                               <li>local power switch reset</li>
	 *                               <li>remote reset</li>
	 *                               <li>in compatible range</li>
	 *                               <li>in private range</li>
	 *                               </ul>
	 * @param changedLocalParameters True, if local parameters are changed.
	 */
	public CauseOfInitialization(int value, boolean changedLocalParameters) {
		this.value = value;
		this.changedLocalParameters = changedLocalParameters;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (changedLocalParameters ? 1231 : 1237);
		result = prime * result + value;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CauseOfInitialization other = (CauseOfInitialization) obj;
		if (changedLocalParameters != other.changedLocalParameters) {
			return false;
		}
		if (value != other.value) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		}

		int byteAsInt = bytes[0] & 0xff;

		value = byteAsInt & 0x7f;
		changedLocalParameters = (byteAsInt & 0x80) == 0x80;
		return Arrays.copyOfRange(bytes, getEncodedSize(), bytes.length);
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		return new byte[] { (byte) (value | (changedLocalParameters ? 0x80 : 0x00)) };
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return 1;
	}

}
