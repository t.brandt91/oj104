/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

//TODO javaDoc
//TODO Tests
//QOS
/**
 * Information element that contains a qualifier for a set point command.
 * <p>
 * Abbreviation: QOS
 * <p>
 * <ul>
 * <li>0 = default</li>
 * <li>1-63 = in compatible range</li>
 * <li>64-127 = in private range</li>
 * </ul>
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
@JsonIgnoreProperties({ "default", "inCompatibleRange", "inPrivateRange", "execute" })
public class SetPointCommandQualifier implements IInformationElement {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = -6749659784875770775L;

	/**
	 * The qualifier.
	 * <p>
	 * Abbreviation: QL
	 * <ul>
	 * <li>0 = default</li>
	 * <li>1-63 = in compatible range</li>
	 * <li>64-127 = in private range</li>
	 * </ul>
	 */
	private int value;

	/**
	 * True for a selection, false for an execution.
	 * <p>
	 * Abbreviation: S/E
	 */
	private boolean select;

	/**
	 * Returns the qualifier.
	 * 
	 * @return An integer in:
	 *         <ul>
	 *         <li>0 = default</li>
	 *         <li>1-63 = in compatible range</li>
	 *         <li>64-127 = in private range</li>
	 *         </ul>
	 */
	public int getValue() {
		return value;
	}

	/**
	 * Sets the qualifier.
	 * 
	 * @param value An integer in:
	 *              <ul>
	 *              <li>0 = default</li>
	 *              <li>1-63 = in compatible range</li>
	 *              <li>64-127 = in private range</li>
	 *              </ul>
	 */
	public void setValue(int value) {
		this.value = value;
	}

	/**
	 * Checks, whether the qualifier means default.
	 * 
	 * @return {@link #getValue()} == 0.
	 */
	public boolean isDefault() {
		return value == 0;
	}

	/**
	 * Checks, whether the qualifier is in compatible range.
	 * 
	 * @return {@link #getValue()} >= 1 && {@link #getValue()} <= 63.
	 */
	public boolean isInCompatibleRange() {
		return value >= 1 && value <= 63;
	}

	/**
	 * Checks, whether the qualifier is in private range.
	 * 
	 * @return {@link #getValue()} >= 64 && {@link #getValue()} <= 127.
	 */
	public boolean isInPrivateRange() {
		return value >= 64 && value <= 127;
	}

	/**
	 * Checks, whether the qualifier means a selection.
	 * 
	 * @return True for a selection, false for an execution.
	 */
	public boolean isSelect() {
		return select;
	}

	/**
	 * Sets, whether the qualifier means a selection.
	 * 
	 * @param select True for a selection, false for an execution.
	 */
	public void setSelect(boolean select) {
		this.select = select;
	}

	/**
	 * Checks, whether the qualifier means an execution.
	 * 
	 * @return True for an execution, false for a selection.
	 */
	public boolean isExecute() {
		return !select;
	}

	/**
	 * Sets, whether the qualifier means an execution.
	 * 
	 * @param execute True for an execution, false for a selection.
	 */
	public void setExecute(boolean execute) {
		this.select = !execute;
	}

	/**
	 * Empty default constructor.
	 */
	public SetPointCommandQualifier() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param value  The qualifier, an integer in:
	 *               <ul>
	 *               <li>0 = default</li>
	 *               <li>1-63 = in compatible range</li>
	 *               <li>64-127 = in private range</li>
	 *               </ul>
	 * @param select True for a selection, false for an execution.
	 */
	public SetPointCommandQualifier(int value, boolean select) {
		this.value = value;
		this.select = select;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (select ? 1231 : 1237);
		result = prime * result + value;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SetPointCommandQualifier other = (SetPointCommandQualifier) obj;
		if (select != other.select) {
			return false;
		}
		if (value != other.value) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		}

		int byteAsInt = bytes[0] & 0xff;

		value = byteAsInt & 0x7f;
		select = (byteAsInt & 0x80) == 0x80;
		return Arrays.copyOfRange(bytes, getEncodedSize(), bytes.length);
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		byte valueByte = (byte) value;
		valueByte |= select ? 0x80 : 0x00;
		return new byte[] { valueByte };
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return 1;
	}

}