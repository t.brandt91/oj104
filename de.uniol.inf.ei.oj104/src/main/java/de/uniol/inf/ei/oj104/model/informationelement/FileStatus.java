/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * Information element that contains the status of a file.
 * <p>
 * Abbreviation: SOF
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
@JsonIgnoreProperties({ "default", "inCompatibleRange", "inPrivateRange" })
public class FileStatus implements IInformationElement {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = 3264129600668541854L;

	/**
	 * The status of the file.
	 * <ul>
	 * <li>0 = default</li>
	 * <li>1-15 = in compatible range</li>
	 * <li>16-31 = in private range</li>
	 * </ul>
	 */
	private int value;

	/**
	 * True, if it is the last file of the directory; false, else.
	 * <p>
	 * Abbreviation: LFD
	 */
	private boolean lastFile;

	/**
	 * True if the name defines a subdirectory (true) or a file (false)
	 * <p>
	 * Abbreviation: FOR
	 */
	private boolean nameDefinesSubDir;

	/**
	 * True for an active file transfer.
	 */
	private boolean activeTransfer;

	/**
	 * Returns the status of the file.
	 * 
	 * @return An integer in:
	 *         <ul>
	 *         <li>0 = default</li>
	 *         <li>1-15 = in compatible range</li>
	 *         <li>16-31 = in private range</li>
	 *         </ul>
	 */
	public int getValue() {
		return value;
	}

	/**
	 * Sets the status of the file.
	 * 
	 * @param value An integer in:
	 *              <ul>
	 *              <li>0 = default</li>
	 *              <li>1-15 = in compatible range</li>
	 *              <li>16-31 = in private range</li>
	 *              </ul>
	 */
	public void setValue(int value) {
		this.value = value;
	}

	/**
	 * Checks, whether the status means default.
	 * 
	 * @return {@link #getValue()} == 0.
	 */
	public boolean isDefault() {
		return value == 0;
	}

	/**
	 * Checks, whether the status is in compatible range.
	 * 
	 * @return {@link #getValue()} >= 1 && {@link #getValue()} <= 15.
	 */
	public boolean isInCompatibleRange() {
		return value >= 1 && value <= 15;
	}

	/**
	 * Checks, whether the qualifier is in private range.
	 * 
	 * @return {@link #getValue()} >= 16 && {@link #getValue()} <= 31.
	 */
	public boolean isInPrivateRange() {
		return value >= 16 && value <= 31;
	}

	/**
	 * Checks, whether it is the last file of the directory.
	 * <p>
	 * Abbreviation: LFD
	 * 
	 * @return True, if it is the last file of the directory; false, else.
	 */
	public boolean isLastFile() {
		return lastFile;
	}

	/**
	 * Sets, whether it is the last file of the directory.
	 * <p>
	 * Abbreviation: LFD
	 * 
	 * @param lastFile True, if it is the last file of the directory; false, else.
	 */
	public void setLastFile(boolean lastFile) {
		this.lastFile = lastFile;
	}

	/**
	 * Checks, whether the name defines a subdirectory.
	 * <p>
	 * Abbreviation: FOR
	 * 
	 * @return True, if the name defines a subdirectory; false, if it defines a
	 *         file.
	 */
	public boolean isNameDefinesSubDir() {
		return nameDefinesSubDir;
	}

	/**
	 * Sets, whether the name defines a subdirectory.
	 * <p>
	 * Abbreviation: FOR
	 * 
	 * @param nameDefinesSubDir True, if the name defines a subdirectory; false, if
	 *                          it defines a file.
	 */
	public void setNameDefinesSubDir(boolean nameDefinesSubDir) {
		this.nameDefinesSubDir = nameDefinesSubDir;
	}

	/**
	 * Checks, whether the name defines a file.
	 * <p>
	 * Abbreviation: FOR
	 * 
	 * @return True, if the name defines a file; false, if it defines a
	 *         subdirectory.
	 */
	public boolean isNameDefinesFile() {
		return !nameDefinesSubDir;
	}

	/**
	 * Sets, whether the name defines a file.
	 * <p>
	 * Abbreviation: FOR
	 * 
	 * @param nameDefinesFile True, if the name defines a file; false, if it defines
	 *                        a subdirectory.
	 */
	public void setNameDefinesFile(boolean nameDefinesFile) {
		this.nameDefinesSubDir = !nameDefinesFile;
	}

	/**
	 * Checks, whether there is an active file transfer.
	 * 
	 * @return True for an active file transfer.
	 */
	public boolean isActiveTransfer() {
		return activeTransfer;
	}

	/**
	 * Sets, whether there is an active file transfer.
	 * 
	 * @param activeTransfer True for an active file transfer.
	 */
	public void setActiveTransfer(boolean activeTransfer) {
		this.activeTransfer = activeTransfer;
	}

	/**
	 * Empty default constructor.
	 */
	public FileStatus() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param value             The status of the file.
	 *                          <ul>
	 *                          <li>0 = default</li>
	 *                          <li>1-15 = in compatible range</li>
	 *                          <li>16-31 = in private range</li>
	 *                          </ul>
	 * @param lastFile          True, if it is the last file of the directory;
	 *                          false, else.
	 *                          <p>
	 *                          Abbreviation: LFD
	 * @param nameDefinesSubDir True if the name defines a subdirectory (true) or a
	 *                          file (false)
	 *                          <p>
	 *                          Abbreviation: FOR
	 * @param activeTransfer    True for an active file transfer.
	 */
	public FileStatus(int value, boolean lastFile, boolean nameDefinesSubDir, boolean activeTransfer) {
		this.value = value;
		this.lastFile = lastFile;
		this.nameDefinesSubDir = nameDefinesSubDir;
		this.activeTransfer = activeTransfer;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (activeTransfer ? 1231 : 1237);
		result = prime * result + (lastFile ? 1231 : 1237);
		result = prime * result + (nameDefinesSubDir ? 1231 : 1237);
		result = prime * result + value;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		FileStatus other = (FileStatus) obj;
		if (activeTransfer != other.activeTransfer) {
			return false;
		}
		if (lastFile != other.lastFile) {
			return false;
		}
		if (nameDefinesSubDir != other.nameDefinesSubDir) {
			return false;
		}
		if (value != other.value) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		}

		int byteAsInt = bytes[0] & 0xff;

		value = byteAsInt & 0x1f;
		lastFile = (byteAsInt & 0x20) == 0x20;
		nameDefinesSubDir = (byteAsInt & 0x40) == 0x40;
		activeTransfer = (byteAsInt & 0x80) == 0x80;
		return Arrays.copyOfRange(bytes, getEncodedSize(), bytes.length);
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		byte valueByte = (byte) value;
		valueByte |= lastFile ? 0x20 : 0x00;
		valueByte |= nameDefinesSubDir ? 0x40 : 0x00;
		valueByte |= activeTransfer ? 0x80 : 0x00;
		return new byte[] { valueByte };
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return 1;
	}

}