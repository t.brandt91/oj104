/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import java.util.Arrays;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * Qualifier for file objects to be used in {@link IInformationElement}s:
 * positive or negative confirm.
 * <p>
 * Abbreviation: FRQ
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class FileReadyQualifier extends FileObjectReadyQualifier implements IInformationElement {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = 1391741663392373336L;

	/**
	 * True for negative, false for positive confirm.
	 */
	private boolean negativeConfirm;

	/**
	 * Checks, whether the confirm is negative.
	 * 
	 * @return True for negative, false for positive confirm.
	 */
	public boolean isNegativeConfirm() {
		return negativeConfirm;
	}

	/**
	 * Sets, whether the confirm is negative.
	 * 
	 * @param negativeConfirm True for negative, false for positive confirm.
	 */
	public void setNegativeConfirm(boolean negativeConfirm) {
		this.negativeConfirm = negativeConfirm;
	}

	/**
	 * Empty default constructor.
	 */
	public FileReadyQualifier() {
		super();
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param value           The qualifier, an integer in:
	 *                        <ul>
	 *                        <li>0 = default</li>
	 *                        <li>1-63 = in compatible range</li>
	 *                        <li>64-127 = in private range</li>
	 *                        </ul>
	 * @param negativeConfirm True for negative, false for positive confirm.
	 */
	public FileReadyQualifier(int value, boolean negativeConfirm) {
		super(value);
		this.negativeConfirm = negativeConfirm;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (negativeConfirm ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		FileReadyQualifier other = (FileReadyQualifier) obj;
		if (negativeConfirm != other.negativeConfirm) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		super.fromBytes(bytes);

		int byteAsInt = bytes[0] & 0xff;

		negativeConfirm = (byteAsInt & 0x80) == 0x80;
		return Arrays.copyOfRange(bytes, getEncodedSize(), bytes.length);
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		byte[] bytes = super.toBytes();
		bytes[0] |= negativeConfirm ? 0x80 : 0x00;
		return bytes;
	}

}