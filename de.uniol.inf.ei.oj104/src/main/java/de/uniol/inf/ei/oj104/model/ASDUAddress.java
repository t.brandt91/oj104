/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.util.JsonParser;
import de.uniol.inf.ei.oj104.util.SystemProperties;

/**
 * The station address of the ASDU.
 * <p>
 * 0 is defined as not used. <br />
 * 255 for addresses with 1 byte or 655535 for addresses with 2 bytes is defined
 * as global address. <br />
 * All other addresses between not used and global addresses are station
 * addresses.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
@JsonIgnoreProperties({ "notUsed", "globalAddress", "stationAddress" })
public class ASDUAddress implements IByteEncodedEntity {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = 356366557285566647L;

	/**
	 * The number of bytes used for the ASDU address. 1 or 2, defined in system
	 * properties.
	 */
	private static final int asduAddressEncodedSize = Integer
			.parseInt(SystemProperties.getProperties().getProperty("asduaddress.bytesize", "1"));

	/**
	 * The ASDU address as an integer in [0, 255/655535].
	 */
	private int value;

	/**
	 * Return the ASDU address.
	 * 
	 * @return An integer in [0, 255/655535].
	 */
	public int getValue() {
		return value;
	}

	/**
	 * Sets the ASDU address.
	 * 
	 * @param value An integer in [0, 255/655535].
	 */
	public void setValue(int value) {
		this.value = value;
	}

	/**
	 * Checks, whether the ASDU address is not used.
	 * 
	 * @return {@link #getValue()} == 0.
	 */
	public boolean isNotUsed() {
		return value == 0;
	}

	/**
	 * Checks, whether the ASDU address is the global one.
	 * 
	 * @return {@link #getValue()} == [0, 255/655535].
	 */
	public boolean isGlobalAddress() {
		return value == (asduAddressEncodedSize == 2 ? Short.MAX_VALUE : Byte.MAX_VALUE);
	}

	/**
	 * Checks, whether the ASDU address is a normal station address.
	 * 
	 * @return !{@link #isNotUsed()} && !{@link #isGlobalAddress()}.
	 */
	public boolean isStationAddress() {
		return !isNotUsed() && !isGlobalAddress();
	}

	/**
	 * Empty default constructor.
	 */
	public ASDUAddress() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param value The ASDU address as an integer in [0, 255/655535].
	 */
	public ASDUAddress(int value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + value;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ASDUAddress other = (ASDUAddress) obj;
		if (value != other.value) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		}

		value = bytes[0] & 0xff;
		if (getEncodedSize() == 2) {
			value |= (bytes[1] & 0xff) << 8;
		}
		return Arrays.copyOfRange(bytes, getEncodedSize(), bytes.length);
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		byte[] bytes = new byte[getEncodedSize()];
		bytes[0] = (byte) value;
		if (getEncodedSize() == 2) {
			bytes[1] = (byte) (value >> 8);
		}
		return bytes;
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return asduAddressEncodedSize;
	}

}