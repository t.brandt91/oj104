/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * Quality descriptors to be used for protection equipment in
 * {@link IInformationElement}s.
 * <p>
 * Abbreviation: QDS
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class QualityDescriptorForPE extends QualityDescriptor implements IInformationElement {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = 8345859459182425624L;

	/**
	 * True, if the specified time interval is elapsed.
	 * <p>
	 * Abbreviation: EI
	 */
	private boolean elapsedTimeInvalid;

	/**
	 * Checks, whether the specified time interval is elapsed.
	 * <p>
	 * Abbreviation: EI
	 * 
	 * @return True, if the specified time interval is elapsed.
	 */
	public boolean isElapsedTimeInvalid() {
		return elapsedTimeInvalid;
	}

	/**
	 * Sets, whether the specified time interval is elapsed.
	 * <p>
	 * Abbreviation: EI
	 * 
	 * @param elapsedTimeInvalid True, if the specified time interval is elapsed.
	 */
	public void setElapsedTimeInvalid(boolean elapsedTimeInvalid) {
		this.elapsedTimeInvalid = elapsedTimeInvalid;
	}

	/**
	 * Empty default constructor.
	 */
	public QualityDescriptorForPE() {
		super();
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param blocked            True, if the value is marked as blocked.
	 *                           <p>
	 *                           Abbreviation: BL
	 * @param substituted        True, if the value is marked as substituted.
	 *                           <p>
	 *                           Abbreviation: SB
	 * @param notTopical         True, if the value is marked as not topical.
	 *                           <p>
	 *                           Abbreviation: NT
	 * @param invalid            True, if the value is marked as invalid.
	 *                           <p>
	 *                           Abbreviation: IV
	 * @param elapsedTimeInvalid True, if the specified time interval is elapsed.
	 *                           <p>
	 *                           Abbreviation: EI
	 */
	public QualityDescriptorForPE(boolean blocked, boolean substituted, boolean notTopical, boolean invalid,
			boolean elapsedTimeInvalid) {
		super(blocked, substituted, notTopical, invalid);
		this.elapsedTimeInvalid = elapsedTimeInvalid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (elapsedTimeInvalid ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		QualityDescriptorForPE other = (QualityDescriptorForPE) obj;
		if (elapsedTimeInvalid != other.elapsedTimeInvalid) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		}

		int byteAsInt = bytes[0] & 0xff;

		elapsedTimeInvalid = (byteAsInt & 0x08) == 0x08;
		return super.fromBytes(bytes);
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		byte[] qdsBytes = super.toBytes();
		qdsBytes[0] |= elapsedTimeInvalid ? 0x08 : 0x00;
		return qdsBytes;
	}

}