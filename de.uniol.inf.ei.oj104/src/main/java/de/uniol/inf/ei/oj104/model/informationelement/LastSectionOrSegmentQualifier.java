/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * Information element that contains a qualifier for last sections or segments.
 * <p>
 * Abbreviation: LSQ
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
@JsonIgnoreProperties({ "notUsed", "fileTransferWithoutDeactivation", "fileTransferWithDeactivation",
		"sectionTransferWithoutDeactivation", "sectionTransferWithDeactivation", "inCompatibleRange",
		"inPrivateRange" })
public class LastSectionOrSegmentQualifier implements IInformationElement {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = 4943319271712827258L;

	/**
	 * The qualifier.
	 * <ul>
	 * <li>0 = not used</li>
	 * <li>1 = file transfer without deactivation</li>
	 * <li>2 = file transfer with deactivation</li>
	 * <li>3 = section transfer without deactivation</li>
	 * <li>4 = section transfer with deactivation</li>
	 * <li>5-127 = in compatible range</li>
	 * <li>128-255 = in private range</li>
	 * </ul>
	 */
	private int value;

	/**
	 * Returns the qualifier.
	 * 
	 * @return An integer in:
	 *         <ul>
	 *         <li>0 = not used</li>
	 *         <li>1 = file transfer without deactivation</li>
	 *         <li>2 = file transfer with deactivation</li>
	 *         <li>3 = section transfer without deactivation</li>
	 *         <li>4 = section transfer with deactivation</li>
	 *         <li>5-127 = in compatible range</li>
	 *         <li>128-255 = in private range</li>
	 *         </ul>
	 */
	public int getValue() {
		return value;
	}

	/**
	 * Sets the qualifier.
	 * 
	 * @param value An integer in:
	 *              <ul>
	 *              <li>0 = not used</li>
	 *              <li>1 = file transfer without deactivation</li>
	 *              <li>2 = file transfer with deactivation</li>
	 *              <li>3 = section transfer without deactivation</li>
	 *              <li>4 = section transfer with deactivation</li>
	 *              <li>5-127 = in compatible range</li>
	 *              <li>128-255 = in private range</li>
	 *              </ul>
	 */
	public void setValue(int value) {
		this.value = value;
	}

	/**
	 * Checks, whether the qualifier means not used.
	 * 
	 * @return {@link #getValue()} == 0.
	 */
	public boolean isNotUsed() {
		return value == 0;
	}

	/**
	 * Checks, whether the qualifier means file transfer without deactivation.
	 * 
	 * @return {@link #getValue()} == 1.
	 */
	public boolean isFileTransferWithoutDeactivation() {
		return value == 1;
	}

	/**
	 * Checks, whether the qualifier means file transfer with deactivation.
	 * 
	 * @return {@link #getValue()} == 2.
	 */
	public boolean isFileTransferWithDeactivation() {
		return value == 2;
	}

	/**
	 * Checks, whether the qualifier means section transfer without deactivation.
	 * 
	 * @return {@link #getValue()} == 3.
	 */
	public boolean isSectionTransferWithoutDeactivation() {
		return value == 3;
	}

	/**
	 * Checks, whether the qualifier means section transfer without deactivation.
	 * 
	 * @return {@link #getValue()} == 4.
	 */
	public boolean isSectionTransferWithDeactivation() {
		return value == 4;
	}

	/**
	 * Checks, whether the qualifier is in compatible range.
	 * 
	 * @return {@link #getValue()} >= 5 && {@link #getValue()} <= 127.
	 */
	public boolean isInCompatibleRange() {
		return value >= 5 && value <= 127;
	}

	/**
	 * Checks, whether the qualifier is in private range.
	 * 
	 * @return {@link #getValue()} >= 128 && {@link #getValue()} <= 255.
	 */
	public boolean isInPrivateRange() {
		return value >= 128 && value <= 255;
	}

	/**
	 * Empty default constructor.
	 */
	public LastSectionOrSegmentQualifier() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param value The qualifier, an integer in:
	 *              <ul>
	 *              <li>0 = not used</li>
	 *              <li>1 = file transfer without deactivation</li>
	 *              <li>2 = file transfer with deactivation</li>
	 *              <li>3 = section transfer without deactivation</li>
	 *              <li>4 = section transfer with deactivation</li>
	 *              <li>5-127 = in compatible range</li>
	 *              <li>128-255 = in private range</li>
	 *              </ul>
	 */
	public LastSectionOrSegmentQualifier(int value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + value;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		LastSectionOrSegmentQualifier other = (LastSectionOrSegmentQualifier) obj;
		if (value != other.value) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		}

		value = bytes[0] & 0xff;
		return Arrays.copyOfRange(bytes, getEncodedSize(), bytes.length);
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		return new byte[] { (byte) value };
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return 1;
	}

}