/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Optional;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * Information element that contains single events of protection equipment.
 * <p>
 * Abbreviation: SEP
 * <p>
 * <ul>
 * <li>0,3 = not determined</li>
 * <li>1 = off</li>
 * <li>2 = on</li>
 * </ul>
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class PESingleEvent implements IInformationElement {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = -7121806105421987287L;

	/**
	 * Enumeration for single event states of protection equipment.
	 * 
	 * @author Michael Brand (michael.brand@uol.de)
	 *
	 */
	public enum EventState implements Serializable {

		/**
		 * Not determinable.
		 */
		INDETERMINATE_0(0),

		/**
		 * Off.
		 */
		OFF(1),

		/**
		 * On.
		 */
		ON(2),

		/**
		 * Not permitted.
		 */
		INDETERMINATE_3(3);

		/**
		 * The code of the single event:
		 * <ul>
		 * <li>0, 3 = indeterminable</li>
		 * <li>1 = off</li>
		 * <li>2 = on</li>
		 * </ul>
		 */
		private int code;

		/**
		 * Returns the code of the single event.
		 * 
		 * @return An integer in:
		 *         <ul>
		 *         <li>0, 3 = indeterminable</li>
		 *         <li>1 = off</li>
		 *         <li>2 = on</li>
		 *         </ul>
		 */
		public int getCode() {
			return code;
		}

		/**
		 * Constructor with fields.
		 * 
		 * @param code An integer in:
		 *             <ul>
		 *             <li>0, 3 = indeterminable</li>
		 *             <li>1 = off</li>
		 *             <li>2 = on</li>
		 *             </ul>
		 */
		private EventState(int code) {
			this.code = code;
		}

		/**
		 * Retrieve an enum entry.
		 * 
		 * @param code The code of the single event:
		 *             <ul>
		 *             <li>0, 3 = indeterminable</li>
		 *             <li>1 = off</li>
		 *             <li>2 = on</li>
		 *             </ul>
		 * @return An optional of the {@link EventState} or {@link Optional#empty()}, if
		 *         there is none for the code.
		 */
		public static Optional<EventState> getState(int code) {
			return Arrays.asList(values()).stream().filter(state -> state.getCode() == code).findAny();
		}
	}

	/**
	 * The state of the single event.
	 * <ul>
	 * <li>{@link EventState#INDETERMINATE_0}</li>
	 * <li>{@link EventState#OFF}</li>
	 * <li>{@link EventState#ON}</li>
	 * <li>{@link EventState#INDETERMINATE_3}</li>
	 * </ul>
	 */
	private EventState eventState;

	/**
	 * The quality descriptor for the protection equipment. Can be blocked,
	 * substituted, not topical, invalid, or elapsed in time.
	 */
	private QualityDescriptorForPE qualityDescriptor;

	/**
	 * Returns state of the single event.
	 * 
	 * @return One of the following values:
	 *         <ul>
	 *         <li>{@link EventState#INDETERMINATE_0}</li>
	 *         <li>{@link EventState#OFF}</li>
	 *         <li>{@link EventState#ON}</li>
	 *         <li>{@link EventState#INDETERMINATE_3}</li>
	 *         </ul>
	 */
	public EventState getEventState() {
		return eventState;
	}

	/**
	 * Sets state of the single event.
	 * 
	 * @param eventState One of the following values:
	 *                   <ul>
	 *                   <li>{@link EventState#INDETERMINATE_0}</li>
	 *                   <li>{@link EventState#OFF}</li>
	 *                   <li>{@link EventState#ON}</li>
	 *                   <li>{@link EventState#INDETERMINATE_3}</li>
	 *                   </ul>
	 */
	public void setEventState(EventState eventState) {
		this.eventState = eventState;
	}

	/**
	 * Returns the quality descriptor for the protection equipment.
	 * 
	 * @return Can be blocked, substituted, not topical, invalid, or elapsed in
	 *         time.
	 */
	protected QualityDescriptorForPE getQualityDescriptor() {
		return qualityDescriptor;
	}

	/**
	 * Checks, whether the value is blocked.
	 * <p>
	 * Abbreviation: BL
	 * 
	 * @return True, if the value is marked as blocked.
	 */
	public boolean isBlocked() {
		return qualityDescriptor.isBlocked();
	}

	/**
	 * Sets, whether the value is blocked.
	 * <p>
	 * Abbreviation: BL
	 * 
	 * @param blocked True, if the value shall be marked as blocked.
	 */
	public void setBlocked(boolean blocked) {
		qualityDescriptor.setBlocked(blocked);
	}

	/**
	 * Checks, whether the value is substituted.
	 * <p>
	 * Abbreviation: SB
	 * 
	 * @return True, if the value is marked as substituted.
	 */
	public boolean isSubstituted() {
		return qualityDescriptor.isSubstituted();
	}

	/**
	 * Sets, whether the value is substituted.
	 * <p>
	 * Abbreviation: SB
	 * 
	 * @param substituted True, if the value shall be marked as substituted.
	 */
	public void setSubstituted(boolean substituted) {
		qualityDescriptor.setSubstituted(substituted);
	}

	/**
	 * Checks, whether the value is not topical.
	 * <p>
	 * Abbreviation: NT
	 * 
	 * @return True, if the value is marked as not topical.
	 */
	public boolean isNotTopical() {
		return qualityDescriptor.isNotTopical();
	}

	/**
	 * Sets, whether the value is not topical.
	 * <p>
	 * Abbreviation: NT
	 * 
	 * @param notTopical True, if the value shall be marked as not topical.
	 */
	public void setNotTopical(boolean notTopical) {
		qualityDescriptor.setNotTopical(notTopical);
	}

	/**
	 * Checks, whether the value is invalid.
	 * <p>
	 * Abbreviation: IV
	 * 
	 * @return True, if the value is marked as invalid.
	 */
	public boolean isInvalid() {
		return qualityDescriptor.isInvalid();
	}

	/**
	 * Sets, whether the value is invalid.
	 * <p>
	 * Abbreviation: IV
	 * 
	 * @param invalid True, if the value shall be marked as invalid.
	 */
	public void setInvalid(boolean invalid) {
		qualityDescriptor.setInvalid(invalid);
	}

	/**
	 * Checks, whether the specified time interval is elapsed.
	 * <p>
	 * Abbreviation: EI
	 * 
	 * @return True, if the specified time interval is elapsed.
	 */
	public boolean isElapsedTimeInvalid() {
		return qualityDescriptor.isElapsedTimeInvalid();
	}

	/**
	 * Sets, whether the specified time interval is elapsed.
	 * <p>
	 * Abbreviation: EI
	 * 
	 * @param elapsedTimeInvalid True, if the specified time interval is elapsed.
	 */
	public void setElapsedTimeInvalid(boolean elapsedTimeInvalid) {
		qualityDescriptor.setElapsedTimeInvalid(elapsedTimeInvalid);
	}

	/**
	 * Empty default constructor.
	 */
	public PESingleEvent() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param eventState        The state of the single event can be:
	 *                          <ul>
	 *                          <li>{@link EventState#INDETERMINATE_0}</li>
	 *                          <li>{@link EventState#OFF}</li>
	 *                          <li>{@link EventState#ON}</li>
	 *                          <li>{@link EventState#INDETERMINATE_3}</li>
	 *                          </ul>
	 * @param qualityDescriptor The quality descriptor for the protection equipment.
	 *                          Can be blocked, substituted, not topical, invalid,
	 *                          or elapsed in time.
	 */
	public PESingleEvent(EventState eventState, QualityDescriptorForPE qualityDescriptor) {
		this.eventState = eventState;
		this.qualityDescriptor = qualityDescriptor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((eventState == null) ? 0 : eventState.hashCode());
		result = prime * result + ((qualityDescriptor == null) ? 0 : qualityDescriptor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PESingleEvent other = (PESingleEvent) obj;
		if (eventState != other.eventState) {
			return false;
		}
		if (qualityDescriptor == null) {
			if (other.qualityDescriptor != null) {
				return false;
			}
		} else if (!qualityDescriptor.equals(other.qualityDescriptor)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		}

		int byteAsInt = bytes[0] & 0xff;

		eventState = EventState.getState(byteAsInt & 0x03)
				.orElseThrow(() -> new IEC608705104ProtocolException(getClass(),
						byteAsInt + " is not a valid single event of protection equipment!"));
		qualityDescriptor = new QualityDescriptorForPE();
		byte[] remainingBytes = qualityDescriptor.fromBytes(bytes);

		return remainingBytes;
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		byte[] qdsBytes = qualityDescriptor.toBytes();
		qdsBytes[0] |= eventState.getCode();
		return qdsBytes;
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return 1;
	}

}