/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import de.uniol.inf.ei.oj104.model.IByteEncodedEntity;
import de.uniol.inf.ei.oj104.model.IInformationElement;

/**
 * Interface for quality descriptors to be used in {@link IInformationElement}s.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public interface IQualityDescriptor extends IByteEncodedEntity {

	/**
	 * Checks, whether the value is blocked.
	 * <p>
	 * Abbreviation: BL
	 * 
	 * @return True, if the value is marked as blocked.
	 */
	public boolean isBlocked();

	/**
	 * Sets, whether the value is blocked.
	 * <p>
	 * Abbreviation: BL
	 * 
	 * @param blocked True, if the value shall be marked as blocked.
	 */
	public void setBlocked(boolean blocked);

	/**
	 * Checks, whether the value is substituted.
	 * <p>
	 * Abbreviation: SB
	 * 
	 * @return True, if the value is marked as substituted.
	 */
	public boolean isSubstituted();

	/**
	 * Sets, whether the value is substituted.
	 * <p>
	 * Abbreviation: SB
	 * 
	 * @param substituted True, if the value shall be marked as substituted.
	 */
	public void setSubstituted(boolean substituted);

	/**
	 * Checks, whether the value is not topical.
	 * <p>
	 * Abbreviation: NT
	 * 
	 * @return True, if the value is marked as not topical.
	 */
	public boolean isNotTopical();

	/**
	 * Sets, whether the value is not topical.
	 * <p>
	 * Abbreviation: NT
	 * 
	 * @param notTopical True, if the value shall be marked as not topical.
	 */
	public void setNotTopical(boolean notTopical);

	/**
	 * Checks, whether the value is invalid.
	 * <p>
	 * Abbreviation: IV
	 * 
	 * @return True, if the value is marked as invalid.
	 */
	public boolean isInvalid();

	/**
	 * Sets, whether the value is invalid.
	 * <p>
	 * Abbreviation: IV
	 * 
	 * @param invalid True, if the value shall be marked as invalid.
	 */
	public void setInvalid(boolean invalid);

}