/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model;

/**
 * Interface for control fields.
 * <p>
 * A control field is part of an {@link APCI} and defines control information
 * like send and receive sequence numbers, or whether the APDU is for
 * starting/stopping the data transfer or for testing.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public interface IControlField extends IByteEncodedEntity {

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return 4;
	}

}