/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import de.uniol.inf.ei.oj104.model.ASDU;
import de.uniol.inf.ei.oj104.model.ASDUAddress;
import de.uniol.inf.ei.oj104.model.ASDUType;
import de.uniol.inf.ei.oj104.model.CauseOfTransmission;
import de.uniol.inf.ei.oj104.model.Confirm;
import de.uniol.inf.ei.oj104.model.DataUnitIdentifier;
import de.uniol.inf.ei.oj104.model.DataUnitType;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.model.IInformationObject;
import de.uniol.inf.ei.oj104.model.ITimeTag;
import de.uniol.inf.ei.oj104.model.InformationElementSequence;
import de.uniol.inf.ei.oj104.model.SequenceInformationObject;
import de.uniol.inf.ei.oj104.model.SingleInformationObject;
import de.uniol.inf.ei.oj104.model.StructureQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.AckFileOrSegmentQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.BinaryCounterReading;
import de.uniol.inf.ei.oj104.model.informationelement.BinaryStateInformation;
import de.uniol.inf.ei.oj104.model.informationelement.CauseOfInitialization;
import de.uniol.inf.ei.oj104.model.informationelement.Checksum;
import de.uniol.inf.ei.oj104.model.informationelement.CounterInterrogationCommandQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.DoubleCommand;
import de.uniol.inf.ei.oj104.model.informationelement.DoublePointInformation;
import de.uniol.inf.ei.oj104.model.informationelement.FileOrSectionLength;
import de.uniol.inf.ei.oj104.model.informationelement.FileReadyQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.FileStatus;
import de.uniol.inf.ei.oj104.model.informationelement.FixedTestBitPattern;
import de.uniol.inf.ei.oj104.model.informationelement.InterrogationQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.LastSectionOrSegmentQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.MeasuredValuesParameterQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.NameOfFile;
import de.uniol.inf.ei.oj104.model.informationelement.NameOfSection;
import de.uniol.inf.ei.oj104.model.informationelement.NormalizedValue;
import de.uniol.inf.ei.oj104.model.informationelement.PEOutputCircuitInformation;
import de.uniol.inf.ei.oj104.model.informationelement.PESingleEvent;
import de.uniol.inf.ei.oj104.model.informationelement.PEStartEvent;
import de.uniol.inf.ei.oj104.model.informationelement.ParameterActivationQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorForPE;
import de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorWithOV;
import de.uniol.inf.ei.oj104.model.informationelement.RegulatingStepCommand;
import de.uniol.inf.ei.oj104.model.informationelement.ResetProcessCommandQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.ScaledValue;
import de.uniol.inf.ei.oj104.model.informationelement.SectionReadyQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.Segment;
import de.uniol.inf.ei.oj104.model.informationelement.SelectAndCallQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.SetPointCommandQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.ShortFloatingPointNumber;
import de.uniol.inf.ei.oj104.model.informationelement.SingleCommand;
import de.uniol.inf.ei.oj104.model.informationelement.SinglePointInformation;
import de.uniol.inf.ei.oj104.model.informationelement.StatusAndChangeDetection;
import de.uniol.inf.ei.oj104.model.informationelement.ValueWithTransientStateIndication;
import de.uniol.inf.ei.oj104.model.timetag.SevenOctetBinaryTime;
import de.uniol.inf.ei.oj104.model.timetag.TwoOctetBinaryTime;

/**
 * Utility class to create random but valid {@link ASDU}s.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class RandomASDUCreator {

	/**
	 * Creates a list of random {@link ASDUs} covering all possible combinations of
	 * the following
	 * <ul>
	 * <li>all valid structure qualifiers</li>
	 * <li>one up to three information elements</li>
	 * <li>all valid causes of transmission</li>
	 * <li>positive and negative confirm</li>
	 * <li>test and no test</li>
	 * <li>0, 1, 255 as originator address and a random address in between</li>
	 * <li>0, 1, 255 as ASDU address and a random address in between</li>
	 * <li>0, 1, 255 as information object address and a random address in
	 * between</li>
	 * </ul>
	 * Calls
	 * {@link #createASDU(int, StructureQualifier, int, CauseOfTransmission, Confirm, boolean, int, int, int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}. The IDs are subsequent
	 *                   numbers defined in the standard.
	 * @param ieClasses  The list of used information element classes in the
	 *                   {@link ASDUType}.
	 * @param ieCreators The list of constructors for the used information element
	 *                   classes.
	 * @return A list of random {@link ASDUs} covering all possible combinations.
	 */
	private List<ASDU> createASDUs(int asduTypeId, List<Class<? extends IInformationElement>> ieClasses,
			List<Method> ieCreators) {
		final List<ASDU> asdus = new ArrayList<>();
		final Random random = new Random();
		final List<StructureQualifier> sqs = ASDUFactory.getStructureQualifiers(asduTypeId);
		final int maxIes = 3;
		List<CauseOfTransmission> cots = ASDUFactory.getCausesOfTransmission(asduTypeId);

		for (StructureQualifier sq : sqs) {
			for (int numIes = 1; numIes < maxIes; numIes++) {
				for (CauseOfTransmission cot : cots) {
					for (Confirm confirm : Confirm.values()) {
						for (boolean test : new boolean[] { true, false }) {
							for (int originatorAddress : new int[] { 0, 1, 255, random.nextInt(255) }) {
								for (int asduAddressCode : new int[] { 0, 1, 255, random.nextInt(255) }) {
									for (int ioAddress : new int[] { 0, 1, 255, random.nextInt(255) }) {
										asdus.add(createASDU(asduTypeId, sq, numIes, cot, confirm, test,
												originatorAddress, asduAddressCode, ioAddress, ieClasses, ieCreators));
									}
								}
							}
						}
					}
				}
			}
		}

		return asdus;
	}

	/**
	 * Creates a {@link ASDUs} for a specific set of features. Random are the
	 * included information elements and time tags.
	 * <p>
	 * Calls {@link RandomInformationElementCreator} methods.
	 * 
	 * @param asduTypeId        The ID of the {@link ASDUType}. The IDs are
	 *                          subsequent numbers defined in the standard.
	 * @param sq                The {@link StructureQualifier} to use.
	 * @param numIes            The amount of {@link IInformationElement}s to use.
	 * @param cot               The {@link CauseOfTransmission} to use.
	 * @param confirm           Positive or negative confirm
	 * @param test              Test ASDU or nor.
	 * @param originatorAddress The originator address to use.
	 * @param asduAddressCode   The ASDU address to use.
	 * @param ieClasses         The list of used information element classes.
	 * @param ieCreators        The list of constructors for the used information
	 *                          element classes.
	 * @return An {@link ASDUs} with random {@link IInformationElement}s.
	 */
	private ASDU createASDU(int asduTypeId, StructureQualifier sq, int numIes, CauseOfTransmission cot, Confirm confirm,
			boolean test, int originatorAddress, int asduAddressCode, int ioAddress,
			List<Class<? extends IInformationElement>> ieClasses, List<Method> ieCreators) {
		// Create data unit identifier
		ASDUType asduType = ASDUFactory.getASDUType(asduTypeId).get();
		DataUnitType dataUnitType = new DataUnitType(asduType, sq, numIes);
		ASDUAddress asduAddress = new ASDUAddress(asduAddressCode);
		DataUnitIdentifier dataUnitIdentifier = new DataUnitIdentifier(dataUnitType, cot, confirm, test,
				originatorAddress, asduAddress);

		// Create information objects
		Optional<Class<? extends ITimeTag>> timeTagClass = ASDUFactory.getTimeTagClass(asduTypeId);
		ITimeTag timeTag = null;
		if (timeTagClass.isPresent()) {
			timeTag = RandomInformationElementCreator.createTimeTag(timeTagClass.get());
		}
		List<IInformationObject> ios = new ArrayList<>();
		List<List<IInformationElement>> ies = new ArrayList<>(numIes);
		for (int i = 0; i < numIes; i++) {
			List<IInformationElement> innerIEs = new ArrayList<>(ieCreators.size());
			for (Method ieCreator : ieCreators) {
				try {
					IInformationElement ie = (IInformationElement) ieCreator.invoke(this);
					innerIEs.add(ie);
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}
			ies.add(innerIEs);
		}

		if (sq == StructureQualifier.SEQUENCE) {
			List<InformationElementSequence> ieSequences = new ArrayList<>(numIes);
			for (int i = 0; i < numIes; i++) {
				List<IInformationElement> sequenceIEs = new ArrayList<>(1);
				for (IInformationElement ie : ies.get(i)) {
					sequenceIEs.add(ie);
				}
				ieSequences.add(new InformationElementSequence(ieClasses, sequenceIEs,
						timeTagClass.orElseGet(() -> null), timeTag));
			}
			ios.add(new SequenceInformationObject(ieClasses, timeTagClass.orElseGet(() -> null), numIes, ioAddress,
					ieSequences));
		} else {
			// SINGLE
			for (int i = 0; i < numIes; i++) {
				List<IInformationElement> sequenceIEs = new ArrayList<>(ieCreators.size());
				for (IInformationElement ie : ies.get(i)) {
					sequenceIEs.add(ie);
				}
				ios.add(new SingleInformationObject(ieClasses, sequenceIEs, timeTagClass.orElseGet(() -> null), timeTag,
						ioAddress));
			}
		}

		// Create ASDU
		ASDU asdu = new ASDU(dataUnitIdentifier, ios);
		return asdu;
	}

	/**
	 * Creates a {@link ASDUs} of the type "Single-Point Information". Random are
	 * the included information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 1, 2, or 30.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "Single-Point Information".
	 */
	private List<ASDU> createASDUs_M_SP_XX_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(1);
		ieClasses.add(SinglePointInformation.class);
		List<Method> ieCreators = new ArrayList<>(1);
		try {
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createSinglePointInformationElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "Double-Point Information". Random are
	 * the included information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 3, 4, or 31.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "Double-Point Information".
	 */
	private List<ASDU> createASDUs_M_DP_XX_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(1);
		ieClasses.add(DoublePointInformation.class);
		List<Method> ieCreators = new ArrayList<>(1);
		try {
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createDoublePointInformationElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "Step position information". Random are
	 * the included information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 5, 6, or 32.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "Step position information".
	 */
	private List<ASDU> createASDUs_M_ST_XX_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(2);
		ieClasses.add(ValueWithTransientStateIndication.class);
		ieClasses.add(QualityDescriptorWithOV.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators.add(RandomInformationElementCreator.class
					.getDeclaredMethod("createValueWithTransientStateIndicationElement"));
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createQualityDescriptorWithOVElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "Bitstring of 32 bits". Random are the
	 * included information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 7, 8, or 33.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "Bitstring of 32 bits".
	 */
	private List<ASDU> createASDUs_M_BO_XX_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(2);
		ieClasses.add(BinaryStateInformation.class);
		ieClasses.add(QualityDescriptorWithOV.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createBinaryStateInformationElement"));
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createQualityDescriptorWithOVElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "measured value, normalized value".
	 * Random are the included information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 9, 10, or 34.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "measured value, normalized value".
	 */
	private List<ASDU> createASDUs_M_ME_XX1_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(2);
		ieClasses.add(NormalizedValue.class);
		ieClasses.add(QualityDescriptorWithOV.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNormalizedValueElement"));
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createQualityDescriptorWithOVElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "measured value, scaled value". Random
	 * are the included information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 11, 12, or 35.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "measured value, scaled value".
	 */
	private List<ASDU> createASDUs_M_ME_XX2_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(2);
		ieClasses.add(ScaledValue.class);
		ieClasses.add(QualityDescriptorWithOV.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createScaledValueElement"));
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createQualityDescriptorWithOVElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "measured value, short floating point
	 * number". Random are the included information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 13, 14, or 36.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "measured value, short floating point number".
	 */
	private List<ASDU> createASDUs_M_ME_XX3_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(2);
		ieClasses.add(ShortFloatingPointNumber.class);
		ieClasses.add(QualityDescriptorWithOV.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createShortFloatingPointNumberElement"));
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createQualityDescriptorWithOVElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "integrated totals". Random are the
	 * included information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 15, 16, or 37.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "integrated totals".
	 */
	private List<ASDU> createASDUs_M_IT_XX_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(2);
		ieClasses.add(BinaryCounterReading.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators
					.add(RandomInformationElementCreator.class.getDeclaredMethod("createBinaryCounterReadingElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "event of protection equipment with time
	 * tag". Random are the included information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 17 or 38.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "event of protection equipment with time tag".
	 */
	private List<ASDU> createASDUs_M_EP_TX1_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(2);
		ieClasses.add(PESingleEvent.class);
		ieClasses.add(TwoOctetBinaryTime.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createPESingleEventElement"));
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createTwoOctetBinaryTimeElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "packed start events of protection
	 * equipment with time tag". Random are the included information elements and
	 * time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 18 or 39.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "packed start events of protection equipment with time tag".
	 */
	private List<ASDU> createASDUs_M_EP_TX2_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(3);
		ieClasses.add(PEStartEvent.class);
		ieClasses.add(QualityDescriptorForPE.class);
		ieClasses.add(TwoOctetBinaryTime.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createPEStartEventElement"));
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createQualityDescriptorForPEElement"));
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createTwoOctetBinaryTimeElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "packed output circuit information of
	 * protection equipment with time tag". Random are the included information
	 * elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 19 or 40.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "packed output circuit information of protection equipment
	 *         with time tag".
	 */
	private List<ASDU> createASDUs_M_EP_TX3_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(3);
		ieClasses.add(PEOutputCircuitInformation.class);
		ieClasses.add(QualityDescriptorForPE.class);
		ieClasses.add(TwoOctetBinaryTime.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createPEOutputCircuitInformationElement"));
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createQualityDescriptorForPEElement"));
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createTwoOctetBinaryTimeElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "packed single-point information with
	 * status change detection". Random are the included information elements and
	 * time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 20.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "packed single-point information with status change
	 *         detection".
	 */
	private List<ASDU> createASDUs_M_PS_NA_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(2);
		ieClasses.add(StatusAndChangeDetection.class);
		ieClasses.add(QualityDescriptorWithOV.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createStatusAndChangeDetectionElement"));
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createQualityDescriptorWithOVElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "measured value, normalized value without
	 * quality descriptor". Random are the included information elements and time
	 * tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 21.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "measured value, normalized value without quality
	 *         descriptor".
	 */
	private List<ASDU> createASDUs_M_ME_ND_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(1);
		ieClasses.add(NormalizedValue.class);
		List<Method> ieCreators = new ArrayList<>(1);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNormalizedValueElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "single command". Random are the included
	 * information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 45 or 58.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "single command".
	 */
	private List<ASDU> createASDUs_C_SC_XA_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(1);
		ieClasses.add(SingleCommand.class);
		List<Method> ieCreators = new ArrayList<>(1);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createSingleCommandElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "double command". Random are the included
	 * information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 46 or 59.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "double command".
	 */
	private List<ASDU> createASDUs_C_DC_XA_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(1);
		ieClasses.add(DoubleCommand.class);
		List<Method> ieCreators = new ArrayList<>(1);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createDoubleCommandElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "regulating step command". Random are the
	 * included information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 47 or 60.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "regulating step command".
	 */
	private List<ASDU> createASDUs_C_RC_XA_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(1);
		ieClasses.add(RegulatingStepCommand.class);
		List<Method> ieCreators = new ArrayList<>(1);
		try {
			ieCreators
					.add(RandomInformationElementCreator.class.getDeclaredMethod("createRegulatingStepCommandElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "set point command, normalized value".
	 * Random are the included information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 48 or 61.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "set point command, normalized value".
	 */
	private List<ASDU> createASDUs_C_SE_XA_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(2);
		ieClasses.add(NormalizedValue.class);
		ieClasses.add(SetPointCommandQualifier.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNormalizedValueElement"));
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createSetPointCommandQualifierElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "set point command, scaled value". Random
	 * are the included information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 49 or 62.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "set point command, scaled value".
	 */
	private List<ASDU> createASDUs_C_SE_XB_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(2);
		ieClasses.add(ScaledValue.class);
		ieClasses.add(SetPointCommandQualifier.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createScaledValueElement"));
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createSetPointCommandQualifierElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "set point command, short floating point
	 * number". Random are the included information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 50 or 63.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "set point command, short floating point number".
	 */
	private List<ASDU> createASDUs_C_SE_XC_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(2);
		ieClasses.add(ShortFloatingPointNumber.class);
		ieClasses.add(SetPointCommandQualifier.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createShortFloatingPointNumberElement"));
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createSetPointCommandQualifierElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "bitstring of 32 bits". Random are the
	 * included information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 51 or 64.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "bitstring of 32 bits".
	 */
	private List<ASDU> createASDUs_C_BO_XA_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(1);
		ieClasses.add(BinaryStateInformation.class);
		List<Method> ieCreators = new ArrayList<>(1);
		try {
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createBinaryStateInformationElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "end of initialization". Random are the
	 * included information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 70.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "end of initialization".
	 */
	private List<ASDU> createASDUs_M_EI_NA_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(1);
		ieClasses.add(CauseOfInitialization.class);
		List<Method> ieCreators = new ArrayList<>(1);
		try {
			ieCreators
					.add(RandomInformationElementCreator.class.getDeclaredMethod("createCauseOfInitializationElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "interrogation command". Random are the
	 * included information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 100.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "interrogation command".
	 */
	private List<ASDU> createASDUs_C_IC_NA_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(1);
		ieClasses.add(InterrogationQualifier.class);
		List<Method> ieCreators = new ArrayList<>(1);
		try {
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createInterrogationQualifierElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "counter interrogation command". Random
	 * are the included information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 101.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "counter interrogation command".
	 */
	private List<ASDU> createASDUs_C_CI_NA_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(1);
		ieClasses.add(CounterInterrogationCommandQualifier.class);
		List<Method> ieCreators = new ArrayList<>(1);
		try {
			ieCreators.add(RandomInformationElementCreator.class
					.getDeclaredMethod("createCounterInterrogationCommandQualifierElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "read command". Random are the included
	 * information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 102.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "read command".
	 */
	private List<ASDU> createASDUs_C_RD_NA_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>();
		List<Method> ieCreators = new ArrayList<>();

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "clock synchronization command". Random
	 * are the included information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 103.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "clock synchronization command".
	 */
	private List<ASDU> createASDUs_C_CS_NA_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(1);
		ieClasses.add(SevenOctetBinaryTime.class);
		List<Method> ieCreators = new ArrayList<>(1);
		try {
			ieCreators
					.add(RandomInformationElementCreator.class.getDeclaredMethod("createSevenOctetBinaryTimeElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "test command". Random are the included
	 * information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 104 or 107.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "test command".
	 */
	private List<ASDU> createASDUs_C_TS_NA_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(1);
		ieClasses.add(FixedTestBitPattern.class);
		List<Method> ieCreators = new ArrayList<>(1);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createFixedTestBitPatternElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "reset process command". Random are the
	 * included information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 105.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "reset process command".
	 */
	private List<ASDU> createASDUs_C_RP_NA_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(1);
		ieClasses.add(ResetProcessCommandQualifier.class);
		List<Method> ieCreators = new ArrayList<>(1);
		try {
			ieCreators.add(RandomInformationElementCreator.class
					.getDeclaredMethod("createResetProcessCommandQualifierElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "delay acquisition command". Random are
	 * the included information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 106.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "delay acquisition command".
	 */
	private List<ASDU> createASDUs_C_CD_NA_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(1);
		ieClasses.add(TwoOctetBinaryTime.class);
		List<Method> ieCreators = new ArrayList<>(1);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createTwoOctetBinaryTimeElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "parameter of measured value, normalized
	 * value". Random are the included information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 110.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "parameter of measured value, normalized value".
	 */
	private List<ASDU> createASDUs_P_ME_NA_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(2);
		ieClasses.add(NormalizedValue.class);
		ieClasses.add(MeasuredValuesParameterQualifier.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNormalizedValueElement"));
			ieCreators.add(RandomInformationElementCreator.class
					.getDeclaredMethod("createMeasuredValuesParameterQualifierElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "parameter of measured value, scaled
	 * value". Random are the included information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 111.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "parameter of measured value, scaled value".
	 */
	private List<ASDU> createASDUs_P_ME_NB_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(2);
		ieClasses.add(ScaledValue.class);
		ieClasses.add(MeasuredValuesParameterQualifier.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createScaledValueElement"));
			ieCreators.add(RandomInformationElementCreator.class
					.getDeclaredMethod("createMeasuredValuesParameterQualifierElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "parameter of measured value, scaled
	 * value". Random are the included information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 112.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "parameter of measured value, scaled value".
	 */
	private List<ASDU> createASDUs_P_ME_NC_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(2);
		ieClasses.add(ShortFloatingPointNumber.class);
		ieClasses.add(MeasuredValuesParameterQualifier.class);
		List<Method> ieCreators = new ArrayList<>(2);
		try {
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createShortFloatingPointNumberElement"));
			ieCreators.add(RandomInformationElementCreator.class
					.getDeclaredMethod("createMeasuredValuesParameterQualifierElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "parameter activation". Random are the
	 * included information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 113.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "parameter activation".
	 */
	private List<ASDU> createASDUs_P_AC_NA_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(1);
		ieClasses.add(ParameterActivationQualifier.class);
		List<Method> ieCreators = new ArrayList<>(1);
		try {
			ieCreators.add(RandomInformationElementCreator.class
					.getDeclaredMethod("createParameterActivationQualifierElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "file ready". Random are the included
	 * information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 120.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "file ready".
	 */
	private List<ASDU> createASDUs_F_FR_NA_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(3);
		ieClasses.add(NameOfFile.class);
		ieClasses.add(FileOrSectionLength.class);
		ieClasses.add(FileReadyQualifier.class);
		List<Method> ieCreators = new ArrayList<>(3);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNameOfFileElement"));
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createFileOrSectionLengthElement"));
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createFileReadyQualifierElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "section ready". Random are the included
	 * information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 121.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "section ready".
	 */
	private List<ASDU> createASDUs_F_SR_NA_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(4);
		ieClasses.add(NameOfFile.class);
		ieClasses.add(NameOfSection.class);
		ieClasses.add(FileOrSectionLength.class);
		ieClasses.add(SectionReadyQualifier.class);
		List<Method> ieCreators = new ArrayList<>(4);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNameOfFileElement"));
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNameOfSectionElement"));
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createFileOrSectionLengthElement"));
			ieCreators
					.add(RandomInformationElementCreator.class.getDeclaredMethod("createSectionReadyQualifierElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "call directory, select file, call file,
	 * call section". Random are the included information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 122.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "call directory, select file, call file, call section".
	 */
	private List<ASDU> createASDUs_F_SC_NA_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(3);
		ieClasses.add(NameOfFile.class);
		ieClasses.add(NameOfSection.class);
		ieClasses.add(SelectAndCallQualifier.class);
		List<Method> ieCreators = new ArrayList<>(3);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNameOfFileElement"));
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNameOfSectionElement"));
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createSelectAndCallQualifierElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "last section, last segment". Random are
	 * the included information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 123.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "last section, last segment".
	 */
	private List<ASDU> createASDUs_F_LS_NA_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(4);
		ieClasses.add(NameOfFile.class);
		ieClasses.add(NameOfSection.class);
		ieClasses.add(LastSectionOrSegmentQualifier.class);
		ieClasses.add(Checksum.class);
		List<Method> ieCreators = new ArrayList<>(4);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNameOfFileElement"));
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNameOfSectionElement"));
			ieCreators.add(RandomInformationElementCreator.class
					.getDeclaredMethod("createLastSectionOrSegmentQualifierElement"));
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createChecksumElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "ack file, ack section". Random are the
	 * included information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 124.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "ack file, ack section".
	 */
	private List<ASDU> createASDUs_F_AF_NA_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(3);
		ieClasses.add(NameOfFile.class);
		ieClasses.add(NameOfSection.class);
		ieClasses.add(AckFileOrSegmentQualifier.class);
		List<Method> ieCreators = new ArrayList<>(3);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNameOfFileElement"));
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNameOfSectionElement"));
			ieCreators.add(
					RandomInformationElementCreator.class.getDeclaredMethod("createAckFileOrSegmentQualifierElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "segment". Random are the included
	 * information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 125.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "segment".
	 */
	private List<ASDU> createASDUs_F_SG_NA_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(3);
		ieClasses.add(NameOfFile.class);
		ieClasses.add(NameOfSection.class);
		ieClasses.add(Segment.class);
		List<Method> ieCreators = new ArrayList<>(3);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNameOfFileElement"));
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNameOfSectionElement"));
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createSegmentElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a {@link ASDUs} of the type "directory". Random are the included
	 * information elements and time tags.
	 * <p>
	 * Calls {@link #createASDUs(int, List, List)}
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}: 126.
	 * @return An list of {@link ASDUs} with random {@link IInformationElement}s of
	 *         the type "directory".
	 */
	private List<ASDU> createASDUs_F_DR_TA_1(int asduTypeId) {
		List<Class<? extends IInformationElement>> ieClasses = new ArrayList<>(3);
		ieClasses.add(NameOfFile.class);
		ieClasses.add(FileOrSectionLength.class);
		ieClasses.add(FileStatus.class);
		List<Method> ieCreators = new ArrayList<>(3);
		try {
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createNameOfFileElement"));
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createFileOrSectionLengthElement"));
			ieCreators.add(RandomInformationElementCreator.class.getDeclaredMethod("createFileStatusElement"));
		} catch (NoSuchMethodException | SecurityException e1) {
			e1.printStackTrace();
		}

		return createASDUs(asduTypeId, ieClasses, ieCreators);
	}

	/**
	 * Creates a list of random {@link ASDUs} for a specific ASDU type covering all
	 * possible combinations of the following
	 * <ul>
	 * <li>all valid structure qualifiers</li>
	 * <li>one up to three information elements</li>
	 * <li>all valid causes of transmission</li>
	 * <li>positive and negative confirm</li>
	 * <li>test and no test</li>
	 * <li>0, 1, 255 as originator address and a random address in between</li>
	 * <li>0, 1, 255 as ASDU address and a random address in between</li>
	 * <li>0, 1, 255 as information object address and a random address in
	 * between</li>
	 * </ul>
	 * 
	 * @param asduTypeId The ID of the {@link ASDUType}. The IDs are subsequent
	 *                   numbers defined in the standard.
	 * @return A list of random {@link ASDUs} covering all possible combinations.
	 */
	public List<ASDU> createASDUs(int asduTypeId) {
		switch (asduTypeId) {
		case 1:
		case 2:
		case 30:
			return createASDUs_M_SP_XX_1(asduTypeId);
		case 3:
		case 4:
		case 31:
			return createASDUs_M_DP_XX_1(asduTypeId);
		case 5:
		case 6:
		case 32:
			return createASDUs_M_ST_XX_1(asduTypeId);
		case 7:
		case 8:
		case 33:
			return createASDUs_M_BO_XX_1(asduTypeId);
		case 9:
		case 10:
		case 34:
			return createASDUs_M_ME_XX1_1(asduTypeId);
		case 11:
		case 12:
		case 35:
			return createASDUs_M_ME_XX2_1(asduTypeId);
		case 13:
		case 14:
		case 36:
			return createASDUs_M_ME_XX3_1(asduTypeId);
		case 15:
		case 16:
		case 37:
			return createASDUs_M_IT_XX_1(asduTypeId);
		case 17:
		case 38:
			return createASDUs_M_EP_TX1_1(asduTypeId);
		case 18:
		case 39:
			return createASDUs_M_EP_TX2_1(asduTypeId);
		case 19:
		case 40:
			return createASDUs_M_EP_TX3_1(asduTypeId);
		case 20:
			return createASDUs_M_PS_NA_1(asduTypeId);
		case 21:
			return createASDUs_M_ME_ND_1(asduTypeId);
		case 45:
		case 58:
			return createASDUs_C_SC_XA_1(asduTypeId);
		case 46:
		case 59:
			return createASDUs_C_DC_XA_1(asduTypeId);
		case 47:
		case 60:
			return createASDUs_C_RC_XA_1(asduTypeId);
		case 48:
		case 61:
			return createASDUs_C_SE_XA_1(asduTypeId);
		case 49:
		case 62:
			return createASDUs_C_SE_XB_1(asduTypeId);
		case 50:
		case 63:
			return createASDUs_C_SE_XC_1(asduTypeId);
		case 51:
		case 64:
			return createASDUs_C_BO_XA_1(asduTypeId);
		case 70:
			return createASDUs_M_EI_NA_1(asduTypeId);
		case 100:
			return createASDUs_C_IC_NA_1(asduTypeId);
		case 101:
			return createASDUs_C_CI_NA_1(asduTypeId);
		case 102:
			return createASDUs_C_RD_NA_1(asduTypeId);
		case 103:
			return createASDUs_C_CS_NA_1(asduTypeId);
		case 104:
		case 107:
			return createASDUs_C_TS_NA_1(asduTypeId);
		case 105:
			return createASDUs_C_RP_NA_1(asduTypeId);
		case 106:
			return createASDUs_C_CD_NA_1(asduTypeId);
		case 110:
			return createASDUs_P_ME_NA_1(asduTypeId);
		case 111:
			return createASDUs_P_ME_NB_1(asduTypeId);
		case 112:
			return createASDUs_P_ME_NC_1(asduTypeId);
		case 113:
			return createASDUs_P_AC_NA_1(asduTypeId);
		case 120:
			return createASDUs_F_FR_NA_1(asduTypeId);
		case 121:
			return createASDUs_F_SR_NA_1(asduTypeId);
		case 122:
			return createASDUs_F_SC_NA_1(asduTypeId);
		case 123:
			return createASDUs_F_LS_NA_1(asduTypeId);
		case 124:
			return createASDUs_F_AF_NA_1(asduTypeId);
		case 125:
			return createASDUs_F_SG_NA_1(asduTypeId);
		case 126:
			return createASDUs_F_DR_TA_1(asduTypeId);
		default:
			return null;
		}
	}

}