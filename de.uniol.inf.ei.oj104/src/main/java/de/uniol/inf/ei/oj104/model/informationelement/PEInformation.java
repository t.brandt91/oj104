/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import java.util.Arrays;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.IByteEncodedEntity;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * Information element that contains information about protection equipment.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class PEInformation implements IByteEncodedEntity {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = 3526058836225882711L;

	/**
	 * True for general protection.
	 */
	private boolean general;

	/**
	 * True for protection of phase L1.
	 */
	private boolean phaseL1;

	/**
	 * True for protection of phase L2.
	 */
	private boolean phaseL2;

	/**
	 * True for protection of phase L3.
	 */
	private boolean phaseL3;

	/**
	 * Checks, whether the information is about general protection.
	 * 
	 * @return True for general protection.
	 */
	public boolean isGeneral() {
		return general;
	}

	/**
	 * Sets, whether the information is about general protection.
	 * 
	 * @param general True for general protection.
	 */
	public void setGeneral(boolean general) {
		this.general = general;
	}

	/**
	 * Checks, whether the information is about protection of phase L1.
	 * 
	 * @return True for protection of phase L1.
	 */
	public boolean isPhaseL1() {
		return phaseL1;
	}

	/**
	 * Sets, whether the information is about protection of phase L1.
	 * 
	 * @param phaseL1 True for protection of phase L1.
	 */
	public void setPhaseL1(boolean phaseL1) {
		this.phaseL1 = phaseL1;
	}

	/**
	 * Checks, whether the information is about protection of phase L2.
	 * 
	 * @return True for protection of phase L2.
	 */
	public boolean isPhaseL2() {
		return phaseL2;
	}

	/**
	 * Sets, whether the information is about protection of phase L2.
	 * 
	 * @param phaseL2 True for protection of phase L2.
	 */
	public void setPhaseL2(boolean phaseL2) {
		this.phaseL2 = phaseL2;
	}

	/**
	 * Checks, whether the information is about protection of phase L3.
	 * 
	 * @return True for protection of phase L3.
	 */
	public boolean isPhaseL3() {
		return phaseL3;
	}

	/**
	 * Sets, whether the information is about protection of phase L3.
	 * 
	 * @param phaseL3 True for protection of phase L3.
	 */
	public void setPhaseL3(boolean phaseL3) {
		this.phaseL3 = phaseL3;
	}

	/**
	 * Empty default constructor.
	 */
	public PEInformation() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param general True for general protection.
	 * @param phaseL1 True for protection of phase L1.
	 * @param phaseL2 True for protection of phase L2.
	 * @param phaseL3 True for protection of phase L3.
	 */
	public PEInformation(boolean general, boolean phaseL1, boolean phaseL2, boolean phaseL3) {
		this.general = general;
		this.phaseL1 = phaseL1;
		this.phaseL2 = phaseL2;
		this.phaseL3 = phaseL3;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (general ? 1231 : 1237);
		result = prime * result + (phaseL1 ? 1231 : 1237);
		result = prime * result + (phaseL2 ? 1231 : 1237);
		result = prime * result + (phaseL3 ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PEInformation other = (PEInformation) obj;
		if (general != other.general) {
			return false;
		}
		if (phaseL1 != other.phaseL1) {
			return false;
		}
		if (phaseL2 != other.phaseL2) {
			return false;
		}
		if (phaseL3 != other.phaseL3) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		}

		int byteAsInt = bytes[0] & 0xff;

		general = (byteAsInt & 0x01) == 0x01;
		phaseL1 = (byteAsInt & 0x02) == 0x02;
		phaseL2 = (byteAsInt & 0x04) == 0x04;
		phaseL3 = (byteAsInt & 0x08) == 0x08;

		return Arrays.copyOfRange(bytes, getEncodedSize(), bytes.length);
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		byte b = 0;
		b |= general ? 0x01 : 0x00;
		b |= phaseL1 ? 0x02 : 0x00;
		b |= phaseL2 ? 0x04 : 0x00;
		b |= phaseL3 ? 0x08 : 0x00;
		return new byte[] { b };
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return 1;
	}

}