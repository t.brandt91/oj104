/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * A structure qualifiers qualifies the structure of the following
 * {@link IInformationObject}s as follows:
 * <p>
 * If the qualifier is {@link StructureQualifier#SINGLE}, individual single
 * {@link IInformationElement}s or a combination of {@link IInformationElement}s
 * in a number of {@link IInformationObject}s of the same type are addressed.
 * <p>
 * If the qualifier is {@link StructureQualifier#SEQUENCE}, an
 * {@link InformationElementSequence} of single {@link IInformationElement}s or
 * equal combinations of {@link IInformationElement}s of a single object are
 * addressed.
 * <p>
 * This is done by providing a constructor for either
 * {@link SingleInformationObject}s or {@link SequenceInformationObject}s.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public enum StructureQualifier implements Serializable {

	/**
	 * Individual single {@link IInformationElement}s or a combination of
	 * {@link IInformationElement}s in a number of {@link IInformationObject}s of
	 * the same type are addressed.
	 * <p>
	 * This is done by providing a constructor for {@link SingleInformationObject}s.
	 */
	SINGLE(0, SingleInformationObject.class) {

		@Override
		public Constructor<? extends IInformationObject> getInformationObjectConstructor()
				throws NoSuchMethodException, SecurityException {
			return SingleInformationObject.class.getConstructor(List.class, Class.class);
		}

		@Override
		public int getNumberOfInformationObjects(DataUnitType dataUnitType) {
			return dataUnitType.getNumberOfInformationElements();
		}
	},

	/**
	 * An {@link InformationElementSequence} of single {@link IInformationElement}s
	 * or equal combinations of {@link IInformationElement}s of a single object are
	 * addressed.
	 * <p>
	 * This is done by providing a constructor for
	 * {@link SequenceInformationObject}s.
	 */
	SEQUENCE(1, SequenceInformationObject.class) {

		@Override
		public Constructor<? extends IInformationObject> getInformationObjectConstructor()
				throws NoSuchMethodException, SecurityException {
			return SequenceInformationObject.class.getConstructor(List.class, Class.class, Integer.class);
		}

		@Override
		public int getNumberOfInformationObjects(DataUnitType dataUnitType) {
			return 1;
		}
	};

	/**
	 * The code of the structure qualifier: 0 = single, 1 = sequence.
	 */
	private int code;

	/**
	 * The class for the {@link IInformationObject}s to use:
	 * {@link SingleInformationObject} or {@link SequenceInformationObject}.
	 */
	private Class<? extends IInformationObject> informationObjectClass;

	/**
	 * Returns the code of the structure qualifier.
	 * 
	 * @return 0 = single, 1 = sequence.
	 */
	public int getCode() {
		return code;
	}

	/**
	 * Returns the class for the {@link IInformationObject}s to use.
	 * 
	 * @return {@link SingleInformationObject} or {@link SequenceInformationObject}.
	 */
	public Class<? extends IInformationObject> getInformationObjectClass() {
		return informationObjectClass;
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param code                   The code of the structure qualifier: 0 =
	 *                               single, 1 = sequence.
	 * @param informationObjectClass The class for the {@link IInformationObject}s
	 *                               to use: {@link SingleInformationObject} or
	 *                               {@link SequenceInformationObject}.
	 */
	private StructureQualifier(int code, Class<? extends IInformationObject> informationObjectClass) {
		this.code = code;
		this.informationObjectClass = informationObjectClass;
	}

	/**
	 * Retrieve an enum entry.
	 * 
	 * @param code The code of the structure qualifier: 0 = single, 1 = sequence.
	 * @return An optional of the {@link StructureQualifier} or
	 *         {@link Optional#empty()}, if there is none for the code.
	 */
	public static Optional<StructureQualifier> getQualifier(int code) {
		return Arrays.asList(values()).stream().filter(qualifier -> qualifier.code == code).findAny();
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return 1;
	}

	/**
	 * Returns the constructor for the {@link IInformationObject}s.
	 * 
	 * @return A class implementing {@link IInformationObject} to be used for the
	 *         given {@link StructureQualifier}.
	 * @throws NoSuchMethodException See {@link Class#getConstructor(Class...)}
	 * @throws SecurityException     See {@link Class#getConstructor(Class...)}
	 */
	public abstract Constructor<? extends IInformationObject> getInformationObjectConstructor()
			throws NoSuchMethodException, SecurityException;

	/**
	 * Returns the number of {@link IInformationObject}s.
	 * 
	 * @return An integer greater than 0 defining how many
	 *         {@link IInformationObject}s are in the {@link ASDU} based on the
	 *         given {@link StructureQualifier}.
	 */
	public abstract int getNumberOfInformationObjects(DataUnitType dataUnitType);

}