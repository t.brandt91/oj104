/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.IByteEncodedEntity;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * A float value to be used in {@link IInformationElement}s.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class FloatValue implements IByteEncodedEntity {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = -6158612073110309683L;

	/**
	 * The float value.
	 */
	private float value;

	/**
	 * Returns the float value.
	 * 
	 * @return A float.
	 */
	public float getValue() {
		return value;
	}

	/**
	 * Sets the float value.
	 * 
	 * @param value A float.
	 */
	public void setValue(float value) {
		this.value = value;
	}

	/**
	 * Empty default constructor.
	 */
	public FloatValue() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param value A float.
	 */
	public FloatValue(float value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(value);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		FloatValue other = (FloatValue) obj;
		if (Float.floatToIntBits(value) != Float.floatToIntBits(other.value)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		}

		IntegerValue intValue = new IntegerValue();
		byte[] remainingBytes = intValue.fromBytes(bytes);
		value = Float.intBitsToFloat(intValue.getValue());
		return remainingBytes;
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		int intValue = Float.floatToIntBits(value);
		return new IntegerValue(intValue).toBytes();
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return Float.BYTES;
	}

}