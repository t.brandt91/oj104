/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.util;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;

import de.uniol.inf.ei.oj104.model.SequenceInformationObject;
import de.uniol.inf.ei.oj104.model.SingleInformationObject;
import de.uniol.inf.ei.oj104.model.informationelement.AckFileOrSegmentQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.BinaryCounterReading;
import de.uniol.inf.ei.oj104.model.informationelement.BinaryStateInformation;
import de.uniol.inf.ei.oj104.model.informationelement.CauseOfInitialization;
import de.uniol.inf.ei.oj104.model.informationelement.Checksum;
import de.uniol.inf.ei.oj104.model.informationelement.CommandQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.CounterInterrogationCommandQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.DoubleCommand;
import de.uniol.inf.ei.oj104.model.informationelement.DoublePointInformation;
import de.uniol.inf.ei.oj104.model.informationelement.FileOrSectionLength;
import de.uniol.inf.ei.oj104.model.informationelement.FileReadyQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.FileStatus;
import de.uniol.inf.ei.oj104.model.informationelement.FixedTestBitPattern;
import de.uniol.inf.ei.oj104.model.informationelement.InterrogationQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.LastSectionOrSegmentQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.MeasuredValuesParameterQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.NameOfFile;
import de.uniol.inf.ei.oj104.model.informationelement.NameOfSection;
import de.uniol.inf.ei.oj104.model.informationelement.NormalizedValue;
import de.uniol.inf.ei.oj104.model.informationelement.PEOutputCircuitInformation;
import de.uniol.inf.ei.oj104.model.informationelement.PESingleEvent;
import de.uniol.inf.ei.oj104.model.informationelement.PEStartEvent;
import de.uniol.inf.ei.oj104.model.informationelement.ParameterActivationQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorForPE;
import de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorWithOV;
import de.uniol.inf.ei.oj104.model.informationelement.RegulatingStepCommand;
import de.uniol.inf.ei.oj104.model.informationelement.ResetProcessCommandQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.ScaledValue;
import de.uniol.inf.ei.oj104.model.informationelement.SectionReadyQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.Segment;
import de.uniol.inf.ei.oj104.model.informationelement.SelectAndCallQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.SetPointCommandQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.ShortFloatingPointNumber;
import de.uniol.inf.ei.oj104.model.informationelement.SingleCommand;
import de.uniol.inf.ei.oj104.model.informationelement.SinglePointInformation;
import de.uniol.inf.ei.oj104.model.informationelement.StatusAndChangeDetection;
import de.uniol.inf.ei.oj104.model.informationelement.ValueWithTransientStateIndication;
import de.uniol.inf.ei.oj104.model.timetag.SevenOctetBinaryTime;
import de.uniol.inf.ei.oj104.model.timetag.ThreeOctetBinaryTime;
import de.uniol.inf.ei.oj104.model.timetag.TwoOctetBinaryTime;

/**
 * JSON parser for the data types defined in OJ104.
 * <p>
 * It is possible to serialize to string and from string to the data types.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class JsonParser {

	/**
	 * The logger instance for this class.
	 */
	static final Logger logger = LoggerFactory.getLogger(JsonParser.class);

	/**
	 * The used object mapper. Modules and submodules are registered for a complete
	 * serialization of the objects.
	 */
	private static final ObjectMapper mapper = new ObjectMapper();

	static {

		// setting the classloader is important, if the json parser is called from a
		// different context, e.g. a different OSGi bundle
		setClassLoader();

		// for Optional
		mapper.registerModule(new Jdk8Module());

		// for IInformationObject
		mapper.registerSubtypes(new NamedType(SingleInformationObject.class, "SingleInformationObject"));
		mapper.registerSubtypes(new NamedType(SequenceInformationObject.class, "SequenceInformationObject"));

		// for IInformationElement
		mapper.registerSubtypes(new NamedType(AckFileOrSegmentQualifier.class, "AckFileOrSegmentQualifier"));
		mapper.registerSubtypes(new NamedType(BinaryCounterReading.class, "BinaryCounterReading"));
		mapper.registerSubtypes(new NamedType(BinaryStateInformation.class, "BinaryStateInformation"));
		mapper.registerSubtypes(new NamedType(CauseOfInitialization.class, "CauseOfInitialization"));
		mapper.registerSubtypes(new NamedType(Checksum.class, "Checksum"));
		mapper.registerSubtypes(new NamedType(CommandQualifier.class, "CommandQualifier"));
		mapper.registerSubtypes(
				new NamedType(CounterInterrogationCommandQualifier.class, "CounterInterrogationCommandQualifier"));
		mapper.registerSubtypes(new NamedType(DoubleCommand.class, "DoubleCommand"));
		mapper.registerSubtypes(new NamedType(DoublePointInformation.class, "DoublePointInformation"));
		mapper.registerSubtypes(new NamedType(FileOrSectionLength.class, "FileOrSectionLength"));
		mapper.registerSubtypes(new NamedType(FileReadyQualifier.class, "FileReadyQualifier"));
		mapper.registerSubtypes(new NamedType(FileStatus.class, "FileStatus"));
		mapper.registerSubtypes(new NamedType(FixedTestBitPattern.class, "FixedTestBitPattern"));
		mapper.registerSubtypes(new NamedType(InterrogationQualifier.class, "InterrogationQualifier"));
		mapper.registerSubtypes(new NamedType(LastSectionOrSegmentQualifier.class, "LastSectionOrSegmentQualifier"));
		mapper.registerSubtypes(
				new NamedType(MeasuredValuesParameterQualifier.class, "MeasuredValuesParameterQualifier"));
		mapper.registerSubtypes(new NamedType(NameOfFile.class, "NameOfFile"));
		mapper.registerSubtypes(new NamedType(NameOfSection.class, "NameOfSection"));
		mapper.registerSubtypes(new NamedType(NormalizedValue.class, "NormalizedValue"));
		mapper.registerSubtypes(new NamedType(ParameterActivationQualifier.class, "ParameterActivationQualifier"));
		mapper.registerSubtypes(new NamedType(PEOutputCircuitInformation.class, "PEOutputCircuitInformation"));
		mapper.registerSubtypes(new NamedType(PESingleEvent.class, "PESingleEvent"));
		mapper.registerSubtypes(new NamedType(PEStartEvent.class, "PEStartEvent"));
		mapper.registerSubtypes(new NamedType(QualityDescriptorForPE.class, "QualityDescriptorForPE"));
		mapper.registerSubtypes(new NamedType(QualityDescriptorWithOV.class, "QualityDescriptorWithOV"));
		mapper.registerSubtypes(new NamedType(RegulatingStepCommand.class, "RegulatingStepCommand"));
		mapper.registerSubtypes(new NamedType(ResetProcessCommandQualifier.class, "ResetProcessCommandQualifier"));
		mapper.registerSubtypes(new NamedType(ScaledValue.class, "ScaledValue"));
		mapper.registerSubtypes(new NamedType(SectionReadyQualifier.class, "SectionReadyQualifier"));
		mapper.registerSubtypes(new NamedType(Segment.class, "Segment"));
		mapper.registerSubtypes(new NamedType(SelectAndCallQualifier.class, "SelectAndCallQualifier"));
		mapper.registerSubtypes(new NamedType(SetPointCommandQualifier.class, "SetPointCommandQualifier"));
		mapper.registerSubtypes(new NamedType(ShortFloatingPointNumber.class, "ShortFloatingPointNumber"));
		mapper.registerSubtypes(new NamedType(SingleCommand.class, "SingleCommand"));
		mapper.registerSubtypes(new NamedType(SinglePointInformation.class, "SinglePointInformation"));
		mapper.registerSubtypes(new NamedType(StatusAndChangeDetection.class, "StatusAndChangeDetection"));
		mapper.registerSubtypes(
				new NamedType(ValueWithTransientStateIndication.class, "ValueWithTransientStateIndication"));

		// for ITimeTag
		mapper.registerSubtypes(new NamedType(TwoOctetBinaryTime.class, "TwoOctetBinaryTime"));
		mapper.registerSubtypes(new NamedType(ThreeOctetBinaryTime.class, "ThreeOctetBinaryTime"));
		mapper.registerSubtypes(new NamedType(SevenOctetBinaryTime.class, "SevenOctetBinaryTime"));
	}

	/**
	 * Sets the class loader from the JSON parser as the class loader of the
	 * {@link TypeFactory} of {@link #mapper}.
	 */
	private static void setClassLoader() {
		ClassLoader classLoader = JsonParser.class.getClassLoader();
		TypeFactory typeFactory = TypeFactory.defaultInstance().withClassLoader(classLoader);
		mapper.setTypeFactory(typeFactory);
	}

	/**
	 * Converts an OJ104 object into a String.
	 * 
	 * @param obj The OJ104 object to convert.
	 * @return A JSON representation of the input.
	 */
	public static String serialize(Object obj) {
		try {
			return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Converts a JSON representation of an OJ104 object into the object.
	 * 
	 * @param json The string to convert.
	 * @param clz  The class of the object represented by the String.
	 * @return AN OJ104 object represented by the String.
	 */
	public static Object deserialize(String json, Class<?> clz) {
		try {
			return mapper.readValue(json, clz);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

}