/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model;

import static com.fasterxml.jackson.annotation.JsonTypeInfo.As.PROPERTY;
import static com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * <p>
 * Interface for information objects. Every information object has an
 * information object address (IOA).
 * </p>
 * 
 * <p>
 * There exist two implementations of this interface, namely
 * {@link SingleInformationObject} and {@link SequenceInformationObject}.
 * </p>
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
@JsonTypeInfo(use = NAME, include = PROPERTY)
@JsonSubTypes({ @JsonSubTypes.Type(value = SingleInformationObject.class, name = "SingleInformationObject"),
		@JsonSubTypes.Type(value = SequenceInformationObject.class, name = "SequenceInformationObject") })
public interface IInformationObject extends IByteEncodedEntity {

	/**
	 * Return the IOA of the information object.
	 * 
	 * @return The IOA of the information object is an integer of two or three bytes
	 *         (system property dependent). 0 := irrelevant
	 */
	public int getInformationObjectAddress();
}