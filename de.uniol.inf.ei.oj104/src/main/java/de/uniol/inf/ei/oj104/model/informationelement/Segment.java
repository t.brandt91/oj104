/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * Information element that contains a segment of a file.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class Segment implements IInformationElement {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = 1747669834996910742L;

	/**
	 * The length of the segment (bytes).
	 */
	private ShortValue length;

	/**
	 * The content of the section.
	 */
	private byte[] value;

	/**
	 * Returns the length of the segment (bytes).
	 * 
	 * @return A {@link ShortValue}.
	 */
	public ShortValue getLength() {
		return length;
	}

	/**
	 * Sets the length of the segment (bytes).
	 * 
	 * @param length A {@link ShortValue}.
	 */
	public void setLength(ShortValue length) {
		this.length = length;
	}

	/**
	 * Returns the content of the section.
	 * 
	 * @return A byte array.
	 */
	public byte[] getValue() {
		return value;
	}

	/**
	 * Sets the content of the section.
	 * 
	 * @param value A byte array.
	 */
	public void setValue(byte[] value) {
		this.value = value;
	}

	/**
	 * Empty default constructor.
	 */
	public Segment() {
	}

	/**
	 * Constructor for segments without content.
	 * 
	 * @param length The length of the segment (bytes).
	 */
	public Segment(ShortValue length) {
		this.length = length;
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param length The length of the segment (bytes).
	 * @param value  The content of the section.
	 */
	public Segment(ShortValue length, byte[] value) {
		this(length);
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((length == null) ? 0 : length.hashCode());
		result = prime * result + Arrays.hashCode(value);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Segment other = (Segment) obj;
		if (length == null) {
			if (other.length != null)
				return false;
		} else if (!length.equals(other.length))
			return false;
		if (!Arrays.equals(value, other.value))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < ByteValue.getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), ByteValue.getEncodedSize(), bytes.length);
		}

		length = new ShortValue();
		byte[] remainingBytes = length.fromBytes(bytes);
		value = Arrays.copyOf(remainingBytes, length.getValue());

		return Arrays.copyOfRange(remainingBytes, length.getValue(), bytes.length);
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		return ArrayUtils.addAll(length.toBytes(), value);
	}

}