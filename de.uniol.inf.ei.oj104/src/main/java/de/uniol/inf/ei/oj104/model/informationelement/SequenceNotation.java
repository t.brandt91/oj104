/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import java.util.Arrays;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.IByteEncodedEntity;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * Sequence notation to be used in {@link IInformationElement}s: sequence
 * number, carry flag (y/n), counter adjusted (y/n), and invalid (y/n).
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class SequenceNotation implements IByteEncodedEntity {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = -4339711722787069843L;

	/**
	 * The sequence number.
	 * <p>
	 * Abbreviation: SQ
	 */
	private byte sequenceNumber;

	/**
	 * The carry flag: 0 = no carry, 1 = carry.
	 * <p>
	 * Abbreviation: CY
	 */
	private boolean carry;

	/**
	 * The adjusted flag: 0 = counter was not adjusted, 1 = counter was adjusted.
	 * <p>
	 * Abbreviation: CA
	 */
	private boolean counterAdjusted;

	/**
	 * The invalid quality flag: 0 = valid, 1 = invalid.
	 * <p>
	 * Abbreviation: IV
	 */
	private boolean invalid;

	/**
	 * Returns the sequence number.
	 * <p>
	 * Abbreviation: SQ
	 * 
	 * @return A byte.
	 */
	public byte getSequenceNumber() {
		return sequenceNumber;
	}

	/**
	 * Sets the sequence number.
	 * <p>
	 * Abbreviation: SQ
	 * 
	 * @param sequenceNumber A byte.
	 */
	public void setSequenceNumber(byte sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	/**
	 * Checks, whether the carry flag is set.
	 * 
	 * @return The carry flag: 0 = no carry, 1 = carry.
	 *         <p>
	 *         Abbreviation: CY
	 */
	public boolean isCarry() {
		return carry;
	}

	/**
	 * Sets the carry flag.
	 * 
	 * @param carry The carry flag: 0 = no carry, 1 = carry.
	 *              <p>
	 *              Abbreviation: CY
	 */
	public void setCarry(boolean carry) {
		this.carry = carry;
	}

	/**
	 * Checks, whether the adjusted flag is set.
	 * 
	 * @return The adjusted flag: 0 = counter was not adjusted, 1 = counter was
	 *         adjusted.
	 *         <p>
	 *         Abbreviation: CA
	 */
	public boolean isCounterAdjusted() {
		return counterAdjusted;
	}

	/**
	 * Sets the adjusted flag.
	 * 
	 * @param counterAdjusted The adjusted flag: 0 = counter was not adjusted, 1 =
	 *                        counter was adjusted.
	 *                        <p>
	 *                        Abbreviation: CA
	 */
	public void setCounterAdjusted(boolean counterAdjusted) {
		this.counterAdjusted = counterAdjusted;
	}

	/**
	 * Checks, whether the invalid quality flag is set.
	 * 
	 * @return The invalid quality flag: 0 = valid, 1 = invalid.
	 *         <p>
	 *         Abbreviation: IV
	 */
	public boolean isInvalid() {
		return invalid;
	}

	/**
	 * Sets the invalid quality flag.
	 * 
	 * @param invalid The invalid quality flag: 0 = valid, 1 = invalid.
	 *                <p>
	 *                Abbreviation: IV
	 */
	public void setInvalid(boolean invalid) {
		this.invalid = invalid;
	}

	/**
	 * Empty default constructor.
	 */
	public SequenceNotation() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param sequenceNumber  A byte representing the sequence number (SQ).
	 * @param carry           The carry flag (CY): 0 = no carry, 1 = carry.
	 * @param counterAdjusted The adjusted flag (CA): 0 = counter was not adjusted,
	 *                        1 = counter was adjusted.
	 * @param invalid         The invalid quality flag (IV): 0 = valid, 1 = invalid.
	 */
	public SequenceNotation(byte sequenceNumber, boolean carry, boolean counterAdjusted, boolean invalid) {
		this.sequenceNumber = sequenceNumber;
		this.carry = carry;
		this.counterAdjusted = counterAdjusted;
		this.invalid = invalid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (carry ? 1231 : 1237);
		result = prime * result + (counterAdjusted ? 1231 : 1237);
		result = prime * result + (invalid ? 1231 : 1237);
		result = prime * result + sequenceNumber;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SequenceNotation other = (SequenceNotation) obj;
		if (carry != other.carry) {
			return false;
		}
		if (counterAdjusted != other.counterAdjusted) {
			return false;
		}
		if (invalid != other.invalid) {
			return false;
		}
		if (sequenceNumber != other.sequenceNumber) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		}

		int byteAsInt = bytes[0] & 0xff;

		sequenceNumber = (byte) (byteAsInt & 0x1f);
		carry = (byteAsInt & 0x20) == 0x20;
		counterAdjusted = (byteAsInt & 0x40) == 0x40;
		invalid = (byteAsInt & 0x80) == 0x80;

		return Arrays.copyOfRange(bytes, getEncodedSize(), bytes.length);
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		byte b = sequenceNumber;
		b |= carry ? 0x20 : 0x00;
		b |= counterAdjusted ? 0x40 : 0x00;
		b |= invalid ? 0x80 : 0x00;
		return new byte[] { b };
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return Byte.BYTES;
	}

}