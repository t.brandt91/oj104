/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Util class that loads all application wide user defined properties from
 * /META-INF/SystemProperties.xml into a {@link Properties} object. Such
 * properties are:
 * <ul>
 * <li>port.default: Default port.</li>
 * <li>cot.bytesize: Size in bytes of field "Cause of Transmission": 1 or
 * 2.</li>
 * <li>asduaddress.bytesize: Size in bytes of field "Common Address of ASDU": 1
 * or 2.</li>
 * <li>ioa.bytesize: Size in bytes of field "Information Object Address": 1, 2
 * or 3.</li>
 * <li>t1: Length in milliseconds of the timer t1 (timeout for sequences of
 * numbered I format APDUs before active close)</li>
 * <li>t2: Length in milliseconds of the timer t2 (timeout for sequences of
 * numbered I format APDUs before sending a S format)</li>
 * <li>t3: Length in milliseconds of the timer t3 (timeout for after receiving S
 * format APDU before sending a test APDU; <= 0 for not sending test APDUs)</li>
 * <li>asduhandler.numexecutors: umber of executors that can be used for ASDU
 * handlers</li>
 * </ul>
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class SystemProperties {

	/**
	 * The logger instance for this class.
	 */
	private static final Logger logger = LoggerFactory.getLogger(SystemProperties.class);

	/**
	 * The XML file containing the system properties.
	 */
	private static final String xmlFile = "/META-INF/SystemProperties.xml";

	/**
	 * The loaded system properties.
	 */
	private static Properties properties = new Properties();

	static {
		init();
	}

	/**
	 * Loads everything from XML file.
	 */
	private static void init() {
		try (InputStream in = SystemProperties.class.getResourceAsStream(xmlFile)) {
			properties.loadFromXML(in);
		} catch (IOException e1) {
			logger.error("Error while parsing SystemProperties.xml");
		}
	}

	/**
	 * Returns the system properties.
	 * 
	 */
	public static Properties getProperties() {
		return properties;
	}

}